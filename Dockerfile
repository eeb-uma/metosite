FROM php:7.2.5-apache

RUN ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
RUN apt-get update && apt-get install -y unzip mysql-client && \
    docker-php-ext-install pdo pdo_mysql

EXPOSE 80