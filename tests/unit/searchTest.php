<?php

use PHPUnit\Framework\TestCase;

class metosite3Test extends TestCase
{
	public function testSearchGetJSONcase1()
	// 1. One Organism & One Oxidant
	{	
		$json = SearchGetJSON(array(3,18), 'Homo sapiens', 'H2O2'); 
		$this->assertJson($json);
	} 
	public function testSearchGetJSONcase2()
    // 2. One Organism & All Oxidants
	{	
		$json = SearchGetJSON(array(3,18), 'Homo sapiens', -1); 
		$this->assertJson($json);
	} 
	public function testSearchGetJSONcase3()
    // 3. All Organisms & One Oxidant
	{	
		$json = SearchGetJSON(array(3,18), -1, 'H2O2'); 
		$this->assertJson($json);
	}
	public function testSearchGetJSONcase4()
    // 4. All Organisms & All Oxidant
	{	
		$json = SearchGetJSON(array(3,18), -1, -1); 
		$this->assertJson($json);
	}
	public function testSearchGetJSONcase5()
    //  All Groups & All Organisms & One Oxidant
	{	
		$json = SearchGetJSON(range(1,65), -1, 'H2O2'); 
		$this->assertJson($json);
	}
}