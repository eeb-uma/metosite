<?php

use PHPUnit\Framework\TestCase;

class metosite6Test extends TestCase
{
	public function testgroups_categories1()
	{ 
		$c = groupsCategories('001', '000000');
		$this->assertInternalType('array', $c);
		$this->assertContains(0, $c);
        $this->assertCount(9, $c);
        $this->assertEquals(
        	array(
        		'0','0','1', array(0),array(0),array(0),array(0),array(0),array(0)
        	), $c);   
	}

	public function testgroups_categories2()
	{
		$c = groupsCategories('110', '100200');
		$this->assertInternalType('array', $c);
		$this->assertContains(0, $c);
        $this->assertCount(9, $c);
        $this->assertEquals(
        	array(
        		'1','1','0', array(1),array(0),array(0),array(0,1),array(0),array(0)
        	), $c);   
	}
}