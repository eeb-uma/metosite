<?php
/**
*		https://phpunit.de/manual/current/en/database.html
*   If you want to test code that works with the Database Extension the setup is a
*   bit more complex and you have to extend a different abstract TestCase requiring 
*   you to implement two abstract methods getConnection() and getDataSet(): 
*/

use PHPUnit\Framework\TestCase;
// use PHPUnit\DbUnit\TestCaseTrait;

class metositeTest extends TestCase
{
	// use TestCaseTrait;

	/**
	*	@return PHPUnit_Extensions_Database_DB_IDatabaseConnection
	*/
	// public function getConnection()
	public function testmy_connection()
	{
		// $pdo = new PDO('sqlite::memory:');
        // return $this->createDefaultDBConnection($pdo, 'memory:');
        $connection = my_connection();
		$this->assertInstanceOf('PDO', $connection);
	}	

	
    public function testScanDevuelveArray()
    {
    	$resultadoScan = scan('P01009'); // Proteína patrón (arbitraria) para testear 'Scan'
    	$this->assertInternalType('array', $resultadoScan); // La función devuelve un array
    	$this->assertGreaterThan(0, count($resultadoScan)); // Existe(n) entrada(s) para esta proteína
    	$this->assertEquals('P01009', $resultadoScan[0][0]); // El ID de la proteína es el esperado
    }
    public function testScanDevuelveArrayCYC()
    {
        $resultadoScan = scan('P00004'); // Proteína patrón (arbitraria) para testear 'Scan'
        $this->assertInternalType('array', $resultadoScan); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoScan)); // Existe(n) entrada(s) para esta proteína
        $this->assertEquals('P00004', $resultadoScan[0][0]); // El ID de la proteína es el esperado
    }

    public function testSearchCase1() 
    // 1. One Organism & One Oxidant
    {
    	$resultadoSearch = search(array(3,18), 'Eukaryota', 'H2O2');
    	$this->assertInternalType('array', $resultadoSearch); // La función devuelve un array
    	$this->assertGreaterThan(0, count($resultadoSearch)); // Existe alguna entrada para la consulta
    }
    public function testSearchCase2()
    // 2. One Organism & All Oxidants
    {
    	$resultadoSearch = search(array(3,18), 'Plantae', -1);
    	$this->assertInternalType('array', $resultadoSearch); // La función devuelve un array
    	$this->assertGreaterThan(0, count($resultadoSearch)); // Existe alguna entrada para la consulta
    }
    public function testSearchCase3()
    // 3. All Organisms & One Oxidant
    {
    	$resultadoSearch = search(array(3,18), -1, 'H2O2');
    	$this->assertInternalType('array', $resultadoSearch); // La función devuelve un array
    	$this->assertGreaterThan(0, count($resultadoSearch)); // Existe alguna entrada para la consulta
    }
    public function testSearchCase4()
    // 4. All Organisms & All Oxidant
    {
    	$resultadoSearch = search(array(3,18), -1, -1);
    	$this->assertInternalType('array', $resultadoSearch); // La función devuelve un array
    	$this->assertGreaterThan(0, count($resultadoSearch)); // Existe alguna entrada para la consulta
    }

    public function testGetallprot()
    {
        $a = getallprot();
        //$this->assertIsString($a);
        $this->assertInternalType('string', $a);
        $this->assertGreaterThanOrEqual(1, count($a)); // More than 7 sites   
    }

    public function testPhosphorylation1()
    {
        $resultadoPhosphorylation = phosphorylation('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoPhosphorylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoPhosphorylation)); // No debe ser vacío
    }   

    public function testPhosphorylation2()
    {
        $resultadoPhosphorylation = phosphorylation('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoPhosphorylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoPhosphorylation)); // No debe ser vacío (contiene 'Any')
    }   

    public function testAcetylation1()
    {
        $resultadoAcetylation = acetylation('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoAcetylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoAcetylation)); // No debe ser vacío
    }   

    public function testAcetylation2()
    {
        $resultadoAcetylation= acetylation('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoAcetylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoAcetylation)); // No debe ser vacío
    } 

        public function testMethylation1()
    {
        $resultadoMethylation = methylation('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoMethylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoMethylation)); // No debe ser vacío
    }   

    public function testMethylation2()
    {
        $resultadoMethylation= methylation('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoMethylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoMethylation)); // No debe ser vacío 
    }   

    public function testUbiquitination1()
    {
        $resultadoUbiquitination = ubiquitination('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoUbiquitination); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoUbiquitination)); // No debe ser vacío
    }   

    public function testUbiquitination2()
    {
        $resultadoUbiquitination = ubiquitination('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoUbiquitination); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoUbiquitination)); // No debe ser vacío 
    }   

    public function testSumoylation1()
    {
        $resultadoSumoylation = sumoylation('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoSumoylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoSumoylation)); // No debe ser vacío
    }   

    public function testonSumoylation2()
    {
        $resultadoSumoylation = sumoylation('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoSumoylation); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoSumoylation)); // No debe ser vacío 
    }   

    public function testOGlcNAc1()
    {
        $resultadoOGlcNAc = oglcnac('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoOGlcNAc); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoOGlcNAc)); // No debe ser vacío
    }   

    public function testOGlcNAc2()
    {
        $resultadoOGlcNAc = acetylation('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoOGlcNAc); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoOGlcNAc)); // No debe ser vacío 
    } 

    public function testRegPTM1()
    {
        $resultadoRegPTM = regptm('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoRegPTM); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoRegPTM)); // No debe ser vacío
    }   

    public function testRegPTM2()
    {
        $resultadoRegPTM = regptm('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoRegPTM); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoRegPTM)); // No debe ser vacío 
    } 

    public function testDisease1()
    {
        $resultadoDisease = disease('P01009'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoDisease); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoDisease)); // No debe ser vacío
    }   

    public function testDisease2()
    {
        $resultadoDisease = disease('P00004'); // Proteína patrón (arbitraria) para testeo
        $this->assertInternalType('array', $resultadoDisease); // La función devuelve un array
        $this->assertGreaterThan(0, count($resultadoDisease)); // No debe ser vacío 
    } 

    public function testSpecies()
    {
        $a = species();
        $this->assertInternalType('array', $a);     
        $this->assertGreaterThanOrEqual(4, count($a)); // More than 8 species
    }

    public function testSpeciesContain()
    {
        $a = species();
        $this->assertArrayHasKey('species', $a[0]); // Each element
        $this->assertArrayHasKey('proteins', $a[0]); // has these three
        $this->assertArrayHasKey('sites', $a[0]); // keys
    }

    public function testOxidant()
    {
        $a = oxidant();
        $this->assertInternalType('array', $a);
        $this->assertGreaterThanOrEqual(4, count($a)); // Morte than 5 oxidants
    }

    public function testOxidantContain()
    {
        $a = oxidant();
        $this->assertArrayHasKey('oxidant', $a[0]); // Each element has
        $this->assertArrayHasKey('sites', $a[0]); // these keys
    }

    public function testReferences()
    {
        $a = references();
        $this->assertInternalType('array', $a);
        $this->assertGreaterThanOrEqual(7, count($a)); // More than 7 lines 
    }

    public function testReferencesContain()
    {
        $a = references();
        $this->assertArrayHasKey('title', $a[0]); // Each element 
        $this->assertArrayHasKey('proteins', $a[0]);  // has these
        $this->assertArrayHasKey('sites', $a[0]); // three keys

    }

    public function testGetallsites()
    {
        $a = getallsites();
        $this->assertInternalType('array', $a);
        $this->assertGreaterThanOrEqual(7, count($a)); // More than 7 sites

    }

    public function testGetonesite()
    {
        $a = getonesite('P01009', 351);
        $this->assertInternalType('array', $a); // La función devuelve un array
        $this->assertGreaterThan(0, count($a)); // Existe una entrada para este sitio
        $this->assertEquals('P01009', $a[0]['prot_id']); // El ID de la proteína es el esperado

    }

}




