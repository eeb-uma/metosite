<?php

use PHPUnit\Framework\TestCase;

class metosite4Test extends TestCase
{
	public function testmapping_functional_category0()
	{
		$c = mapping_functional_categories();
		$this->assertInternalType('array', $c);
		$this->assertContainsOnly('int', $c);
        $this->assertCount(0, $c);
        $this->assertEquals(array(), $c);
	}
	
	public function testmapping_functional_category1()
	{
		$c = mapping_functional_categories($group1 = TRUE, $group2 = FALSE, $group3 = FALSE);
		$this->assertInternalType('array', $c);
		$this->assertContainsOnly('int', $c);
        $this->assertCount(1, $c);
        $this->assertEquals(array(1), $c);
	}	

	public function testmapping_functional_categories1_2()
	{
		$c = mapping_functional_categories($group1 = TRUE, $group2 = TRUE, $group3 = FALSE);
		$this->assertInternalType('array', $c);
		$this->assertContainsOnly('int', $c);
        $this->assertCount(2, $c);
        $this->assertEquals(array(1,2), $c);
	}	

	public function testmapping_functional_categories1_2_3()
	{
		$c = mapping_functional_categories($group1 = TRUE, $group2 = TRUE, $group3 = TRUE);
		$this->assertInternalType('array', $c);
		$this->assertContainsOnly('int', $c);
        $this->assertCount(65, $c);
        $this->assertEquals(range(1,65), $c);
	}	

	public function testmapping_functional_categories3()
	{
		$c = mapping_functional_categories($group1 = FALSE, $group2 = FALSE, $group3 = TRUE,
			$gain_act=array(1), $loss_act=array(1), 
			$gain_ppi=array(1), $loss_ppi=array(1),
			$stability=array(1), $localization=array(1));
		$this->assertInternalType('array', $c);
		$this->assertContainsOnly('int', $c);
        $this->assertCount(1, $c);
        $this->assertEquals(array(65), $c);
	}	
}

class metosite5Test extends TestCase
{
	public function testinverse_mapping()
	{
		$c = inverse_mapping(rand(1,65));
		$this->assertInternalType('array', $c);
		$this->assertContainsOnly('int', $c);
		$this->assertCount(6, $c);
	}

}
