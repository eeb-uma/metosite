<?php
/** 
*	Tnis script takes the array returned by 'scan()', which is
*   a quite redundant table (data for the same protein repited
*   in every row), and return a non-redundant report with json
*   format.
*/

require_once('data.functions.php');

function GetSiteJSON($protid, $met_pos)
{

    $result = getonesite($protid, $met_pos);

    return json_encode($result);

}