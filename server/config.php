<?php
/**
 * Configuration for database connection
 *
 */

$host = getenv('METOSITE_DB_HOST') ?: "127.0.0.1";
$port = getenv('METOSITE_DB_PORT') ?: '3306';
$username = getenv('METOSITE_DB_USER') ?: 'root';
$password = getenv('METOSITE_DB_PASS') ?: 'root';
$dbname = getenv('METOSITE_DB_NAME') ?: 'metosite';
$slim_displayErrorDetails = getenv('METOSITE_SLIM_DISPLAY_ERROR_DETAILS') ?: true;
$slim_addContentLenghtHeader = getenv('METOSITE_SLIM_ADD_CONTENT_LENGHTHEADER') ?: false;
$options	= array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
