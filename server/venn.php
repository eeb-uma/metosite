<?php

require_once('data.functions.php');
require_once('mapping_functional_categories.php');

function getVennSummary(){


    $basic_summary = array(
        "Not addressed" => 0,
        "No effect found" => 0,
        "Gain of Activity" => 0,
        "Loss of Activity" => 0,
        "Gain of Protein-Protein Interaction" => 0,
        "Loss of Protein-Protein Interaction" => 0,
        "Effect on Protein Stability" => 0,
        "Effect on Subcellular Location" => 0
    );

    $venn_summary = array(
    );

    // Count all categories
    $temp_venn_summary = array();
    foreach(getallsites() as $site){
        if(!$temp_venn_summary[$site['reg_id']]) {
            $temp_venn_summary[$site['reg_id']] = 1;
        } else {
            $temp_venn_summary[$site['reg_id']] += 1;
        }
    }
    
    foreach($temp_venn_summary as $fc => $count){
        $item_label_array = label_functional_category($fc);
        if (count($item_label_array) > 1){
            foreach($item_label_array as $basic_label){
                $basic_summary[$basic_label] += $count;
            }
            $item = array(
                "sets" => label_functional_category($fc),
                "size" => $count,
                "debug" => $fc
            );
        } elseif(count($item_label_array) == 1 && $item_label_array[0] != "Not addressed") {
            $basic_summary[$item_label_array[0]] += $count;
        } else {
            // New category?
            // This is unhandled
        }
        
        if ($item) {
            array_push($venn_summary, $item);
        }
    }

    foreach($basic_summary as $l => $v){
        array_push($venn_summary, array("sets" => [$l], "size" => $v));
    }

    //return [$temp_venn_summary,$venn_summary];
    return $venn_summary;
}