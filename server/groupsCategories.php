<?php
/** -------- Keep your code DRY ------------ 
* This function, 'groupsCategories' will be used
* at least twice in api.php (Get/mapping and 
* Get/mp_and_search). It takes two string, the
* first $groups is a three digits number to
* indicate whether each of the three Functional
* Groups are or not selected (encode by 1 and0,
* respectively). For instance, $groups = 011 
* means that we are selecting for Groups 2 and
* 3, but nor for Group 1. The second argument
* of this function is $categories, a 6 digits
* number, encoding for gain_act, loss_act,
* gain_ppi, loss_ppi, stability and localization.
* for instance $categories = 011112 means
* $gain_act = array(0), $loss_act = array(1),
* $gain_ppi = array(1), $loss_ppi = array(1),
* $stability = array(1), $localization = 
* array(0,1). The function returns a subset
* of the 65 Functional Categories.
*
*/

function groupsCategories($groups, $categories)
{
    $g = str_split($groups);
    $group1 = $g[0];
    $group2 = $g[1];
    $group3 = $g[2];

    $c = str_split($categories);

    if ($c[0] == 1){
        $gain_act = array(1);
    } elseif ($c[0] == 0){
        $gain_act = array(0);
    } else {
        $gain_act = array(0,1);
    }

    if ($c[1] == 1){
        $loss_act = array(1);
    } elseif ($c[1] == 0){
        $loss_act = array(0);
    } else {
        $loss_act = array(0,1);
    }

    if ($c[2] == 1){
        $gain_ppi = array(1);
    } elseif ($c[2] == 0){
        $gain_ppi = array(0);
    } else {
        $gain_ppi = array(0,1);
    }

    if ($c[3] == 1){
        $loss_ppi = array(1);
    } elseif ($c[3] == 0){
        $loss_ppi = array(0);
    } else {
        $loss_ppi = array(0,1);
    }

    if ($c[4] == 1){
        $stability = array(1);
    } elseif ($c[4] == 0){
        $stability = array(0);
    } else {
        $stability = array(0,1);
    }

    if ($c[5] == 1){
        $localization = array(1);
    } elseif ($c[5] == 0){
        $localization = array(0);
    } else {
        $localization = array(0,1);
    }
   
   $results = array($group1, $group2, $group3, $gain_act, $loss_act, $gain_ppi, $loss_ppi, $stability, $localization);
   return($results);
} 
