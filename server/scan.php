<?php
/** 
*	Tnis script takes the array returned by 'scan()', which is
*   a quite redundant table (data for the same protein repited
*   in every row), and return a non-redundant report with json
*   format.
*/

require_once('data.functions.php');

function ScanGetJSON($protid)
{
	$r = scan($protid);

	// Fields from the table Protein:
	$report = array 
	(
	"prot_id" => $r[0]["prot_id"], 					
	"prot_name" => $r[0]["prot_name"], 
	"prot_nickname" => $r[0]["prot_nickname"],  
	"gene_name" => $r[0]["gene_name"], 
	"prot_sp" => $r[0]["prot_sp"],
	"prot_sub" => $r[0]["prot_sub"], 
	"prot_pdb" => $r[0]["prot_pdb"], 
	"prot_seq" => $r[0]["prot_seq"], 
	"prot_note" => $r[0]["prot_note"]
	);


	// Fields related to metosites:
	$metosites = array();
	$counter = 0;
	for ($i = 0; $i < count($r); $i++){

		if ($i == 0){
		
			$current_met = $r[0]["met_pos"];
			$reference = array([
				"href" => $r[0]["href"],
				"title" => $r[0]["title"]]);
			$metosites[] = array(
								'met_id' => $r[0]['met_id'],
								'met_pos'=>$current_met,
								'reg_id' =>$r[0]['reg_id'],
								'org_oxidant'=>$r[0]['org_oxidant'],
								'met_vivo_vitro'=>$r[0]['met_vivo_vitro'],
								'org_tissue'=>$r[0]['org_tissue'],
								'References'=> $reference
							);
		} else {

			// Other MetO in the same protein 
			if ($r[$i]['met_pos'] != $current_met){

				$counter++;
				$current_met = $r[$i]['met_pos'];
				$reference = array([
					"href" => $r[$i]["href"],
					"title" => $r[$i]["title"]]);
				$metosites[] = array(
								'met_id' => $r[$i]['met_id'],
								'met_pos'=>$current_met,
								'reg_id' =>$r[$i]['reg_id'],
								'org_oxidant'=>$r[$i]['org_oxidant'],
								'met_vivo_vitro'=>$r[$i]['met_vivo_vitro'],
								'org_tissue'=>$r[$i]['org_tissue'],
								'References'=> $reference
								);
			} else {
				// Other Reference for the same MetO		
				array_push($reference, ["href" => $r[$i]['href'], "title" => $r[$i]['title']]);
				$metosites[$counter]['References'] = $reference;

			}
		}
	}		
	$report['Metosites'] = $metosites;

	// Other PTMs incorporated to the report
	 $report["Phosphorylation"] = phosphorylation($protid); 
	 $report["Acetylation"] = acetylation($protid);
	 $report["Methylation"] = methylation($protid);
	 $report["Ubiquitination"] = ubiquitination($protid);
	 $report["Sumoylation"] = sumoylation($protid);
	 $report["OGlcNAc"] = oglcnac($protid);
	 $report["RegPTM"] = regptm($protid);
	 $report["Disease"] = disease($protid); 


	$report = json_encode($report);
	return($report);
	
}

