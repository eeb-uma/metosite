<?php
require('config.php');

function my_connection($localdbname=null)
{
	global $host, $port, $username, $password, $dbname, $options;
	$dbname = isset($localdbname) ? $localdbname : $dbname;
	try
	{
		$connection = new PDO("mysql:host=$host:$port;dbname=$dbname", $username, $password, $options);
		return $connection;
	}
	catch(PDOException $error) 
	{
		throw $error;
	}	
}

/**
*   The scan() function takes as argument a Uniprot ID. 
*   Afterwards the function  carries out a query
*   for that protein into MetOSite. The result of this
*   query is returned as an array calle $result
*/

function scan($protid)
{
	$connection = my_connection();

	$sql = "SELECT Protein.prot_id, Protein.prot_name, Protein.prot_nickname, 
			Protein.gene_name, Protein.prot_sp, Protein.prot_sub, Protein.prot_pdb,
			Protein.prot_seq, Protein.prot_note, Methionine.met_pos, Methionine.met_id,
			Regulatory.reg_id, Organism.org_oxidant, Methionine.met_vivo_vitro, 
            Organism.org_tissue, DOI.ref, DOI.href, DOI.title
            FROM Protein JOIN Methionine ON Protein.prot_id = Methionine.prot_id
			JOIN Organism ON Organism.org_id = Methionine.org_id
			JOIN Regulatory ON Regulatory.reg_id = Methionine.reg_id
			JOIN Evidence ON Methionine.met_id = Evidence.met_id
            JOIN DOI ON DOI.doi_id = Evidence.doi_id
            WHERE Protein.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$result = $statement->fetchAll();
   	
	return $result;	
}


/**
*   The pname() function takes as argument a character 
*   string. The function returns an array containing all
*   the proteins whose name contains the character string
*   used as argument. 
*/

function pname($name){

	$connection = my_connection();

	$protname = '%'.$name.'%'; // provides flexibility in the search using 'LIKE'

	$sql = "SELECT Protein.prot_id, Protein.prot_name, Protein.prot_sp
            FROM Protein WHERE Protein.prot_name LIKE :protname";


	$statement = $connection->prepare($sql);
	$statement->bindParam(':protname', $protname, PDO::PARAM_STR);
	$statement->execute();

	$result = $statement->fetchAll();
   	
	return $result;	

}

/**
*
*   The getallprot() returns an array with all the Uniprot 
*   ID of all the proteins currently found into MetOSite.
* 
*/

function getallprot(){

	$connection = my_connection();

	// $sql = "SELECT Protein.prot_id FROM Protein WHERE";
	$sql = "SELECT Protein.prot_id FROM Protein";

	$statement = $connection->prepare($sql);
	$statement->execute();

	$result = $statement->fetchAll();
	
	
   	foreach($result as $value){
		$output[] = $value['prot_id'];
	}
	$mer = implode($output, "\n");

	//echo "\n";
	//echo $mer;
	return $mer;

}


/* *
*
*	 Function to search for other PTMs:
*			* phosphorylation
*			* acetylation
*			* methylation
*			* ubiquitination
*			* sumoylation
*			* oglcnac
*			* regptm
*			* disease
*
*/

function phosphorylation($protid)
{
	$connection = my_connection();

	$sql = "SELECT Phosphorylation.p_res FROM Phosphorylation
			WHERE Phosphorylation.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}


function acetylation($protid)
{
	$connection = my_connection();

	$sql = "SELECT Acetylation.ac_res FROM Acetylation
			WHERE Acetylation.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}

function methylation($protid)
{
	$connection = my_connection();
	
	$sql= "SELECT Methylation.m_res FROM Methylation
			WHERE Methylation.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}

function ubiquitination($protid)
{
	$connection = my_connection();

	$sql = "SELECT Ubiquitination.u_res FROM Ubiquitination
			WHERE Ubiquitination.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}

function sumoylation($protid)
{
	$connection = my_connection();

	$sql = "SELECT Sumoylation.s_res FROM Sumoylation
			WHERE Sumoylation.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}

function oglcnac($protid)
{
	$connection = my_connection();

	$sql = "SELECT OGlcNAc.g_res FROM OGlcNAc
			WHERE OGlcNAc.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}

function regptm($protid)
{
	$connection = my_connection();

	$sql = "SELECT RegPTM.r_res FROM RegPTM
			WHERE RegPTM.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}

	return $result;	
}

function disease($protid)
{
	$connection = my_connection();

	$sql = "SELECT Disease.di_res FROM Disease
			WHERE Disease.prot_id = :protid";

	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->execute();

	$r = $statement->fetchAll();

	$result = array();
	foreach ($r as $value) {
		array_push($result, $value[0]);
	}
	
	return $result;	
}

/* *
*
*	 The search() function takes three arguments: (i) $functional_category, which
*    is an array containing any subset from the functional categories; 
*    (ii) $organism which can take the value -1 to indicate that all organism
*    need to be selected or can take the name of a taxon (either a Domain, Kingdom,
*	 Phylum or Species). The third argument is (iii) $oxidant, again -1 when all
* 	 the oxidants are selected or the name of the oxdiant. 
*
*	 E.g.:     search(array(2,18), 'Oryctolagus cuniculus', -1) 
*
*    The result of this query is returned as an array called $result.
*
*	 The following page was very useful to bind an array to an IN() condition:
* https://stackoverflow.com/questions/920353/can-i-bind-an-array-to-an-in-condition
*
*/

function search($functional_category, $organism, $oxidant)
{
	$connection = my_connection();

	// --------------- Common lines for all the queries --------------------//
	// Four possible queries. The following lines are common to all the cases

	foreach($functional_category as &$val)
			$val = $connection->quote($val); //iterate through array and quote
	$in = implode(',', $functional_category); //create comma separated         

	$sql_constante = 'SELECT Protein.prot_name, Protein.prot_id, Methionine.met_pos, 
						Methionine.met_vivo_vitro, Methionine.reg_id,  Organism.org_sp,
				    	Organism.org_oxidant
				    	FROM Methionine  JOIN Protein ON Methionine.prot_id = Protein.prot_id
				    	JOIN Organism  ON Methionine.org_id = Organism.org_id
				    	JOIN Taxon ON Taxon.org_sp = Organism.org_sp ';
		
	//--------------------------- End of the common part -------------------//
				    	

	if ($organism != -1 & $oxidant != -1){ // 1. ONE ORGANISM & ONE OXIDANT

		$oxidant = '%'.$oxidant.'%'; // provides flexibility in the search using 'LIKE'

		$sql_variable = 'WHERE (Taxon.domain = :organism OR Taxon.kingdom = :organism 
				    	OR Taxon.phylum = :organism OR Taxon.org_sp = :organism)
				    	AND Organism.org_oxidant LIKE :oxidant
				    	AND Methionine.reg_id IN (' .$in. ')'; 
				    
		$sql = $sql_constante.$sql_variable; 
		
		$statement = $connection->prepare($sql);
		$statement->bindParam(':organism', $organism, PDO::PARAM_STR);
		$statement->bindParam(':oxidant', $oxidant, PDO::PARAM_STR);

	} elseif ($organism != -1 & $oxidant == -1){ // 2. ONE ORGANISM & ALL OXIDANTS

		$sql_variable = 'WHERE (Taxon.domain = :organism OR Taxon.kingdom = :organism 
				    	OR Taxon.phylum = :organism OR Taxon.org_sp = :organism)
				    	AND Methionine.reg_id IN (' .$in. ')'; 
				    	
		$sql = $sql_constante.$sql_variable; 

		$statement = $connection->prepare($sql);
		$statement->bindParam(':organism', $organism, PDO::PARAM_STR);

	} elseif ($organism == -1 & $oxidant != -1){ // 3. ALL ORGANISM & ONE OXIDANT
		
		$oxidant = '%'.$oxidant.'%'; // provides flexibility in the search using 'LIKE'

		$sql_variable = 'WHERE Organism.org_oxidant LIKE :oxidant
				    	AND Methionine.reg_id IN (' .$in. ')'; 

		$sql = $sql_constante.$sql_variable; 

		$statement = $connection->prepare($sql);
		$statement->bindParam(':oxidant', $oxidant, PDO::PARAM_STR);

	} elseif ($oxidant == -1 & $organism == -1){ // 4. ALL ORGANISMS  & ALL OXIDANTS

		$sql_variable = 'WHERE Methionine.reg_id IN (' .$in. ')'; 

		$sql = $sql_constante.$sql_variable; 

		$statement = $connection->prepare($sql);

	} 

	$statement->execute();
	$result = $statement->fetchAll();		
					
	return $result;
} 

/**
 *  The new search function, search2(), accepts a query string in the format:
 *  organism=<org1>,<org2>&specie[]=sp1 ...
 *  The accepted parametes are:
 * 
 *  group
 *  property
 *  specie
 *  oxidant
 *  format
 *  reference
 
 *  
 *  Array are serialized by comma separated items
 */


/**
 *  Auxiliar function for search funtions
 *  */ 

function queryFilters($vars, $functional_category){

	$query_filters = "";
	$filters = false;
	function quote_string_elements($n)
	{
		return('"'.str_replace(',','","',str_replace(', ',',',$n).'"'));
	}

	# Specie
	if ($vars['specie']){
		$species = quote_string_elements($vars['specie']);
		$specie_filter = '(
		Taxon.domain IN ('.$species.') 
		OR Taxon.kingdom IN ('.$species.') 
		OR Taxon.phylum IN ('.$species.') 
		OR Taxon.org_sp IN ('.$species.')
		)';
	$query_filters .= 'AND '.$specie_filter;
	$filters = true;
	}

	# Oxidant
	if ($vars['oxidant']){
		$oxidant_array = explode(',', $vars['oxidant']);
		$oxidant_filter = ' AND (';
		$filters = true;
		$count_first = true;
		foreach ($oxidant_array as $oxidant){
			if ($count_first) {
				$oxidant_filter .= 'Organism.org_oxidant LIKE "%'.$oxidant.'%"';
				$count_first = false;
			} else {
			$oxidant_filter .= ' OR Organism.org_oxidant LIKE "%'.$oxidant.'%"';
			}
		}
		$query_filters .= $oxidant_filter.')';
	}

	# Functional category
	// foreach($functional_category as &$val)
	// 		$val = $connection->quote($val); //iterate through array and quote
	$in = implode(',', $functional_category); //create comma separated 

	# Reference search
	if ($vars['reference']) {
	    $query_filters .= "AND DOI.doi_id IN (".quote_string_elements($vars['reference']).")";
	}
	return [$query_filters, $in];
}

function search2($vars, $functional_category){

	$connection = my_connection();

	$query_filters = queryFilters($vars, $functional_category);
	
	$sql = 'SELECT Protein.prot_name, Protein.prot_id, Methionine.met_id, 
			Methionine.met_pos, Methionine.met_vivo_vitro, Methionine.reg_id,  
			Organism.org_sp, Organism.org_oxidant, 
			GROUP_CONCAT(DISTINCT(DOI.href)) AS reference_links, 
			GROUP_CONCAT(DISTINCT(DOI.title)) AS reference_titles
			FROM Methionine JOIN Protein ON Methionine.prot_id = Protein.prot_id
			JOIN Organism ON Methionine.org_id = Organism.org_id
			JOIN Taxon ON Taxon.org_sp = Organism.org_sp 
			JOIN Evidence ON Evidence.met_id = Methionine.met_id
			JOIN DOI ON DOI.doi_id = Evidence.doi_id
			WHERE Methionine.reg_id IN (' .$query_filters[1]. ')'.$query_filters[0].
			'GROUP BY Methionine.met_id';
	
	$statement = $connection->prepare($sql);
	$statement->execute();
	$result = $statement->fetchAll();
	return $result;
}

/*
** search3 is search2 plus pagination
*/

function search3($vars, $functional_category){

	$connection = my_connection();

	$query_filters = queryFilters($vars, $functional_category);

	$page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
	$limit = isset($_GET['limit']) ? $_GET['limit'] : 20;

	$offset = (--$page) * $limit;

	$countQuery = $connection->prepare('SELECT COUNT(DISTINCT(Methionine.met_id)) AS total
		FROM Methionine
		JOIN Protein ON Methionine.prot_id = Protein.prot_id
		JOIN Organism ON Methionine.org_id = Organism.org_id
		JOIN Taxon ON Taxon.org_sp = Organism.org_sp
		JOIN Evidence ON Evidence.met_id = Methionine.met_id
		JOIN DOI ON DOI.doi_id = Evidence.doi_id
		WHERE Methionine.reg_id IN (' .$query_filters[1]. ')'.$query_filters[0]);
	$dataQuery  = $connection->prepare('SELECT Protein.prot_name, Protein.prot_id, Methionine.met_id, 
		Methionine.met_pos, Methionine.met_vivo_vitro, Methionine.reg_id,  
		Organism.org_sp, Organism.org_oxidant, 
		GROUP_CONCAT(DISTINCT(DOI.href)) AS reference_links, 
		GROUP_CONCAT(DISTINCT(DOI.title)) AS reference_titles
		FROM Methionine JOIN Protein ON Methionine.prot_id = Protein.prot_id
		JOIN Organism ON Methionine.org_id = Organism.org_id
		JOIN Taxon ON Taxon.org_sp = Organism.org_sp 
		JOIN Evidence ON Evidence.met_id = Methionine.met_id
		JOIN DOI ON DOI.doi_id = Evidence.doi_id
		WHERE Methionine.reg_id IN (' .$query_filters[1]. ')'.$query_filters[0].
		'GROUP BY Methionine.met_id LIMIT :limit OFFSET :offset');
	$dataQuery->bindValue(':limit', $limit, \PDO::PARAM_INT);
	$dataQuery->bindValue(':offset', $offset, \PDO::PARAM_INT);

	try {
		$data = $dataQuery->execute();
		$data = $dataQuery->fetchAll();
		$count = $countQuery->execute();
		$count = $countQuery->fetchObject();
	} catch(PDOException $e) {
		die('Error.');
	}
	$result = new \stdClass();
	$result->data = $data;
	$result->count = $count->total;
	return $result;
}

/**
 * Search for proteins instead sites
 */

function searchProteins($vars, $functional_category){

	$connection = my_connection();

	$query_filters = queryFilters($vars, $functional_category);

	$sql = "SELECT Protein.prot_name, 
			Protein.prot_id, 
			GROUP_CONCAT(DISTINCT(Methionine.met_pos)) AS met_pos,
			GROUP_CONCAT(DISTINCT(Organism.org_sp)) AS species,
			GROUP_CONCAT(DISTINCT(Organism.org_oxidant)) AS oxidants, 
			GROUP_CONCAT(DISTINCT(DOI.href)) AS reference_links, 
			GROUP_CONCAT(DISTINCT(DOI.title)) AS reference_titles
			FROM Protein 
			JOIN Methionine ON Protein.prot_id = Methionine.prot_id
			JOIN Organism ON Methionine.org_id = Organism.org_id
			JOIN Taxon ON Taxon.org_sp = Organism.org_sp 
			JOIN Evidence ON Evidence.met_id = Methionine.met_id
			JOIN DOI ON DOI.doi_id = Evidence.doi_id
			WHERE Methionine.reg_id IN (" .$query_filters[1]. ")".$query_filters[0]."
			GROUP BY Protein.prot_id";

	$statement = $connection->prepare($sql);
	$statement->execute();
	$result = $statement->fetchAll();
	return $result;

}

/**
 * Search for proteins instead sites. Paginated
 */

function searchProteins2($vars, $functional_category){

	$connection = my_connection();

	$query_filters = queryFilters($vars, $functional_category);

	$page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 20;

    $offset = (--$page) * $limit;

	$countQuery = $connection->prepare('SELECT COUNT(DISTINCT(Protein.prot_id)) AS total
			FROM Protein 
			JOIN Methionine ON Protein.prot_id = Methionine.prot_id
			JOIN Organism ON Methionine.org_id = Organism.org_id
			JOIN Taxon ON Taxon.org_sp = Organism.org_sp
			JOIN Evidence ON Evidence.met_id = Methionine.met_id
			JOIN DOI ON DOI.doi_id = Evidence.doi_id
			WHERE Methionine.reg_id IN (' .$query_filters[1]. ')'.$query_filters[0]);
	$dataQuery  = $connection->prepare("SELECT Protein.prot_name, 
			Protein.prot_id, 
			GROUP_CONCAT(DISTINCT(Methionine.met_pos)) AS met_pos,
			GROUP_CONCAT(DISTINCT(Organism.org_sp)) AS species,
			GROUP_CONCAT(DISTINCT(Organism.org_oxidant)) AS oxidants, 
			GROUP_CONCAT(DISTINCT(DOI.href)) AS reference_links, 
			GROUP_CONCAT(DISTINCT(DOI.title)) AS reference_titles
			FROM Protein 
			JOIN Methionine ON Protein.prot_id = Methionine.prot_id
			JOIN Organism ON Methionine.org_id = Organism.org_id
			JOIN Taxon ON Taxon.org_sp = Organism.org_sp 
			JOIN Evidence ON Evidence.met_id = Methionine.met_id
			JOIN DOI ON DOI.doi_id = Evidence.doi_id
			WHERE Methionine.reg_id IN (" .$query_filters[1]. ")".$query_filters[0]."
			GROUP BY Protein.prot_id LIMIT :limit OFFSET :offset");
	$dataQuery->bindValue(':limit', $limit, \PDO::PARAM_INT);
	$dataQuery->bindValue(':offset', $offset, \PDO::PARAM_INT);

	try {
		$data = $dataQuery->execute();
		$data = $dataQuery->fetchAll();
		$count = $countQuery->execute();
		$count = $countQuery->fetchObject();
	} catch(PDOException $e) {
		die('Error.');
	}
	$result = new \stdClass();
	$result->data = $data;
	$result->count = $count->total;
	return $result;
}


/* *
*	 The getallsites() returns an array with all the MetO sites
*    currently found into MetOSite, providing info regarding
*    prot_name, prot_id, prot_sp, met_pos, met_vivo_vitro, 
*    reg_id (FC), org_oxidant and references.
*	 
*/

function getallsites()
{
	$connection = my_connection();     

	$sql = 'SELECT Protein.prot_name, Protein.prot_id, Protein.prot_sp,  
						Methionine.met_pos, Methionine.met_vivo_vitro, Methionine.reg_id,  
				    	Organism.org_oxidant, DOI.pmid, DOI.title
				    	FROM Methionine  JOIN Protein ON Methionine.prot_id = Protein.prot_id
				    	JOIN Evidence ON Methionine.met_id = Evidence.met_id
				    	JOIN DOI ON DOI.doi_id = Evidence.doi_id
				    	JOIN Organism  ON Methionine.org_id = Organism.org_id';

	$statement = $connection->prepare($sql);
	$statement->execute();
	$result = $statement->fetchAll(); // query result

	foreach($result as $value){
		$output[] = array('prot_id' => $value['prot_id'],
						  'prot_name' => $value['prot_name'],
						  'prot_sp' => $value['prot_sp'],
						  'met_pos' =>$value['met_pos'],
						  'met_vivo_vitro' =>$value['met_vivo_vitro'],
						  'reg_id' =>$value['reg_id'],
						  'org_oxidant' =>$value['org_oxidant'],
						  'pmid' =>$value['pmid'],
						  'title' =>$value['title']);
	}

	return $output;
} 

/* *
*	 The getonesite(site) returns an array providing info regarding
*    prot_name, prot_id, prot_sp, met_pos, met_vivo_vitro, 
*    reg_id (FC), org_oxidant and references.
*	 
*/

function getonesite($protid, $metpos)
{
	$connection = my_connection();     

	$sql = 'SELECT Protein.prot_id, Protein.prot_name, Protein.prot_sp, Methionine.met_pos,  
				   Methionine.met_vivo_vitro, Methionine.reg_id, Organism.org_oxidant, DOI.href, DOI.title
				   FROM Methionine  JOIN Protein ON Methionine.prot_id = Protein.prot_id
				   JOIN Evidence ON Methionine.met_id = Evidence.met_id
				   JOIN DOI ON DOI.doi_id = Evidence.doi_id
				   JOIN Organism  ON Methionine.org_id = Organism.org_id
				   WHERE Protein.prot_id = :protid AND Methionine.met_pos = :metpos';


	$statement = $connection->prepare($sql);
	$statement->bindParam(':protid', $protid, PDO::PARAM_STR);
	$statement->bindParam(':metpos', $metpos, PDO::PARAM_STR);
	$statement->execute();
	$result = $statement->fetchAll(); // query result


	foreach($result as $value){
		$output[] = array('prot_id' => $value['prot_id'],
						  'prot_name' => $value['prot_name'],
						  'prot_sp' => $value['prot_sp'],
						  'met_pos' =>$value['met_pos'],
						  'met_vivo_vitro' =>$value['met_vivo_vitro'],
						  'reg_id' =>$value['reg_id'],
						  'org_oxidant' =>$value['org_oxidant'],
						  'href' =>$value['href'],
						  'title' =>$value['title']);
	}



	return $output;
}



/** 
*
*	This script carries out a SQL query that will return
*   a table like:
*
*   prot_id    prot_sp
*   P01009	   Homo sapiens
*   P01009     Homo sapiens
*	P00004	   Equus caballus
*   ...		   ...
*   
*   Afterwards, the function builds and returns an array that, 
*   when converted to json, will look like:
*
*  [
*  {species: "Homo sapiens", proteins: 1, sites: 2},
*  {species: "Equus caballus", proteins: 1, sites: 1},
*  ]
* 	
*/

function species()
{
	$connection = my_connection();

	$sql = "SELECT Methionine.prot_id, Protein.prot_sp 
            FROM Methionine JOIN Protein ON Methionine.prot_id = Protein.prot_id";
			
	$statement = $connection->prepare($sql);
	$statement->execute();
	$a = $statement->fetchAll(); // query result

	foreach ($a as $clave => $fila){
		$protid[$clave] = $fila['prot_id'];
		$species[$clave] = $fila['prot_sp'];
	}
	asort($species); // sort alphabeticaly the species


	foreach ($a as $clave => $fila) {
   		$protein[ $fila['prot_id'] ] = $fila['prot_sp'];
	}
	asort($protein); // sort alphabeticaly the proteins

	$meto_entries = array_count_values($species);
	$prot_entries = array_count_values($protein);

	// $output = array('Proteins' => $prot_entries, 'Sites' => $meto_entries);

	$list_species = array_keys($prot_entries);
	$number_proteins_each_species = array_values($prot_entries);
	$number_sites_each_species =  array_values($meto_entries);
	$number_species = count($list_species);

	$output = array();
	for ($i = 0; $i < $number_species; $i++){
		$output[] = array('species' => $list_species[$i],
			  'proteins' => $number_proteins_each_species[$i],
			  'sites' => $number_sites_each_species[$i]);
	}

	return $output;

}

/** 
*
*	This script carries out a SQL query that will return
*   a table like:
*
*   met_id 	org_oxidant   
*   2994	taurine chloramine (TnCl)
*	2995	hydrogen peroxide (H2O2)
*   ...		...
*	2997	taurine chloramine (TnCl)
*	2998	hypochlorite (HClO)
*	...		...		...
*
*   Afterwards, the script build an array that when
*   converted to json looks like:
*  [
*  {"oxidant": "taurine chloramine (TnCl)", "number of sites": 2}, 
*  {"oxidant": "hydrogen peroxide (H2O2)", "number of sites2: 1}, 
*  {"oxidant2: "hypochlorite (HClO)", "number of sites": 1}, 
*  ]
* 	
*/

function oxidant()
{
	$connection = my_connection();

	$sql = "SELECT Methionine.met_id, Organism.org_oxidant 
			FROM Methionine JOIN Organism ON Methionine.org_id = Organism.org_id";
			
	$statement = $connection->prepare($sql);
	$statement->execute();
	$a = $statement->fetchAll(); // query result

	foreach ($a as $clave => $fila){
		$oxidant[$clave] = $fila['org_oxidant'];
	}
	asort($oxidant); // sort alphabeticaly the oxidants

	$oxi = array_count_values($oxidant);
	// the key is the name of the oxidant and the value its frequency

	$ox = array();
	foreach($oxidant as $key){
		$t = explode(",", $key);
		foreach($t as $k){
			array_push($ox, trim($k));
		}
	}
	$ox = array_count_values($ox);
	foreach($ox as $k => $v){
		$output[]=array('oxidant' => $k,
	                    'sites' => $v);
	}
	return($output);
}

/** 
*
*	This script carries out a SQL query that will return
*   a table like:
*
*   prot_id    		href 				title
*   A0A0E1M553	   	https://... 1 	 	Methionine oxidation activates a transcription ...
*   A0A0E1M553     	https://... 1	 	Methionine oxidation activates a transcription ...
*	A0A0E1M553	   	https://... 1	 	Methionine oxidation activates a transcription ...
*   A2RRP1 			https://... 2		Redox proteomics of protein-bound methionine ...
*   A2RRP1 			https://... 2		Redox proteomics of protein-bound methionine ...
*   XXXXXX 			https://...	2		Redox proteomics of protein-bound methionine ...
*   	...				... 								   ...							
*   
*   
*   Afterwards, the function builds and returns an array that, 
*   when converted to json, will look like:
*
*  [
*  {title: "Methionine oxidation activates a ...", proteins: 1, sites: 3, link: https://... 1},
*  {title: "Redox proteomics of protein-bound ...", proteins: 2, sites: 3, link: https://... 2},
*  ...
*  ]
* 	
*/


function references()
{
	$connection = my_connection();

	$sql = "SELECT Methionine.prot_id, DOI.pmid, DOI.title, DOI.href, DOI.doi_id
			FROM Methionine JOIN Protein ON Methionine.prot_id = Protein.prot_id  
			JOIN Evidence ON Evidence.met_id = Methionine.met_id
			JOIN DOI ON DOI.doi_id = Evidence.doi_id";
			
	$statement = $connection->prepare($sql);
	$statement->execute();
	$a = $statement->fetchAll(); // query result


	foreach($a as $value){
		$protid[] = $value['prot_id'];
		$href[] = $value['href'];
		$title[] = $value['title'];
		$doiid[] = $value['doi_id'];
	}


	for($i = 0; $i<count($protid); $i++){
		$comp[$protid[$i].$title[$i]] = $title[$i];
		$url[$href[$i]] = $title[$i];
	}
	$proteins = array_count_values($comp); // provides the number of proteins
	$sites = array_count_values($title); // provides the number of sites
	$link = array_keys($url);
	$list_titles = array_keys($sites);
	$doi_id = array_values(array_unique($doiid));

	for ($i = 0; $i<count($list_titles); $i++){
		$output[] = array('title' => $list_titles[$i], 
						  'proteins' => $proteins[$list_titles[$i]], 
						  'sites' => $sites[$list_titles[$i]],
						  'link' => $link[$i],
						  'doi_id' => $doi_id[$i]);
						  
	}

	return $output;

}

/** 
*
*	This script carries out a SQL query that will return
*   the total number of sites, proteins and species
*		plus the date of the last modification
* 	
*/

function getCountSummary()
{
	$connection = my_connection();

	$sql = "SELECT (SELECT COUNT(*) FROM Protein) AS proteins, (SELECT COUNT(*) FROM Methionine) AS sites,
		(SELECT COUNT(DISTINCT Protein.prot_sp) FROM Protein) AS species,
    (SELECT DATE(MAX(CREATE_TIME))
        FROM information_schema.tables
        WHERE TABLE_NAME IN ('DOI' , 'Methionine', 'Methylation',
				  'Organism', 'Phosphorylation', 'Protein')) AS last_updated";

	$statement = $connection->prepare($sql);
	$statement->execute();
	$result = $statement->fetchObject();

	return $result;
}
