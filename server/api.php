<?php


// This is the "front-controller" PHP file

/**
 *   At its core, Slim is a dispacher that receives an 
 *   HTTP request, invokes an appropriate routine, and
 *   returns an HTTP response.
 *   (https://www.slimframework.com/docs/)
 *   The API documentation is prepared using Swagger
 *   (http://petstore.swagger.io)
 *   (https://editor.swagger.io/)
 */
require_once '../vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

require 'config.php';
require 'scan.php';
require 'search.php';
require 'pname.php';
require 'mapping_functional_categories.php';
require 'groupsCategories.php';
require 'venn.php';
require 'site.php';


/**
 * Slim configuration
 */
$config['displayErrorDetails'] = $slim_displayErrorDetails;
$config['addContentLengthHeader'] = $slim_addContentLenghtHeader;


/**
 * Slim instance
 */
$app = new \Slim\App(['settings' => $config]);

$container = $app->getContainer();
$container['upload_directory'] = './uploads';

//  -------------- scan --------------- //

$app->get('/api/proteins/scan/{prot_id}', function (Request $request, Response $response, array $args) {
    $prot_id = $args['prot_id'];
    $body = $response->getBody();
    $body->rewind(); // ensure your JSON is the only thing in the body
    $body->write(ScanGetJSON($prot_id));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});


// ----------- pname --------------------- //

$app->get('/api/proteins/pname/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $body = $response->getBody();
    $body->rewind(); // ensure your JSON is the only thing in the body
    $body->write(PnameGetJSON($name));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});


 // ------------ search --------------------- //

$app->get('/api/sites/search/{functional_categories}/{organism}/{oxidant}', function (Request $request, Response $response, array $args){
    $a = $args['functional_categories'];  
    $b = str_replace('&', ' ', $a);
    $functional_categories = preg_split("/[\s,]+/", $b);  
    $organism = $args['organism'];
    $oxidant = $args['oxidant'];
    $body = $response->getBody();
    $body->rewind();
    $body->write(SearchGetJSON($functional_categories, $organism, $oxidant));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

// ------------ getonesite --------------------- //

$app->get('/api/sites/getonesite/{prot_id}/{met_pos}', function (Request $request, Response $response, array $args){
    $prot_id = $args['prot_id'];  
    $met_pos = $args['met_pos'];  
    $body = $response->getBody();
    $body->rewind();
    $body->write(GetSiteJSON($prot_id, $met_pos));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

// ------------ protein search --------------------- //
$app->get('/api/proteins/search', function (Request $request, Response $response, array $args){
    $query = $request->getUri()->getQuery();
    parse_str($query, $vars);
    
    $groups = $vars['group'] ? $vars['group'] : '111';
    $properties = $vars['property'] ? $vars['property'] : '222222';
    $temp = groupsCategories($groups, $properties);
    $functional_category = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                                         $temp[3], $temp[4], $temp[5],
                                                         $temp[6], $temp[7], $temp[8]);

    $body = $response->getBody();
    if (isset($vars['format']) && $vars['format'] == 'csv') {
        $file_result = SearchProteinsCSV($vars, $functional_category);
        $body->write(file_get_contents($file_result[0]));
        $nr = $response->withHeader('Content-Type', 'application/force-download;charset=utf-8');
        $nr = $nr->withHeader('Content-Length', $file_result[1]);
        unlink($file_result[0]);
        return $nr->withHeader('Content-Disposition', 'attachment; filename="metosite_search_result.csv"');
    }
    $body->rewind();
    $body->write(SearchProteinsJSON($vars, $functional_category));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

// ------------ protein search2 --------------------- //
$app->get('/api/proteins/search2', function (Request $request, Response $response, array $args){
    $query = $request->getUri()->getQuery();
    parse_str($query, $vars);
    
    $groups = $vars['group'] ? $vars['group'] : '111';
    $properties = $vars['property'] ? $vars['property'] : '222222';
    $temp = groupsCategories($groups, $properties);
    $functional_category = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                                         $temp[3], $temp[4], $temp[5],
                                                         $temp[6], $temp[7], $temp[8]);

    $body = $response->getBody();
    if (isset($vars['format']) && $vars['format'] == 'csv') {
        $file_result = SearchProteinsCSV($vars, $functional_category);
        $body->write(file_get_contents($file_result[0]));
        $nr = $response->withHeader('Content-Type', 'application/force-download;charset=utf-8');
        $nr = $nr->withHeader('Content-Length', $file_result[1]);
        unlink($file_result[0]);
        return $nr->withHeader('Content-Disposition', 'attachment; filename="metosite_search_result.csv"');
    }
    $body->rewind();
    $body->write(SearchProteins2JSON($vars, $functional_category));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

 // ------------ search2 --------------------- //

 $app->get('/api/sites/search2', function (Request $request, Response $response, array $args){
    // /{functional_categories}/{organism}/{oxidant}
    $query = $request->getUri()->getQuery();
    parse_str($query, $vars);
    
    // This logic is not good here
    $groups = $vars['group'] ? $vars['group'] : '111';
    $properties = $vars['category'] ? $vars['category'] : '222222';
    $temp = groupsCategories($groups, $properties);
    $functional_category = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                                         $temp[3], $temp[4], $temp[5],
                                                         $temp[6], $temp[7], $temp[8]);
    //

    $body = $response->getBody();
    if (isset($vars['format']) && $vars['format'] == 'csv') {
        $file_result = Search2GetCSV($vars, $functional_category);
        $body->write(file_get_contents($file_result[0]));
        $nr = $response->withHeader('Content-Type', 'application/force-download;charset=utf-8');
        $nr = $nr->withHeader('Content-Length', $file_result[1]);
        unlink($file_result[0]);
        return $nr->withHeader('Content-Disposition', 'attachment; filename="metosite_search_result.csv"');
    }
    $body->rewind();
    $body->write(Search2GetJSON($vars, $functional_category));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

 // ------------ search3 --------------------- //

 $app->get('/api/sites/search3', function (Request $request, Response $response, array $args){
    $query = $request->getUri()->getQuery();
    parse_str($query, $vars);
    
    // This logic is not good here
    $groups = $vars['group'] ? $vars['group'] : '111';
    $properties = $vars['category'] ? $vars['category'] : '222222';
    $temp = groupsCategories($groups, $properties);
    $functional_category = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                                         $temp[3], $temp[4], $temp[5],
                                                         $temp[6], $temp[7], $temp[8]);
    //

    $body = $response->getBody();
    if (isset($vars['format']) && $vars['format'] == 'csv') {
        $file_result = Search2GetCSV($vars, $functional_category);
        $body->write(file_get_contents($file_result[0]));
        $nr = $response->withHeader('Content-Type', 'application/force-download;charset=utf-8');
        $nr = $nr->withHeader('Content-Length', $file_result[1]);
        unlink($file_result[0]);
        return $nr->withHeader('Content-Disposition', 'attachment; filename="metosite_search_result.csv"');
    }
    $body->rewind();
    $body->write(Search3GetJSON($vars, $functional_category));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

 // ------------ mp_and_search --------------------- //

$app->get('/api/sites/mp_and_search/{groups}/{categories}/{organism}/{oxidant}', 
            function (Request $request, Response $response, array $args) {

    $groups = $args['groups'];
    $categories = $args['categories'];
    $temp = groupsCategories($groups, $categories);

    $organism = $args['organism'];
    $oxidant = $args['oxidant'];

    $functional_category = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                                         $temp[3], $temp[4], $temp[5],
                                                         $temp[6], $temp[7], $temp[8]);
           
    $body = $response->getBody();
    $body->rewind();
    $body->write(SearchGetJSON($functional_category, $organism, $oxidant));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
  
});

// -------------- mapping ----------------------- //

$app->get('/api/sites/mapping/{groups}/{categories}',
                    function (Request $request, Response $response, array $args) {

    $groups = $args['groups'];
    $categories = $args['categories'];
    $temp = groupsCategories($groups, $categories);

    $result = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                            $temp[3], $temp[4], $temp[5],
                                            $temp[6], $temp[7], $temp[8]);

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');

});

// -------------- inv_mapping ----------------------- //

$app->get('/api/sites/inv_mapping/{functional_category}',
                    function (Request $request, Response $response, array $args) {

    $functional_category = $args['functional_category'];
    
    $result = inverse_mapping($functional_category);

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');

});

// -------------- species ----------------------- //

$app->get('/api/summaries/species',
                    function (Request $request, Response $response) {
    
    $result = species();

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');

});

// -------------- oxidant ----------------------- //

$app->get('/api/summaries/oxidant',
                    function (Request $request, Response $response) {
    
    $result = oxidant();

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

// -------------- references ----------------------- //

$app->get('/api/summaries/references',
                    function (Request $request, Response $response) {
    
    $result = references();

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');

});

$app->get('/api/summaries/fcsummary',
                    function (Request $request, Response $response) {
    
    $result = getVennSummary();
    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');

});

// -------------- count ----------------------- //

$app->get('/api/summaries/count',
                    function (Request $request, Response $response) {
    
    $result = getCountSummary();

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

// -------------- getallsites ----------------------- //

$app->get('/api/sites/getallsites',
                    function (Request $request, Response $response) {
    
    $result = getallsites();

    $body = $response->getBody();
    $body->rewind();
    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
});

// -------------- getallprot ----------------------- //

$app->get('/api/proteins/getallprot',
                    function (Request $request, Response $response) {

    $result = getallprot();

    $body = $response->getBody();
    $body->rewind();
    $body->write($result);
    return $response->withHeader('Content-Type', 'text/plain; charset=utf-8');
});

// -------------- getDBs ----------------------- //

$app->get('/api/databases',
                    function (Request $request, Response $response) {

    $directory = $this->get('upload_directory');
    $result = getDirFiles($directory);

    $response = $response
        ->withAddedHeader('Cache-Control', 'must-revalidate')
        ->withAddedHeader('Expires', '0');

    return $response->withJson($result);
});

function getDirFiles($dir)
{
    $aFiles = [];
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                $fileName = pathinfo($file, PATHINFO_FILENAME);
                if ($file != "." && $file != "..") {
                    array_push($aFiles, $fileName);
                }
            }
            closedir($dh);
        }
    }

    return $aFiles;
};

// -------------- upload ----------------------- //

$app->post('/api/upload', function (Request $request, Response $response) {
    $directory = $this->get('upload_directory');

    $uploadedFiles = $request->getUploadedFiles();

    $uploadedFile = $uploadedFiles['file'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filename = moveUploadedFile($directory, $uploadedFile);
    }

    $result = loadFileToDB("$directory/$filename");

    return $response->withJson($result);
});

function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $filename = $uploadedFile->getClientFilename();
    if (!preg_match('/\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])*/', $filename)) {
        $filename = date('Y-m-d') . '.sql';
    }

    $uploadedFile->moveTo($directory . "/" . $filename);

    return $filename;
};

// -------------- load ----------------------- //

$app->post('/api/load', function (Request $request, Response $response) {
    $result = ['reply' => 'Error loading the file', 'ok' => false];

    if (checkWhitelist($request) === true) {
        $directory = $this->get('upload_directory');

        $file = $request->getParsedBody();
        if (array_key_exists('file', $file)) {
            $file = $file['file'] . '.sql';
            $result = loadFileToDB("$directory/$file");
        }
    }

    return $response->withJson($result);
});

function loadFileToDB($file, $toProd = false) {
    $result = 'Error loading the DB: the file does not exist';
    $ok = false;

    if (file_exists($file)) {
        $connection = $toProd ? my_connection('metosite') : my_connection();
        try {
            $connection->beginTransaction();
            $sql = $connection->exec("SET FOREIGN_KEY_CHECKS = 0;
                SET GROUP_CONCAT_MAX_LEN=32768;
                SET @tables = NULL;
                SELECT GROUP_CONCAT('`', table_name, '`') INTO @tables
                FROM information_schema.tables
                WHERE table_schema = (SELECT DATABASE());
                SELECT IFNULL(@tables,'dummy') INTO @tables;
                SET @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
                PREPARE stmt FROM @tables;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
                SET FOREIGN_KEY_CHECKS = 1;");
            $sql_dump_result = $connection->query(file_get_contents($file));
            if (trim($sql_dump_result != "")) {
                $connection->commit();
                $result = $toProd
                    ? "The database " . $file . " has been copied to Production!"
                    : "The database has been uploaded & updated as " . $file;
                $ok = true;
            }
        } catch ( PDOException $e ) { 
            $connection->rollback();
            $result = "There was an error loading the DB... " . $e->getMessage();
            $ok = false;
        }
    }
    return array('reply' => $result, 'ok' => $ok);
};

// -------------- download ----------------------- //

$app->get('/api/download', function (Request $request, Response $response) {
    $query = $request->getUri()->getQuery();
    parse_str($query, $vars);
    if (!isset($vars['format'])) $vars['format'] = "sql";
    if (isset($vars['file'])) {
        $directory = $this->get('upload_directory');
        $file = "$directory/" . $vars['file'] . '.' . $vars['format'];
        if (file_exists($file)) {
            $result = $response->withHeader('Content-Description', 'Database file download')
                ->withHeader('Content-Type', 'application/octet-stream')
                ->withHeader('Content-Disposition', 'attachment;filename='.basename($file))
                ->withHeader('Expires', '0')
                ->withHeader('Cache-Control', 'must-revalidate')
                ->withHeader('Pragma', 'public')
                ->withHeader('Content-Length', filesize($file));
            readfile($file);
            return $result;
        }
    }
    return $response->withStatus(404);
});

// -------------- delete ----------------------- //

$app->post('/api/delete', function (Request $request, Response $response) {
    $result = ['reply' => 'Error deleting the file', 'ok' => false];

    if (checkWhitelist($request) === true) {
        $directory = $this->get('upload_directory');

        $file = $request->getParsedBody();
        if (array_key_exists('file', $file)) {
            $file = $file['file'] . '.sql';
            $result = deleteFile("$directory/$file");
        }
    }

    return $response->withJson($result);
});

function deleteFile($file) {
    $realPath = realpath($file);
    if ($realPath && is_writable($realPath)) {
        if (unlink($realPath)) {
            return array('reply' => $file . ' deleted', 'ok' => true);
        }
    } else {
        return array('reply' => $file . ' could not be deleted', 'ok' => false);
    }
};

/* -------------- deploy to production ----------------------- */

$app->post('/api/deploy', function (Request $request, Response $response) {
    $aRes = ['reply' => 'Error by default', 'ok' => false];
    if (checkWhitelist($request) === true) {
        $filename = $request->getParsedBody();
        if (array_key_exists('file', $filename)) {
            $filename = $filename['file'];
            $directory = $this->get('upload_directory');
            $aRes = loadFileToDB("$directory/$filename.sql", true);
            if ($aRes['ok']) {
                $aRes['csv'] = generateCSV("$directory/$filename.csv");
            }
        }
    }
    $result = $aRes;
    return $response->withJson($result);
});

function checkWhitelist(Request $request) {
    $whitelist = array('alfa', 'staging', 'localhost');
    $baseUrl = $request->getUri()->getBaseUrl();
    foreach($whitelist as $a) {
        if (strpos($baseUrl, $a) !== false) {
            return true;
        }
    }
    return false;
};

// Generate CSV and save it
function generateCSV($to) {
    $groups = '111';
    $properties = '222222';
    $temp = groupsCategories($groups, $properties);
    $functional_category = mapping_functional_categories($temp[0], $temp[1], $temp[2],
                                                         $temp[3], $temp[4], $temp[5],
                                                         $temp[6], $temp[7], $temp[8]);

    $content = Search2GetCSV(null, $functional_category);
    return file_put_contents($to, file_get_contents($content[0]));
};

/**
 * api docs
 */
$app->get('/api/docs', function (Request $request, Response $response, array $args) {
    $response->getBody()->write(file_get_contents('metositeSwagger.yaml'));
    return $response;
});


/**
 * Run API
 */
$app->run();

