<?php
/** 
*	Tnis script changes the array format returned by
*	the function 'search()' to a json format.
*/

require_once('data.functions.php');

function SearchGetJSON($functional_group, $organism, $oxidant)
{
	$r = search($functional_group, $organism, $oxidant);
	$result = array();

	for ($i=0; $i<count($r); $i++){

		$result[] = array(
			"prot_id" => $r[$i]["prot_id"], 					
			"prot_name" => $r[$i]["prot_name"], 
			"met_pos" => $r[$i]["met_pos"],  
			"met_vivo_vitro" => $r[$i]["met_vivo_vitro"], 
			"reg_id" => $r[$i]["reg_id"],
			"org_sp" => $r[$i]["org_sp"], 
			"org_oxidant" => $r[$i]["org_oxidant"]
			); 
	}
	
	return(json_encode($result));
}

function Search2GetJSON($vars, $functional_category)
{
	$r = search2($vars, $functional_category);
	$result = array();

	for ($i=0; $i<count($r); $i++){
		$result[] = array(
			"met_id" => $r[$i]["met_id"],
			"prot_id" => $r[$i]["prot_id"], 					
			"prot_name" => $r[$i]["prot_name"], 
			"met_pos" => $r[$i]["met_pos"],  
			"met_vivo_vitro" => $r[$i]["met_vivo_vitro"], 
			"reg_id" => $r[$i]["reg_id"],
			"org_sp" => $r[$i]["org_sp"], 
			"org_oxidant" => $r[$i]["org_oxidant"],
			"reference" => array(
				"href" => $r[$i]["reference_links"],
				"title" => $r[$i]["reference_titles"])
			); 
	}
	
	return(json_encode($result));
}

function Search3GetJSON($vars, $functional_category)
{
	$r = search3($vars, $functional_category);
	$result = array();

	for ($i=0; $i<count($r->data); $i++){
		$result[] = array(
			"met_id" => $r->data[$i]["met_id"],
			"prot_id" => $r->data[$i]["prot_id"], 					
			"prot_name" => $r->data[$i]["prot_name"], 
			"met_pos" => $r->data[$i]["met_pos"],  
			"met_vivo_vitro" => $r->data[$i]["met_vivo_vitro"], 
			"reg_id" => $r->data[$i]["reg_id"],
			"org_sp" => $r->data[$i]["org_sp"], 
			"org_oxidant" => $r->data[$i]["org_oxidant"],
			"reference" => array(
				"href" => $r->data[$i]["reference_links"],
				"title" => $r->data[$i]["reference_titles"])
			); 
	}
	$r->data = $result;
	
	return(json_encode($r));
}

function Search2GetCSV($vars, $functional_category) {
	
	$json = Search2GetJSON($vars, $functional_category);
	$jsonDecoded = json_decode($json, true);

	$filename = uniqid(rand(), true) . '.csv';
	$fp = fopen($filename, 'w');
	
	fputcsv($fp,["prot_id","prot_name","met_pos","met_vivo_vitro",
				"functional_category","org_sp","org_oxidant","references_links"]);
	//Loop through the associative array.
	foreach($jsonDecoded as $row){
		// Flatten the last column and provide only the link to the reference
		$row["reference"] = $row["reference"]["href"];
		//Write the row to the CSV file.
		fputcsv($fp, $row);
	}

	return [$filename, filesize($filename)];
}


function SearchProteinsCSV($vars, $functional_category)
{
	return "TODO";
}

function SearchProteinsJSON($vars, $functional_category)
{
	$result = array();
	$r = searchProteins($vars, $functional_category);
	for ($i=0; $i<count($r); $i++){
		$refs = array();
		$links_array = explode(',', $r[$i]['reference_links']);
		$titles_array = explode(',', $r[$i]['reference_titles']);
		for ($j=0; $j<count($links_array); $j++){
			$refs[] = array(
				"title" => $titles_array[$j],
				"link" => $links_array[$j]
			);
		}
		$result[] = array(
			"prot_id" => $r[$i]['prot_id'],
			"prot_name" => $r[$i]['prot_name'],
			"met_pos" => $r[$i]['met_pos'],
			"species" => $r[$i]['species'],
			"oxidants" => $r[$i]['oxidants'],
			"references" => $refs
		);
	}
	return json_encode($result);
}

function SearchProteins2JSON($vars, $functional_category)
{
	$result = array();
	$r = searchProteins2($vars, $functional_category);

	for ($i=0; $i<count($r->data); $i++){
		$refs = array();
		$links_array = explode(',', $r->data[$i]['reference_links']);
		$titles_array = explode(',', $r->data[$i]['reference_titles']);
		for ($j=0; $j<count($links_array); $j++){
			$refs[] = array(
				"title" => $titles_array[$j],
				"link" => $links_array[$j]
			);
		}
		$result[] = array(
			"prot_id" => $r->data[$i]['prot_id'],
			"prot_name" => $r->data[$i]['prot_name'],
			"met_pos" => $r->data[$i]['met_pos'],
			"species" => $r->data[$i]['species'],
			"oxidants" => $r->data[$i]['oxidants'],
			"references" => $refs
		);
	}
	$r->data = $result;

	return json_encode($r);
}
