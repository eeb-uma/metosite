<?php
/**
*		mapping_functional_category function
*
*						WHAT DOES THIS FUNCTION DO?
*
*		Esta función, tiene como objetivo devolver un conjunto (en forma de arreglo)
*		con las Categoría Funcionales que el usuario desea seleccionar para llevar a 
*		cabo la búsqueda tomo 9 argumentos, los tres primeros son variables booleanas 
*		para decidir si los Grupos Funcionales 1, 2 y 3 han de ser incluidos en la
*		elección o no. Le siguen seis argumentos que solo se precisarán si el Grupo
*		Funcional 3 ha sido seleccionado. En este caso, el usuario podrá elegir si
*		quiere seleccionar las categorías funcionales que implique ganancia de 
*		actividad, $gain_act = array(1), o que no impliquen ganancia de actividad, 
*		$gain_act = array(0). Una opción, seleccionada por defecto, es que se  
*		seleccionen tanto las categorías que implican ganancia de actividad como las 
*		que no, $gain_act = array(0,1). Opciones análogas tenemos para los siguientes 
*		5 argumentos, pero ahora relativos a la perdidad de actividad, ganancia de PPI,
*		pérdida de PPI, modificación de estabilidad o cambio de localización subcelular.
*
*					EXAMPLES OF HOW TO USE THIS FUNCTION
*
*		The first three arguments must be specified, eithr as TRUE or FALSE. For instance,
*		if we want to select just the functional categories belonging to the functional
*		group 3:
*		Rigth:	mapping_functional_categories($group1 = FALSE, $group2 = FALSE, $group3 = TRUE) 
*		Wrong: 	mapping_functional_categories($group3 = TRUE)
*		If we wish to select the functional categories that involve a gain in PPI regardles
*		of what happen with the other arguments:
*		mapping_functional_categories($group1 = FALSE, $group2 = FALSE, $group3 = TRUE, 
*											gain_ppi = array(1))
*
*/

function mapping_functional_categories($group1 = FALSE, $group2 = FALSE, $group3 = FALSE, 
									   $gain_act = array(0,1), $loss_act = array(0,1),
									   $gain_ppi = array(0,1), $loss_ppi = array(0,1), 
								       $stability = array(0,1), $localization = array(0,1))
{ 
													
	global $fcM; // Functional Category Matrix defined at the bottom

	$categories = array(); // it will be the output (containing the selected categories)
	$provisional = array(); // it will contain the categories belonging to group3 if selected

	if ($group1){
		$categories[]=1;
	}
	if ($group2){
		$categories[]= 2;
	} 
	if ($group3){

		// Gain of Activity (first column from $fcM)
		$gainActSet = array();
		if (in_array(1, $gain_act)){
			$gainActSet = array_merge($gainActSet, which($fcM, 1, 1));
		} 
		if (in_array(0, $gain_act)){
			$gainActSet = array_merge($gainActSet, which($fcM, 1, 0));
		}
		
		// Loss of Activity (second column from $fcM)
		$lossActSet = array();
		if (in_array(1, $loss_act)){
			$lossActSet = array_merge($lossActSet, which($fcM, 2, 1));
		}
		if (in_array(0, $loss_act)){
			$lossActSet = array_merge($lossActSet, which($fcM, 2, 0));
		}

		// Gain of PPI (third column from $fcM)
		$gainPPISet = array();
		if (in_array(1, $gain_ppi)){
			$gainPPISet = array_merge($gainPPISet, which($fcM, 3, 1));
		} 
		if (in_array(0, $gain_ppi)){
			$gainPPISet = array_merge($gainPPISet, which($fcM, 3, 0));
		}

		// Loss of PPI (fourth column from $fcM)
		$lossPPISet = array();
		if (in_array(1, $loss_ppi)){
			$lossPPISet = array_merge($lossPPISet, which($fcM, 4, 1));
		} 
		if (in_array(0, $loss_ppi)){
			$lossPPISet = array_merge($lossPPISet, which($fcM, 4, 0));
		}

		// Protein Stability (fifth column from $fcM)
		$stabilitySet = array();
		if (in_array(1, $stability)){
			$stabilitySet = array_merge($stabilitySet, which($fcM, 5, 1));
		}
		if (in_array(0, $stability)){
			$stabilitySet =  array_merge($stabilitySet, which($fcM, 5, 0));
		}

		// Subcellular Localization (sixth column from $fcM)
		$localizationSet = array();
		if (in_array(1, $localization)){
			$localizationSet = array_merge($localizationSet, which($fcM, 6, 1));
		}
		if (in_array(0, $localization)){
			$localizationSet = array_merge($localizationSet, which($fcM, 6, 0));
		}

		// Intersect of the formed sets.

		$provisional = array_intersect($gainActSet, $lossActSet, $gainPPISet, $lossPPISet,
												$stabilitySet, $localizationSet);
		
	}
	
	$categories = array_merge($provisional, $categories);
	sort($categories);
	return $categories;
}


/**
*		inverse_mapping function
*
* 	This function takes as argument a number from 1 to 65 (the 
*   functional category) and returns a vector of dimension 6
*   where each coordinates is either 1 (TRUE) or 0 (FALSE)
*   First coordinate: Is there an Activity Gain?
*   Second coordinate: Is there an Activity Loss?
*   Third coordinate: Is there a PPI Gain?
*   Fourth coordinate: Is there a PPI Loss?
*   Fifth coordinate: Is there a change in protein Stability?
*   Sixth coordinate: Is there a change in subcellular Localization?
*
*/

function inverse_mapping($functional_category){


	if ($functional_category == 1 or $functional_category == 2){

		$result = array(0,0,0,0,0,0);

	} else {

		global $fcM;
		$result = $fcM[$functional_category-3];

	} 
	 
	return($result);
}



// ------ Ancillary Function ----------------------------//
	/**
	*    Selecciona los índices de las filas que poseen 
	*    el $valor en la $columna indicada de la $matriz.
	*	 Nota: +1 porque los índices empenzian en 0 y
	*          +2 porque de la matriz fcM orignal se han
	*          eliminado las dos primeras filas correspon-
	*          dientes a los grupos funcionales 1 y 2.
	*/

	function which($matriz, $columna, $valor){
		$c = array_column($matriz, $columna-1);
		$output = array_keys($c, $valor);
		for ($i=0; $i<count($output); $i++){
			$output[$i] = $output[$i]+3; // Nota.
		}
		return $output;
	}
// -----------------------------------------------------//


// ------------ Functional Category Matrix ----------- //
	$fcM = array(
		array(1,  0,  0,  0,  0,  0),
		array(0,  1,  0,  0,  0,  0),
		array(0,  0,  1,  0,  0,  0),
		array(0,  0,  0,  1,  0,  0),
		array(0,  0,  0,  0,  1,  0),
		array(0,  0,  0,  0,  0,  1),
		array(0,  0,  0,  1,  0,  1),
		array(0,  0,  0,  1,  1,  0),
		array(0,  0,  1,  0,  0,  1),
		array(0,  0,  1,  0,  1,  0),
		array(0,  1,  0,  0,  0,  1),
		array(0,  1,  0,  0,  1,  0),
		array(1,  0,  0,  0,  0,  1),
		array(1,  0,  0,  0,  1,  0),
		array(0,  0,  1,  1,  0,  0),
		array(0,  1,  0,  1,  0,  0),
		array(0,  1,  1,  0,  0,  0),
		array(1,  0,  0,  1,  0,  0),
		array(1,  0,  1,  0,  0,  0),
		array(1,  1,  0,  0,  0,  0),
		array(0,  0,  0,  0,  1,  1),
		array(0,  0,  0,  1,  1,  1),
		array(0,  0,  1,  0,  1,  1),
		array(0,  1,  0,  0,  1,  1),
		array(1,  0,  0,  0,  1,  1),
		array(0,  0,  1,  1,  0,  1),
		array(0,  0,  1,  1,  1,  0),
		array(1,  1,  1,  0,  0,  0),
		array(0,  1,  0,  1,  0,  1),
		array(0,  1,  0,  1,  1,  0),
		array(0,  1,  1,  0,  0,  1),
		array(0,  1,  1,  0,  1,  0),
		array(1,  0,  0,  1,  0,  1),
		array(1,  0,  0,  1,  1,  0),
		array(1,  0,  1,  0,  0,  1),
		array(1,  0,  1,  0,  1,  0),
		array(1,  1,  0,  0,  0,  1),
		array(1,  1,  0,  0,  1,  0),
		array(0,  1,  1,  1,  0,  0),
		array(1,  0,  1,  1,  0,  0),
		array(1,  1,  0,  1,  0,  0),
		array(0,  0,  1,  1,  1,  1),
		array(0,  1,  0,  1,  1,  1),
		array(0,  1,  1,  0,  1,  1),
		array(1,  0,  0,  1,  1,  1),
		array(1,  0,  1,  0,  1,  1),
		array(1,  1,  0,  0,  1,  1),
		array(0,  1,  1,  1,  0,  1),
		array(0,  1,  1,  1,  1,  0),
		array(1,  0,  1,  1,  0,  1),
		array(1,  0,  1,  1,  1,  0),
		array(1,  1,  0,  1,  0,  1),
		array(1,  1,  0,  1,  1,  0),
		array(1,  1,  1,  1,  0,  0),
		array(1,  1,  1,  0,  0,  1),
		array(1,  1,  1,  0,  1,  0),
		array(0,  1,  1,  1,  1,  1),
		array(1,  0,  1,  1,  1,  1),
		array(1,  1,  0,  1,  1,  1),
		array(1,  1,  1,  0,  1,  1),
		array(1,  1,  1,  1,  0,  1),
		array(1,  1,  1,  1,  1,  0),
		array(1,  1,  1,  1,  1,  1 )
	); 
// ------End of Functional Category Matrix -------- //


function label_functional_category($fc){
	$label = array();
	$basic_fc_label = array(
		1 => "Not addressed",
		2 => "No effect found",
		3 => "Gain of Activity",
		4 => "Loss of Activity",
		5 => "Gain of Protein-Protein Interaction",
		6 => "Loss of Protein-Protein Interaction",
		7 => "Effect on Protein Stability",
		8 => "Effect on Subcellular Location"
	);
    $combined_labels = array(
		9 => [6,8],
		10 => [6,7],
		11 => [5,8],
		12 => [5,7],
		13 => [4,8],
		14 => [4,7],
		15 => [3,8],
		16 => [3,7],
		17 => [5,6],
		18 => [4,6],
		19 => [4,5],
		20 => [3,6],
		21 => [3,5],
		22 => [3,4],
		23 => [7,8],
		24 => [6,7,8],
		25 => [5,7,8],
		26 => [4,7,8],
		27 => [3,7,8],
		28 => [5,6,8],
		29 => [5,6,7],
		30 => [3,4,5],
		31 => [4,6,8],
		32 => [4,6,7],
		33 => [4,5,8],
		34 => [4,5,7],
		35 => [3,6,8],
		36 => [3,6,7],
		37 => [3,5,8],
		38 => [3,5,7],
		39 => [3,4,8],
		40 => [3,4,7],
		41 => [4,5,6],
		42 => [3,5,6],
		43 => [3,4,6],
		44 => [5,6,7,8],
		45 => [4,6,7,8],
		46 => [4,5,7,8],
		47 => [3,6,7,8],
		48 => [3,5,7,8],
		49 => [3,4,7,8],
		50 => [4,5,4,8],
		51 => [4,5,6,7],
		52 => [3,5,6,8],
		53 => [3,5,6,7],
		54 => [3,4,6,8],
		55 => [3,4,6,7],
		56 => [3,4,5,6],
		57 => [3,4,5,8],
		58 => [3,4,5,7],
		59 => [4,5,6,7,8],
		60 => [3,5,6,7,8],
		61 => [3,4,6,7,8],
		62 => [3,4,5,7,8],
		63 => [3,4,5,6,8],
		64 => [3,4,5,6,7],
		65 => [3,4,5,6,7,8]
	);

	if ($fc <= 65 && $fc > 8){
		foreach ($combined_labels[$fc] as $n){
			array_push($label, $basic_fc_label[$n]);
		}
	} elseif ( $fc < 9 && $fc > 0 ){
		array_push($label, $basic_fc_label[$fc]);
	}
	
	return $label;
}


/* For internal checking purposes 
$v = mapping_functional_categories($group1 = FALSE, $group2 = FALSE, $group3 = TRUE, 
									   $gain_act = array(0,1), $loss_act = array(0),
									   $gain_ppi = array(0), $loss_ppi = array(0), 
								       $stability = array(0,1), $localization = array(0,1));
print_r($v);

$v = inverse_mapping(18);
print_r($v);
*/



