<?php
/** 
*	This script changes the array format returned by
*	the function 'pname()' to a json format.
*/

require_once('data.functions.php');

function PnameGetJSON($name)
{
	$r = pname($name);
	$result = array();

	for ($i=0; $i<count($r); $i++){

		$result[] = array(
			"prot_id" => $r[$i]["prot_id"], 					
			"prot_name" => $r[$i]["prot_name"], 
			"prot_sp" => $r[$i]["prot_sp"]
			); 
	}
	
	return(json_encode($result));
}

