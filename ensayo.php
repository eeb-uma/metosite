<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Ensayos</title>
</head>

<body>
	<form action="" method="post">
		
		<fieldset>
			<legend><h2>Functional Groups</h2></legend>

			<label for="group1">GROUP 1: UNKNOWN EFFECT</label>
			<input type="checkbox" name="group" id="group1" value="group1"><br/><br/>

			<label for="group2">GROUP 2: NON REGULATORY</label>
			<input type="checkbox" name="group" id="group2" value="group2"><br/><br/>

			<label for="group3">GROUP 3: REGULATORY</label>
			<input type="checkbox" name="group" id="group3" value="group3" checked="checked"><br/><br/>

			<fieldset>
				Gain of Activity<br/>
				<label for="yes">Yes</label>
				<input type="radio" name="gain_activity" id="yes" value="YES">
				<label for="non">Non</label>
				<input type="radio" name="gain_activity" id="non" value="NON">
				<label for="whatever">Whatever</label>
				<input type="radio" name="gain_activity" id="whatever" value="WHATEVER" checked="checked"><br/><br/>
				
				Loss of Activity<br/>
				<label for="yes">Yes</label>
				<input type="radio" name="loss_activity" id="yes" value="YES">
				<label for="non">Non</label>
				<input type="radio" name="loss_activity" id="non" value="NON">
				<label for="whatever">Whatever</label>
				<input type="radio" name="loss_activity" id="whatever" value="WHATEVER" checked="checked"><br/><br/>

				Gain of PPI<br/>
				<label for="yes">Yes</label>
				<input type="radio" name="gain_ppi" id="yes" value="YES">
				<label for="non">Non</label>
				<input type="radio" name="gain_ppi" id="non" value="NON">
				<label for="whatever">Whatever</label>
				<input type="radio" name="gain_ppi" id="whatever" value="WHATEVER" checked="checked"><br/><br/>

				Loss of PPI<br/>
				<label for="yes">Yes</label>
				<input type="radio" name="loss_ppi" id="yes" value="YES">
				<label for="non">Non</label>
				<input type="radio" name="loss_ppi" id="non" value="NON">
				<label for="whatever">Whatever</label>
				<input type="radio" name="loss_ppi" id="whatever" value="WHATEVER" checked="checked"><br/><br/>

				Effect on Protein Stability<br/>
				<label for="yes">Yes</label>
				<input type="radio" name="protein_stability" id="yes" value="YES">
				<label for="non">Non</label>
				<input type="radio" name="protein_stability" id="non" value="NON">
				<label for="whatever">Whatever</label>
				<input type="radio" name="protein_stability" id="whatever" value="WHATEVER" checked="checked"><br/><br/>

				Effect on Subcellular Localization<br/>
				<label for="yes">Yes</label>
				<input type="radio" name="subcellular_localization" id="yes" value="YES">
				<label for="non">Non</label>
				<input type="radio" name="subcellular_localization" id="non" value="NON">
				<label for="whatever">Whatever</label>
				<input type="radio" name="subcellular_localization" id="whatever" value="WHATEVER" checked="checked"><br/><br/>
			</fieldset>
		</fieldset>

		<fieldset>
			<legend><h2>Organisms</h2></legend>
			<select>
				<option>Select all the species</option>
				<option>Archaeabacteria</option> <!-- Dominio/Domain -->
				<option>Bacteria</option> <!-- Dominio/Domain -->
				<option>Eukaryota</option> <!-- Dominio/Domain -->
				<option>Animalia</option> <!-- Reino/Kingdom -->
				<option>Fungi</option> <!-- Reino/Kingdom -->
				<option>Plantae</option> <!-- Reino/Kingdom -->
				<option>Protista</option> <!-- Reino/Kingdom -->
				<option>Arthropoda</option> <!-- Filo/Phylum -->
				<option>Chordata</option> <!-- Filo/Phylum -->
				<option>Aspergillus nidulans</option>
				<option>Drosophila melanoganster</option> 
				<option>Equus caballus</option> 
				<option>Escherichia coli</option> 
				<option>Homo sapiens</option> 
				<option>Oryctolagus cuniculus</option> 
			</select>
		</fieldset>

		<fieldset>
			<legend><h2>Oxidants</h2></legend>
			<select>
				<option>Select all the oxidants</option>
				<option>angiotensin II</option>
				<option>hydrogen peroxide</option>
				<option>hypochlorite</option>
				<option>ischaemia</option>
				<option>mical-catalyzed</option>
				<option>ROS</option>
				<option>taurine chloramine</option>
				<option>unknown</option>
			</select>
		</fieldset>
	</form>

</body>
</html>