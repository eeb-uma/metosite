<?php


// Configure Slim app
$config = ['settings' => [
	'addContentLengthHeader' => false,
]];

// Instance Slim app
$app = new \Slim\App($config);

// Define app route
$app -> get('hello/{name}', function($request, $response, $args) {
	return $response -> write("Hello " . $args['name']);
});

// Run app
$app -> run();
