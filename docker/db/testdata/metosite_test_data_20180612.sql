/* BD ligera para realizar tests */

--
-- Estructura de tabla para la tabla `Methionine` ------------------------
--
CREATE TABLE `Methionine` (
  `met_id` int(11) NOT NULL, 
  `met_pos` int(4) NOT NULL,
  `met_ext` int(3) DEFAULT NULL,
  `met_vivo_vitro` varchar(5) DEFAULT NULL,
  `prot_id` varchar(20) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `org_id`int(11) NOT NULL,
  PRIMARY KEY (`met_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `Regulatory`
--
INSERT INTO `Methionine` (`met_id`, `met_pos`, `met_ext`, `met_vivo_vitro`,
`prot_id`, `reg_id`, `org_id`) 
VALUES 
(1, 115, NULL, 'vitro','P23528', 18,1), -- from Cofilin-1
(2, 351,  NULL, 'both', 'P01009', 4,2), -- from A1AT
(3, 358,  NULL, 'both', 'P01009', 4,2), -- from A1AT
(4, 45,   NULL, 'both', 'P25963', 16,3), -- from iKBa
(5, 123,  NULL, 'both', 'A0A0E1M553', 3,4), -- from HypT
(6, 206,  NULL, 'both', 'A0A0E1M553', 3,4), -- from HypT
(7, 230,  NULL, 'both', 'A0A0E1M553', 3,4), -- from HypT
(8, 46,   NULL, 'both', 'P68135', 18,5), -- from a-Actin
(9, 49,   NULL, 'both', 'P68135', 2,5), -- from a-Actin
(10, 44,  NULL, 'both', 'P10987', 18,6), -- from b-Actin
(11, 47,  NULL, 'both', 'P10987', 2,6), -- from b-Actin
(12, 169, NULL, 'vivo', 'P28348', 13,7), -- from NirA
(13, 406, NULL, 'vitro','Q08209', 18,8), -- from Calcineurin
(14, 281, NULL, 'both', 'Q6PHZ2', 3,10), -- from CaMKII
(15, 285, NULL, 'both', 'Q6PHZ2', 3,10), -- from CaMKII
(16, 538, NULL, 'both', 'P11035', 3,11), -- from NR
(17, 293, NULL, 'both', 'P52901', 3,11), -- from E1a PDH
(18, 80, NULL, 'vitro', 'P00004', 39,12), -- Cytc

(19, 492, 10.76, 'vivo', 'A0AVT1',1, 9), 
(20, 56, 11.87, 'vivo', 'A0AVT1',1, 9), 
(21, 2042, 11.7, 'vivo', 'A2RRP1',1, 9), 
(22, 2204, 0.18, 'vivo', 'A2RRP1',1, 9);

--
-- Estructura de tabla para la tabla `Protein` ---------------------------
--
CREATE TABLE `Protein`( 
  `prot_id` varchar(20) NOT NULL,
  `prot_name`varchar(250) DEFAULT NULL,
  `prot_nickname`varchar(250) DEFAULT NULL,
  `gene_name`varchar(250) DEFAULT NULL,
  `prot_seq` text DEFAULT NULL,
  `prot_pdb` varchar(5) DEFAULT NULL, 
  `prot_sub` varchar(1000) DEFAULT NULL,
  `prot_note` varchar(1000) DEFAULT NULL,
  `prot_sp` varchar(25) NOT NULL,
  PRIMARY KEY (`prot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Protein`
--
INSERT INTO `Protein` (`prot_id`, `prot_name`,  `prot_nickname`, `gene_name`, `prot_seq`, 
`prot_pdb`, `prot_sub`, `prot_note`, `prot_sp`) 

VALUES 
('P23528', 'Cofilin-1', 'Cofilin', 'CFL1', 'MASGVAVSDGVIKVFNDMKVRKSSTPEEVKKRKKAVLFCLSEDKKNIILEEGKEILVGDVGQTVDDPYATFVKMLPDKDCRYALYDATYETKESKKEDLVFIFWAPESAPLKSKMIYASSKDAIKKKLTGIKHELQANCYEEVKDRCTLAEKLGGSAVISLEGKPL', '4BEX', 'nuclear matrix, nucleus, cytoskeleton, cytoplasm, cell cortex, peripheral membrane protein ruffle membrane, lamellipodium membrane. It should be noted that cellular localization varies throughout development and may be related to phosphorylation levels. Shows diffuse cortical cytoplasm localizatio', 'Oxidation inhibits its F-actin-binding and depolymerization activity', 'Homo sapiens'), 
('P01009', 'Alpha-1-antitrypsin', 'A1AT', 'SERPINA1', 'EDPQGDAAQKTDTSHHDQDHPTFNKITPNLAEFAFSLYRQLAHQSNSTNIFFSPVSIATAFAMLSLGTKADTHDEILEGLNFNLTEIPEAQIHEGFQELLRTLNQPDSQLQLTTGNGLFLSEGLKLVDKFLEDVKKLYHSEAFTVNFGDTEEAKKQINDYVEKGTQGKIVDLVKELDRDTVFALVNYIFFKGKWERPFEVKDTEEEDFHVDQVTTVKVPMMKRLGMFNIQHCKKLSSWVLLMKYLGNATAIFFLPDEGKLQHLENELTHDIITKFLENEDRRSASLHLPKLSITGTYDLKSVLGQLGITKVFSNGADLSGVTEEAPLKLSKAVHKAVLTIDEKGTEAAGAMFLEAIPMSIPPEVKFNKPFVFLMIEQNTKSPLFMGKVVNPTQK', '3CWM', 'extracellular', 'The processed protein start at the position 25 from the precursor. Oxidation causes loss of anti-elactase activity', 'Homo sapiens'), 
('P25963', 'NF-kappa-B inhibitor alpha', 'IkBa', 'NFKBIA', 'MFQAAERPQEWAMEGPRDGLKKERLLDDRHDSGLDSMKDEEYEQMVKELQEIRLEPQEVPRGSEPWKQQLTEDGDSFLHLAIIHEEKALTMEVIRQVKGDLAFLNFQNNLQQTPLHLAVITNQPEIAEALLGAGCDPELRDFRGNTPLHLACEQGCLASVGVLTQSCTTPHLHSILKATNYNGHTCLHLASIHGYLGIVELLVSLGADVNAQEPCNGRTALHLAVDLQNPDLVSLLLKCGADVNRVTYQGYSPYQLTWGRPSTRIQQQLGQLTLENLQMLPESEDEESYDTESEFTEFTEDELPYDDCVFGGQRLTL', NULL, 'nucleus, cytoplasm. Shuttles between nucleus and cytoplasm', 'Activation in the sense that oxidation leads to gain of inhibitory activity. The oxidized protein is resistant to degradation and the undegradated protein retains the transcriptionf factor  NF-kappa-B into the cytosol. It is possible that the oxidation interfers with the phosphorylation of S32 and S36 ??', 'Homo sapiens'), 
('A0A0E1M553', 'Hypochlorite-responsive transcription factor', 'HypT', 'hypT', 'MDDCGAVLHNIETKWLYDFLTLEKCRNFSQAAVSRNVSQPAFSRRIRALEQAIGVELFNRQVTPLQLSEQGKIFHSQIRHLLQQLESNLAELRGGSDYAQRKIKIAAAHSLSLGLLPSIISQMPPLFTWAIEAIDVDEAVDKLREGQSDCIFSFHDEDLLEAPFDHIRLFESQLFPVCASDEHGEALFNLAQPHFPLLNYSRNSYMGRLINRTLTRHSELSFSTFFVSSMSELLKQVALDGCGIAWLPEYAIQQEIRSGQLVVLNRDELVIPIQAYAYRMNTRMNPVAERFWRELRELEIVLS', NULL, 'cytoplasm', 'The activation of this transcription factor requires the simultaneous oxidation of M123, M206 and M230', 'Escherichia coli'), 
('P68135', 'alpha-Actin', 'a-Actin', 'ACTA1', 'MCDEDETTALVCDNGSGLVKAGFAGDDAPRAVFPSIVGRPRHQGVMVGMGQKDSYVGDEAQSKRGILTLKYPIEHGIITNWDDMEKIWHHTFYNELRVAPEEHPTLLTEAPLNPKANREKMTQIMFETFNVPAMYVAIQAVLSLYASGRTTGIVLDSGDGVTHNVPIYEGYALPHAIMRLDLAGRDLTDYLMKILTERGYSFVTTAEREIVRDIKEKLCYVALDFENEMATAASSSSLEKSYELPDGQVITIGNERFRCPETLFQPSFIGMESAGIHETTYNSIMKCDIDIRKDLYANNVMSGGTTMYPGIADRMQKEITALAPSTMKIKIIAPPERKYSVWIGGSILASLSTFQQMWITKQEYDEAGPSIVHRKCF', NULL, 'cytoskeleton', 'Sulfoxided G-actin is not competent to polymerize. Although Mical oxidizes both M46 and M49, it is probable that only M46 interfers with the polymerization process (10.1126/science.1211956)', 'Oryctolagus cuniculus'), 
('P10987', 'Actin-5C', 'b-Actin', 'Act5C', 'CDEEVAALVVDNGSGMCKAGFAGDDAPRAVFPSIVGRPRHQGVMVGMGQKDSYVGDEAQSKRGILTLKYPIEHGIVTNWDDMEKIWHHTFYNELRVAPEEHPVLLTEAPLNPKANREKMTQIMFETFNTPAMYVAIQAVLSLYASGRTTGIVLDSGDGVSHTVPIYEGYALPHAILRLDLAGRDLTDYLMKILTERGYSFTTTAEREIVRDIKEKLCYVALDFEQEMATAASSSSLEKSYELPDGQVITIGNERFRCPEALFQPSFLGMEACGIHETTYNSIMKCDVDIRKDLYANTVLSGGTTMYPGIADRMQKEITALAPSTMKIKIIAPPERKYSVWIGGSILASLSTFQQMWISKQEYDESGPSIVHRKCF', '2HF3', 'cytoskeleton', 'Mical oxidizes actin on its Met 44 and Met 47 residues, althought it is the oxidation of the Met 44 residue through which Mical induces F-actin disassembly (10.1126/science.1211956)', 'Drosophila melanoganster'), 
('P28348', 'Nitrogen assimilation transcription factor nirA', 'NirA', 'nirA', 'MGEKLDPELSSDGPHTKSSSKGQGTSTDNAPASKRRCVSTACIACRRRKSKCDGNLPSCAACSSVYHTTCVYDPNSDHRRKGVYKKDTDTLRTKNSTLLTLIQALLNYEEEDAFDLVRQIRSCDNLEDVAQSLVNQEKKSSGWLSNAVIHEENDIAQTDQFESELAGKMSNLVLDGSRKFIGGTSNLIFLPPGSELNEFKPGLATNGDLEGSVTRWTTVTDDQQLISHLLTMYFSWHYPFFTTLSKELFYRDYSRGVPSQYCSSLLVNTMLALGCHFSSWPGAREDPDNSATAGDHFFKEAKRLILDNDELVNSKLCTVQALALMSVREAGCGREGKGWVYSGMSFRMAFDLGLNLESSSLRDLSEEEIDARRITFWGCFLFDKCWSNYLGRQPQFTTANTSVSAVDILPNEESTLWSPYSDMGPSREYAQPSRTRAVADQISQLCKISGDLVVFFYDLAPKEKPSSKQLELKKLSEIHTRLEAWKKGLPKELEPREGQLPQALLMHMFYQLLLIHLYRPFLKYTKSTSPLPQHVSPRKLCTQAAAAISKLLRLYKRTYGFKQICNIAVYIAHTALTIHLLNLPEKNAQRDVIHGLRHLEEMGESWLCARRTLRILDISASKWQVQLPREAVIVFEQTHARWGSWGPWDQAASPSTTSDSPPSVSSQSVVATTDLSQPVSQSAGNQPANPSMGTSPNLTQPVASQYSSTPSGPVSVSAMRAVQRSFSAQLAHNEARQPEPTYLRPVSTSYGPVPSTQSAQEQWYSPTEAQFRAFTAAHSMPTTSAQSPLTTFDTPENLVEESQDWWSRDVNALQLGAEDWTQNWNNGLPTTSADWRYVDNVPNIPSTSAPDADYKPPQPPPNMARPNQYPTDPVANVNSNQTNMIFPGSFQR', NULL, 'nucleus', 'When this Met is oxidized (in the absence of nitrate) the inactive protein is retained in the cytoplasm. When nitrate is present, MetO is reduced back to Met and the active protein is located into the nucleus', 'Aspergillus nidulans'), 
('Q08209', 'Serine/threonine-protein phosphatase 2B catalytic subunit alpha isoform', 'Calcineurin', 'PPP3CA', 'MSEPKAIDPKLSTTDRVVKAVPFPPSHRLTAKEVFDNDGKPRVDILKAHLMKEGRLEESVALRIITEGASILRQEKNLLDIDAPVTVCGDIHGQFFDLMKLFEVGGSPANTRYLFLGDYVDRGYFSIECVLYLWALKILYPKTLFLLRGNHECRHLTEYFTFKQECKIKYSERVYDACMDAFDCLPLAALMNQQFLCVHGGLSPEINTLDDIRKLDRFKEPPAYGPMCDILWSDPLEDFGNEKTQEHFTHNTVRGCSYFYSYPAVCEFLQHNNLLSILRAHEAQDAGYRMYRKSQTTGFPSLITIFSAPNYLDVYNNKAAVLKYENNVMNIRQFNCSPHPYWLPNFMDVFTWSLPFVGEKVTEMLVNVLNICSDDELGSEEDGFDGATAAARKEVIRNKIRAIGKMARVFSVLREESESVLTLKGLTPTGMLPSGVLSGGKQTLQSATVEAIEADEAIKGFSPQHKITSFEEAKGLDRINERMPPRRDAMPSDANLNSINKALTSETNGTDSNGSNSSNIQ', NULL, 'cell membrane, sarcolemma, nucleus', 'Calcineurin activation exhibited a loss in cooperativity with respect to calmodulin following Met406 oxidation as shown by a reduction in the Hill slope from 1.88 to 0.86. Maximum phosphatase activity was unaffected by Met oxidation', 'Homo sapiens'), 
('Q6PHZ2', 'Calcium/calmodulin-dependent protein kinase type II subunit delta', 'CaMKII', 'Camk2d', 'MASTTTCTRFTDEYQLFEELGKGAFSVVRRCMKIPTGQEYAAKIINTKKLSARDHQKLEREARICRLLKHPNIVRLHDSISEEGFHYLVFDLVTGGELFEDIVAREYYSEADASHCIQQILESVNHCHLNGIVHRDLKPENLLLASKSKGAAVKLADFGLAIEVQGDQQAWFGFAGTPGYLSPEVLRKDPYGKPVDMWACGVILYILLVGYPPFWDEDQHRLYQQIKAGAYDFPSPEWDTVTPEAKDLINKMLTINPAKRITASEALKHPWICQRSTVASMMHRQETVDCLKKFNARRKLKGAILTTMLATRNFSAAKSLLKKPDGVKESTESSNTTIEDEDVKARKQEIIKVTEQLIEAINNGDFEAYTKICDPGLTAFEPEALGNLVEGMDFHRFYFENALSKSNKPIHTIILNPHVHLVGDDAACIAYIRLTQYMDGSGMPKTMQSEETRVWHRRDGKWQNVHFHRSGSPTVPIKPPCIPNGKENFSGGTSLWQNI', '6BAB', 'sarcolemma, peripheral membrane protein, sarcoplasmic reticulum membrane', 'The sulfoxidation activates the kinase in a Ca/CaM fashion. In the 6BAB PDB this Met is labeled as 282', 'Mus musculus'), 
('P11035', 'Nitrate reductase [NADH] 2', 'NR', 'NIA2', 'MAASVDNRQYARLEPGLNGVVRSYKPPVPGRSDSPKAHQNQTTNQTVFLKPAKVHDDDEDVSSEDENETHNSNAVYYKEMIRKSNAELEPSVLDPRDEYTADSWIERNPSMVRLTGKHPFNSEAPLNRLMHHGFITPVPLHYVRNHGHVPKAQWAEWTVEVTGFVKRPMKFTMDQLVSEFAYREFAATLVCAGNRRKEQNMVKKSKGFNWGSAGVSTSVWRGVPLCDVLRRCGIFSRKGGALNVCFEGSEDLPGGAGTAGSKYGTSIKKEYAMDPSRDIILAYMQNGEYLTPDHGFPVRIIIPGFIGGRMVKWLKRIIVTTKESDNFYHFKDNRVLPSLVDAELADEEGWWYKPEYIINELNINSVITTPCHEEILPINAFTTQRPYTLKGYAYSGGGKKVTRVEVTVDGGETWNVCALDHQEKPNKYGKFWCWCFWSLEVEVLDLLSAKEIAVRAWDETLNTQPEKMIWNLMGMMNNCWFRVKTNVCKPHKGEIGIVFEHPTLPGNESGGWMAKERHLEKSADAPPSLKKSVSTPFMNTTAKMYSMSEVKKHNSADSCWIIVHGHIYDCTRFLMDHPGGSDSILINAGTDCTEEFEAIHSDKAKKMLEDYRIGELITTGYSSDSSSPNNSVHGSSAVFSLLAPIGEATPVRNLALVNPRAKVPVQLVEKTSISHDVRKFRFALPVEDMVLGLPVGKHIFLCATINDKLCLRAYTPSSTVDVVGYFELVVKIYFGGVHPRFPNGGLMSQYLDSLPIGSTLEIKGPLGHVEYLGKGSFTVHGKPKFADKLAMLAGGTGITPVYQIIQAILKDPEDETEMYVIYANRTEEDILLREELDGWAEQYPDRLKVWYVVESAKEGWAYSTGFISEAIMREHIPDGLDGSALAMACGPPPMIQFAVQPNLEKMQYNIKEDFLIF', NULL, 'cytosol, plasma membrane, vacuole', 'The oxidation inhibits the phosphorylation of the regulatory S534, which would avoid the inactivation of NR', 'Arabidopsis thaliana'), 
('P52901', 'Pyruvate dehydrogenase E1 component subunit alpha-1, mitochondrial', 'E1-alpha1 PDH', 'E1 ALPHA', 'MALSRLSSRSNIITRPFSAAFSRLISTDTTPITIETSLPFTAHLCDPPSRSVESSSQELLDFFRTMALMRRMEIAADSLYKAKLIRGFCHLYDGQEAVAIGMEAAITKKDAIITAYRDHCIFLGRGGSLHEVFSELMGRQAGCSKGKGGSMHFYKKESSFYGGHGIVGAQVPLGCGIAFAQKYNKEEAVTFALYGDGAANQGQLFEALNISALWDLPAILVCENNHYGMGTAEWRAAKSPSYYKRGDYVPGLKVDGMDAFAVKQACKFAKQHALEKGPIILEMDTYRYHGHSMSDPGSTYRTRDEISGVRQERDPIERIKKLVLSHDLATEKELKDMEKEIRKEVDDAIAKAKDCPMPEPSELFTNVYVKGFGTESFGPDRKEVKASLP', NULL, 'mitochondrion', 'Oxidation inhibits the phosphorylation of the adjacent phosphorylation site 1, which, in turn, avoid inactivation of PDH (doi.org/10.3389/fpls.2012.00153)', 'Arabidopsis thaliana'), 
('P00004', 'Cytochrome c', 'Cyt c', 'CYCS', 'GDVEKGKKIFVQKCAQCHTVEKGGKHKTGPNLHGLFGRKTGQAPGFTYTDANKNKGITWKEETLMEYLENPKKYIPGTKMIFAGIKKKTEREDLIAYLKKATNE', '1HRC', 'mitochondrion intermembrane space', 'The oxidation of M80 inhibits electron transport but increases the peroxidase activity. It may also influence the subcellular location of the protein (10.1073/pnas.0809279106)', 'Equus caballus'), 

('A0AVT1', 'Ubiquitin-like modifier-activating enzyme 6', NULL, 'UBA6', 'MEGSEPVAAHQGEEASCSSWGTGSTNKNLPIMSTASVEIDDALYSRQRYVLGDTAMQKMAKSHVFLSGMGGLGLEIAKNLVLAGIKAVTIHDTEKCQAWDLGTNFFLSEDDVVNKRNRAEAVLKHIAELNPYVHVTSSSVPFNETTDLSFLDKYQCVVLTEMKLPLQKKINDFCRSQCPPIKFISADVHGIWSRLFCDFGDEFEVLDTTGEEPKEIFISNITQANPGIVTCLENHPHKLETGQFLTFREINGMTGLNGSIQQITVISPFSFSIGDTTELEPYLHGGIAVQVKTPKTVFFESLERQLKHPKCLIVDFSNPEAPLEIHTAMLALDQFQEKYSRKPNVGCQQDSEELLKLATSISETLEEKPDVNADIVHWLSWTAQGFLSPLAAAVGGVASQEVLKAVTGKFSPLCQWLYLEAADIVESLGKPECEEFLPRGDRYDALRACIGDTLCQKLQNLNIFLVGCGAIGCEMLKNFALLGVGTSKEKGMITVTDPDLIEKSNLNRQFLFRPHHIQKPKSYTAADATLKINSQIKIDAHLNKVCPTTETIYNDEFYTKQDVIITALDNVEARRYVDSRCLANLRPLLDSGTMGTKGHTEVIVPHLTESYNSHRDPPEEEIPFCTLKSFPAAIEHTIQWARDKFESSFSHKPSLFNKFWQTYSSAEEVLQKIQSGHSLEGCFQVIKLLSRRPRNWSQCVELARLKFEKYFNHKALQLLHCFPLDIRLKDGSLFWQSPKRPPSPIKFDLNEPLHLSFLQNAAKLYATVYCIPFAEEDLSADALLNILSEVKIQEFKPSNKVVQTDETARKPDHVPISSEDERNAIFQLEKAILSNEATKSDLQMAVLSFEKDDDHNGHIDFITAASNLRAKMYSIEPADRFKTKRIAGKIIPAIATTTATVSGLVALEMIKVTGGYPFEAYKNCFLNLAIPIVVFTETTEVRKTKIRNGISFTIWDRWTVHGKEDFTLLDFINAVKEKYGIEPTMVVQGVKMLYVPVMPGHAKRLKLTMHKLVKPTTEKKYVDLTVSFAPDIDGDEDLPGPPVRYYFSHDTD', NULL, 'cytosol, cytoplasm', NULL, 'Homo sapiens'), 
('A2RRP1', 'Neuroblastoma-amplified sequence', NULL, 'NBAS', 'MAAPESGPALSPGTAEGEEETILYDLLVNTEWPPETEVQPRGNQKHGASFIITKAIRDRLLFLRQYIWYSPAPFLLPDGLVRLVNKQINWHLVLASNGKLLAAVQDQCVEIRSAKDDFTSIIGKCQVPKDPKPQWRRVAWSYDCTLLAYAESTGTVRVFDLMGSELFVISPASSFIGDLSYAIAGLIFLEYKASAQWSAELLVINYRGELRSYLVSVGTNQSYQESHCFSFSSHYPHGINTAIYHPGHRLLLVGGCETAEVGMSKASSCGLSAWRVLSGSPYYKQVTNGGDGVTAVPKTLGLLRMLSVKFYSRQGQEQDGIFKMSLSPDGMLLAAIHFSGKLSIWAIPSLKQQGEWGQNEQPGYDDLNPDWRLSTEKRKKIKDKESFYPLIDVNWWADSAVTLARCSGALTVSSVKTLKNLLGKSCEWFEPSPQVTATHDGGFLSLECEIKLAPKRSRLETRAGEEDEGEEDSDSDYEISAKARYFGYIKQGLYLVTEMERFAPPRKRPRTITKNYRLVSLRSTTPEELYQRKIESEEYEEALSLAHTYGLDTDLVYQRQWRKSAVNVASIQNYLSKIKKRSWVLHECLERVPENVDAAKELLQYGLKGTDLEALLAIGKGADDGRFTLPGEIDIDSISYEELSPPDEEPAKNKKEKELKKRQELLKLVNFSKLTLEQKELCRCRRKLLTYLDRLATYEEILGVPHASEQRYDAEFFKKFRNQNIVLSARTYAQESNVQALEILFTYHGSDLLPHRLAILSNFPETTSPHEYSVLLPEACFNGDSLMIIPWHEHKHRAKDWCEELACRMVVEPNLQDESEFLYAAQPELLRFRMTQLTVEKVMDWYQTRAEEIEHYARQVDCALSLIRLGMERNIPGLLVLCDNLVTLETLVYEARCDVTLTLKELQQMKDIEKLRLLMNSCSEDKYVTSAYQWMVPFLHRCEKQSPGVANELLKEYLVTLAKGDLKFPLKIFQHSKPDLQQKIIPDQDQLMAIALECIYTCERNDQLCLCYDLLECLPERGYGDKTEATTKLHDMVDQLEQILSVSELLEKHGLEKPISFVKNTQSSSEEARKLMVRLTRHTGRKQPPVSESHWRTLLQDMLTMQQNVYTCLDSDACYEIFTESLLCSSRLENIHLAGQMMHCSACSENPPAGIAHKGKPHYRVSYEKSIDLVLAASREYFNSSTNLTDSCMDLARCCLQLITDRPPAIQEELDLIQAVGCLEEFGVKILPLQVRLCPDRISLIKECISQSPTCYKQSTKLLGLAELLRVAGENPEERRGQVLILLVEQALRFHDYKAASMHCQELMATGYPKSWDVCSQLGQSEGYQDLATRQELMAFALTHCPPSSIELLLAASSSLQTEILYQRVNFQIHHEGGENISASPLTSKAVQEDEVGVPGSNSADLLRWTTATTMKVLSNTTTTTKAVLQAVSDGQWWKKSLTYLRPLQGQKCGGAYQIGTTANEDLEKQGCHPFYESVISNPFVAESEGTYDTYQHVPVESFAEVLLRTGKLAEAKNKGEVFPTTEVLLQLASEALPNDMTLALAYLLALPQVLDANRCFEKQSPSALSLQLAAYYYSLQIYARLAPCFRDKCHPLYRADPKELIKMVTRHVTRHEHEAWPEDLISLTKQLHCYNERLLDFTQAQILQGLRKGVDVQRFTADDQYKRETILGLAETLEESVYSIAISLAQRYSVSRWEVFMTHLEFLFTDSGLSTLEIENRAQDLHLFETLKTDPEAFHQHMVKYIYPTIGGFDHERLQYYFTLLENCGCADLGNCAIKPETHIRLLKKFKVVASGLNYKKLTDENMSPLEALEPVLSSQNILSISKLVPKIPEKDGQMLSPSSLYTIWLQKLFWTGDPHLIKQVPGSSPEWLHAYDVCMKYFDRLHPGDLITVVDAVTFSPKAVTKLSVEARKEMTRKAIKTVKHFIEKPRKRNSEDEAQEAKDSKVTYADTLNHLEKSLAHLETLSHSFILSLKNSEQETLQKYSHLYDLSRSEKEKLHDEAVAICLDGQPLAMIQQLLEVAVGPLDISPKDIVQSAIMKIISALSGGSADLGGPRDPLKVLEGVVAAVHASVDKGEELVSPEDLLEWLRPFCADDAWPVRPRIHVLQILGQSFHLTEEDSKLLVFFRTEAILKASWPQRQVDIADIENEENRYCLFMELLESSHHEAEFQHLVLLLQAWPPMKSEYVITNNPWVRLATVMLTRCTMENKEGLGNEVLKMCRSLYNTKQMLPAEGVKELCLLLLNQSLLLPSLKLLLESRDEHLHEMALEQITAVTTVNDSNCDQELLSLLLDAKLLVKCVSTPFYPRIVDHLLASLQQGRWDAEELGRHLREAGHEAEAGSLLLAVRGTHQAFRTFSTALRAAQHWV', NULL, 'endoplasmic reticulum membrane, peripheral membrane, cytoplasm', NULL, 'Homo sapiens');


--
-- Estructura de tabla para la tabla `Phosphorylation` ---------------------------
--
CREATE TABLE `Phosphorylation`(
  `p_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `p_res` varchar(20) DEFAULT NULL -- position residue being phosphorylated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Phosphorylation`
--
INSERT INTO `Phosphorylation` (`p_id`, `prot_id`, `p_res`) 
VALUES 
(1, 'P23528', 'S3-p'), 
(2, 'P23528', 'S8-p'), 
(3, 'P23528', 'S23-p'), 
(4, 'P23528', 'S24-p'), 
(5, 'P23528', 'T25-p'), 
(6, 'P23528', 'S41-p'), 
(7, 'P23528', 'T63-p'), 
(8, 'P23528', 'Y68-p'), 
(9, 'P23528', 'T70-p'), 
(10, 'P23528', 'Y82-p'), 
(11, 'P23528', 'Y85-p'), 
(12, 'P23528', 'T88-p'), 
(13, 'P23528', 'Y89-p'), 
(14, 'P23528', 'T91-p'), 
(15, 'P23528', 'S94-p'), 
(16, 'P23528', 'S108-p'), 
(17, 'P23528', 'Y117-p'), 
(18, 'P23528', 'S119-p'), 
(19, 'P23528', 'T129-p'), 
(20, 'P23528', 'Y140-p'), 
(21, 'P23528', 'S156-p'), 
(22, 'P23528', 'S160-p'), 
(23, 'P01009', 'T37-p'), 
(24, 'P01009', 'S38-p'), 
(25, 'P01009', 'Y184-p'), 
(26, 'P01009', 'S261-p'), 
(27, 'P01009', 'S307-p'), 
(28, 'P01009', 'S309-p'), 
(29, 'P01009', 'S316-p'), 
(30, 'P01009', 'T318-p'), 
(31, 'P01009', 'Y321-p'), 
(32, 'P01009', 'S325-p'), 
(33, 'P01009', 'T333-p'), 
(34, 'P01009', 'S343-p'), 
(35, 'P01009', 'S383-p'), 
(36, 'P01009', 'T416-p'), 
(37, 'P25963', 'S32-p'), 
(38, 'P25963', 'S36-p'), 
(39, 'P25963', 'Y42-p'), 
(40, 'P25963', 'T90-p'), 
(41, 'P25963', 'S166-p'), 
(42, 'P25963', 'S283-p'), 
(43, 'P25963', 'S288-p'), 
(44, 'P25963', 'T291-p'), 
(45, 'P25963', 'S293-p'), 
(46, 'P25963', 'T299-p'), 
(47, 'P25963', 'Y305-p'), 
(48, 'A0A0E1M553', 'Any'), 
(49, 'P68135', 'Any'), 
(50, 'P10987', 'Any'), 
(51, 'P28348', 'Any'), 
(52, 'Q08209', 'S2-p'), 
(53, 'Q08209', 'Y119-p'), 
(54, 'Q08209', 'Y159-p'), 
(55, 'Q08209', 'S411-p'), 
(56, 'Q08209', 'T427-p'), 
(57, 'Q08209', 'S462-p'), 
(58, 'Q08209', 'S492-p'), 
(59, 'Q08209', 'S498-p'), 
(60, 'Q08209', 'S512-p'), 
(61, 'Q08209', 'S515-p'), 
(62, 'Q08209', 'S518-p'), 
(63, 'Q6PHZ2', 'S26-p'), 
(64, 'Q6PHZ2', 'S79-p'), 
(65, 'Q6PHZ2', 'S146-p'), 
(66, 'Q6PHZ2', 'Y231-p'), 
(67, 'Q6PHZ2', 'S235-p'), 
(68, 'Q6PHZ2', 'T254-p'), 
(69, 'Q6PHZ2', 'S276-p'), 
(70, 'Q6PHZ2', 'S280-p'), 
(71, 'Q6PHZ2', 'T287-p'), 
(72, 'Q6PHZ2', 'T306-p'), 
(73, 'Q6PHZ2', 'T307-p'), 
(74, 'Q6PHZ2', 'T311-p'), 
(75, 'Q6PHZ2', 'S315-p'), 
(76, 'Q6PHZ2', 'S319-p'), 
(77, 'Q6PHZ2', 'S330-p'), 
(78, 'Q6PHZ2', 'T331-p'), 
(79, 'Q6PHZ2', 'S333-p'), 
(80, 'Q6PHZ2', 'S334-p'), 
(81, 'Q6PHZ2', 'T336-p'), 
(82, 'Q6PHZ2', 'T337-p'), 
(83, 'Q6PHZ2', 'S441-p'), 
(84, 'Q6PHZ2', 'S472-p'), 
(85, 'Q6PHZ2', 'S494-p'), 
(86, 'P11035', 'Any'), 
(87, 'P52901', 'Any'), 
(88, 'P00004', 'Any'), 

(89, 'A0AVT1', 'S16-p'),  
(90, 'A0AVT1', 'S36-p'),  
(91, 'A0AVT1', 'Y44-p'),  
(92, 'A0AVT1', 'T54-p'),  
(93, 'A0AVT1', 'T146-p'),  
(94, 'A0AVT1', 'S149-p'),  
(95, 'A0AVT1', 'T241-p'),  
(96, 'A0AVT1', 'T246-p'),  
(97, 'A0AVT1', 'S301-p'),  
(98, 'A0AVT1', 'T637-p'),  
(99, 'A0AVT1', 'S737-p'),  
(100, 'A0AVT1', 'S743-p'),  
(101, 'A0AVT1', 'S951-p'),  
(102, 'A0AVT1', 'Y979-p'),  
(103, 'A0AVT1', 'Y994-p'),  
(104, 'A0AVT1', 'T1051-p'),  
(105, 'A2RRP1', 'S11-p'),  
(106, 'A2RRP1', 'T294-p'),  
(107, 'A2RRP1', 'S307-p'),  
(108, 'A2RRP1', 'S473-p'),  
(109, 'A2RRP1', 'S475-p'),  
(110, 'A2RRP1', 'Y477-p'),  
(111, 'A2RRP1', 'S480-p'),  
(112, 'A2RRP1', 'Y485-p'),  
(113, 'A2RRP1', 'Y488-p'),  
(114, 'A2RRP1', 'Y494-p'),  
(115, 'A2RRP1', 'T497-p'),  
(116, 'A2RRP1', 'T511-p'),  
(117, 'A2RRP1', 'T525-p'),  
(118, 'A2RRP1', 'Y539-p'),  
(119, 'A2RRP1', 'Y549-p'),  
(120, 'A2RRP1', 'Y557-p'),  
(121, 'A2RRP1', 'S564-p'),  
(122, 'A2RRP1', 'Y640-p'),  
(123, 'A2RRP1', 'T1027-p'),  
(124, 'A2RRP1', 'T1030-p'),  
(125, 'A2RRP1', 'S1145-p'),  
(126, 'A2RRP1', 'S1243-p'),  
(127, 'A2RRP1', 'S1259-p'),  
(128, 'A2RRP1', 'T1260-p'),  
(129, 'A2RRP1', 'S1382-p'),  
(130, 'A2RRP1', 'S1388-p'),  
(131, 'A2RRP1', 'S1441-p'),  
(132, 'A2RRP1', 'S1842-p'),  
(133, 'A2RRP1', 'S1869-p'),  
(134, 'A2RRP1', 'S1870-p'),  
(135, 'A2RRP1', 'Y1877-p'),  
(136, 'A2RRP1', 'Y1883-p'),  
(137, 'A2RRP1', 'T1894-p'),  
(138, 'A2RRP1', 'S1902-p'),  
(139, 'A2RRP1', 'S2052-p');



--
-- Estructura de tabla para la tabla `Acetylation` ---------------------------
--
CREATE TABLE `Acetylation`(
  `ac_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `ac_res` varchar(20) DEFAULT NULL -- position residue being acetylated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Acetylation`
--
INSERT INTO `Acetylation` (`ac_id`, `prot_id`, `ac_res`) 
VALUES 
(1, 'P23528', 'K13-ac'), 
(2, 'P23528', 'K19-ac'), 
(3, 'P23528', 'K30-ac'), 
(4, 'P23528', 'K31-ac'), 
(5, 'P23528', 'K33-ac'), 
(6, 'P23528', 'K44-ac'), 
(7, 'P23528', 'K45-ac'), 
(8, 'P23528', 'K73-ac'), 
(9, 'P23528', 'K78-ac'), 
(10, 'P23528', 'K92-ac'), 
(11, 'P23528', 'K96-ac'), 
(12, 'P23528', 'K114-ac'), 
(13, 'P23528', 'K121-ac'), 
(14, 'P23528', 'K125-ac'), 
(15, 'P23528', 'K126-ac'), 
(16, 'P23528', 'K127-ac'), 
(17, 'P23528', 'K132-ac'), 
(18, 'P23528', 'K144-ac'), 
(19, 'P01009', 'K159-ac'), 
(20, 'P01009', 'K160-ac'), 
(21, 'P01009', 'K198-ac'), 
(22, 'P01009', 'K257-ac'), 
(23, 'P01009', 'K314-ac'), 
(24, 'P25963', 'Any'), 
(25, 'A0A0E1M553', 'Any'), 
(26, 'P68135', 'Any'), 
(27, 'P10987', 'Any'), 
(28, 'P28348', 'Any'), 
(29, 'Q08209', 'Any'), 
(30, 'Q6PHZ2', 'K48-ac'), 
(31, 'P11035', 'Any'), 
(32, 'P52901', 'Any'), 
(33, 'P00004', 'K9-ac'), 
(34, 'P00004', 'K28-ac'), 
(35, 'P00004', 'K73-ac'), 
(36, 'P00004', 'K87-ac'), 
(37, 'P00004', 'K88-ac'), 

(38, 'A0AVT1', 'K544-ac'), 
(39, 'A0AVT1', 'K1003-ac'), 
(40, 'A2RRP1', 'K1057-ac'), 
(41, 'A2RRP1', 'K1261-ac'), 
(42, 'A2RRP1', 'K1931-ac');

--
-- Estructura de tabla para la tabla `Methylation` ---------------------------
--
CREATE TABLE `Methylation`(
  `m_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `m_res` varchar(20) DEFAULT NULL -- position residue being methylated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Methylation`
--
INSERT INTO `Methylation` (`m_id`, `prot_id`, `m_res`) 
VALUES 
(1, 'P23528', 'K19-m1'), 
(2, 'P23528', 'K96-m3'), 
(3, 'P01009', 'Any'), 
(4, 'P25963', 'Any'), 
(5, 'A0A0E1M553', 'Any'), 
(6, 'P68135', 'Any'), 
(7, 'P10987', 'Any'), 
(8, 'P28348', 'Any'), 
(9, 'Q08209', 'R16-m1'), 
(10, 'Q6PHZ2', 'K299-m1'), 
(11, 'Q6PHZ2', 'K301-m1'), 
(12, 'P11035', 'Any'), 
(13, 'P52901', 'Any'), 
(14, 'P00004', 'Any'), 

(15, 'A0AVT1', 'Any'), 
(16, 'A2RRP1', 'Any'); 
--
-- Estructura de tabla para la tabla `Ubiquitination` ---------------------------
--
CREATE TABLE `Ubiquitination`(
  `u_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `u_res` varchar(20) DEFAULT NULL -- position residue being ubiquitinated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Ubiquitination`
--
INSERT INTO `Ubiquitination` (`u_id`, `prot_id`, `u_res`) 
VALUES 
(1, 'P23528', 'K13-ub'), 
(2, 'P23528', 'K19-ub'), 
(3, 'P23528', 'K22-ub'), 
(4, 'P23528', 'K30-ub'), 
(5, 'P23528', 'K34-ub'), 
(6, 'P23528', 'K44-ub'), 
(7, 'P23528', 'K45-ub'), 
(8, 'P23528', 'K53-ub'), 
(9, 'P23528', 'K73-ub'), 
(10, 'P23528', 'K78-ub'), 
(11, 'P23528', 'K92-ub'), 
(12, 'P23528', 'K95-ub'), 
(13, 'P23528', 'K96-ub'), 
(14, 'P23528', 'K112-ub'), 
(15, 'P23528', 'K114-ub'), 
(16, 'P23528', 'K121-ub'), 
(17, 'P23528', 'K132-ub'), 
(18, 'P23528', 'K144-ub'), 
(19, 'P23528', 'K152-ub'), 
(20, 'P23528', 'K164-ub'), 
(21, 'P01009', 'Any'), 
(22, 'P25963', 'K21-ub'), 
(23, 'P25963', 'K22-ub'), 
(24, 'P25963', 'K38-ub'), 
(25, 'P25963', 'K47-ub'), 
(26, 'P25963', 'K67-ub'), 
(27, 'P25963', 'K87-ub'), 
(28, 'P25963', 'K98-ub'), 
(29, 'P25963', 'K238-ub'), 
(30, 'A0A0E1M553', 'Any'), 
(31, 'P68135', 'Any'), 
(32, 'P10987', 'Any'), 
(33, 'P28348', 'Any'), 
(34, 'Q08209', 'K32-ub'), 
(35, 'Q08209', 'K142-ub'), 
(36, 'Q08209', 'K163-ub'), 
(37, 'Q08209', 'K293-ub'), 
(38, 'Q08209', 'K323-ub'), 
(39, 'Q08209', 'K424-ub'), 
(40, 'Q6PHZ2', 'K43-ub'), 
(41, 'Q6PHZ2', 'K48-ub'), 
(42, 'Q6PHZ2', 'K49-ub'), 
(43, 'Q6PHZ2', 'K69-ub'), 
(44, 'Q6PHZ2', 'K138-ub'), 
(45, 'Q6PHZ2', 'K227-ub'), 
(46, 'Q6PHZ2', 'K246-ub'), 
(47, 'Q6PHZ2', 'K251-ub'), 
(48, 'Q6PHZ2', 'K259-ub'), 
(49, 'Q6PHZ2', 'K268-ub'), 
(50, 'Q6PHZ2', 'K292-ub'), 
(51, 'Q6PHZ2', 'K293-ub'), 
(52, 'Q6PHZ2', 'K301-ub'), 
(53, 'P11035', 'Any'), 
(54, 'P52901', 'Any'), 
(55, 'P00004', 'Any'), 

(56, 'A0AVT1', 'K58-ub'),  
(57, 'A0AVT1', 'K169-ub'),  
(58, 'A0AVT1', 'K182-ub'),  
(59, 'A0AVT1', 'K238-ub'),  
(60, 'A0AVT1', 'K295-ub'),  
(61, 'A0AVT1', 'K490-ub'),  
(62, 'A0AVT1', 'K503-ub'),  
(63, 'A0AVT1', 'K521-ub'),  
(64, 'A0AVT1', 'K544-ub'),  
(65, 'A0AVT1', 'K628-ub'),  
(66, 'A0AVT1', 'K644-ub'),  
(67, 'A0AVT1', 'K652-ub'),  
(68, 'A0AVT1', 'K687-ub'),  
(69, 'A0AVT1', 'K709-ub'),  
(70, 'A0AVT1', 'K729-ub'),  
(71, 'A0AVT1', 'K739-ub'),  
(72, 'A0AVT1', 'K746-ub'),  
(73, 'A0AVT1', 'K800-ub'),  
(74, 'A0AVT1', 'K810-ub'),  
(75, 'A0AVT1', 'K830-ub'),  
(76, 'A0AVT1', 'K871-ub'),  
(77, 'A0AVT1', 'K882-ub'),  
(78, 'A0AVT1', 'K889-ub'),  
(79, 'A0AVT1', 'K1003-ub'),  
(80, 'A2RRP1', 'K115-ub'),  
(81, 'A2RRP1', 'K265-ub'),  
(82, 'A2RRP1', 'K284-ub'),  
(83, 'A2RRP1', 'K416-ub'),  
(84, 'A2RRP1', 'K490-ub'),  
(85, 'A2RRP1', 'K563-ub'),  
(86, 'A2RRP1', 'K600-ub'),  
(87, 'A2RRP1', 'K620-ub'),  
(88, 'A2RRP1', 'K673-ub'),  
(89, 'A2RRP1', 'K679-ub'),  
(90, 'A2RRP1', 'K687-ub'),  
(91, 'A2RRP1', 'K799-ub'),  
(92, 'A2RRP1', 'K841-ub'),  
(93, 'A2RRP1', 'K944-ub'),  
(94, 'A2RRP1', 'K955-ub'),  
(95, 'A2RRP1', 'K977-ub'),  
(96, 'A2RRP1', 'K1057-ub'),  
(97, 'A2RRP1', 'K1063-ub'),  
(98, 'A2RRP1', 'K1086-ub'),  
(99, 'A2RRP1', 'K1169-ub'),  
(100, 'A2RRP1', 'K1246-ub'),  
(101, 'A2RRP1', 'K1261-ub'),  
(102, 'A2RRP1', 'K1314-ub'),  
(103, 'A2RRP1', 'K1440-ub'),  
(104, 'A2RRP1', 'K1452-ub'),  
(105, 'A2RRP1', 'K1667-ub'),  
(106, 'A2RRP1', 'K1908-ub'),  
(107, 'A2RRP1', 'K1986-ub'),  
(108, 'A2RRP1', 'K2213-ub');



--
-- Estructura de tabla para la tabla `Sumoylation` ---------------------------
--
CREATE TABLE `Sumoylation`(
  `s_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `s_res` varchar(20) DEFAULT NULL -- position residue being sumoylated 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `Sumoylation`
-- 
INSERT INTO `Sumoylation` (`s_id`, `prot_id`, `s_res`) 
VALUES 
(1, 'P23528', 'K13-sm'), 
(2, 'P23528', 'K19-sm'), 
(3, 'P23528', 'K30-sm'), 
(4, 'P23528', 'K34-sm'), 
(5, 'P23528', 'K44-sm'), 
(6, 'P23528', 'K45-sm'), 
(7, 'P23528', 'K53-sm'), 
(8, 'P23528', 'K112-sm'), 
(9, 'P23528', 'K114-sm'), 
(10, 'P23528', 'K121-sm'), 
(11, 'P23528', 'K132-sm'), 
(12, 'P23528', 'K164-sm'), 
(13, 'P01009', 'Any'), 
(14, 'P25963', 'K21-sm'), 
(15, 'P25963', 'K22-sm'), 
(16, 'A0A0E1M553', 'Any'), 
(17, 'P68135', 'Any'), 
(18, 'P10987', 'Any'), 
(19, 'P28348', 'Any'), 
(20, 'Q08209', 'Any'), 
(21, 'Q6PHZ2', 'Any'), 
(22, 'P11035', 'Any'), 
(23, 'P52901', 'Any'), 
(24, 'P00004', 'Any'), 

(25, 'A0AVT1', 'Any'), 
(26, 'A2RRP1', 'Any'); 
--
-- Estructura de tabla para la tabla `OGlcNAc` ---------------------------
--
CREATE TABLE `OGlcNAc`(
  `g_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `g_res` varchar(20) DEFAULT NULL -- position residue being glycosilated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `OGlcNAc`
-- 
INSERT INTO `OGlcNAc` (`g_id`, `prot_id`, `g_res`) 
VALUES 
(1, 'P23528', 'Any'), 
(2, 'P01009', 'Any'), 
(3, 'P25963', 'Any'), 
(4, 'A0A0E1M553', 'Any'), 
(5, 'P68135', 'Any'), 
(6, 'P10987', 'Any'), 
(7, 'P28348', 'Any'), 
(8, 'Q08209', 'Any'), 
(9, 'Q6PHZ2', 'T307-gl'), 
(10, 'P11035', 'Any'), 
(11, 'P52901', 'Any'), 
(12, 'P00004', 'Any'), 

(13, 'A0AVT1', 'Any'), 
(14, 'A2RRP1', 'Any'); 


--
-- Estructura de tabla para la tabla `RegPTM` ---------------------------
--
CREATE TABLE `RegPTM`(
  `r_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `r_res` varchar(20) DEFAULT NULL -- position residue being targeted
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `RegPTM`
-- 
INSERT INTO `RegPTM` (`r_id`, `prot_id`, `r_res`) 
VALUES 
(1, 'P23528', 'S3-p'), 
(2, 'P23528', 'S23-p'), 
(3, 'P23528', 'Y68-p'), 
(4, 'P23528', 'S24-p'), 
(5, 'P01009', 'T416-p'), 
(6, 'P25963', 'K22-ub'), 
(7, 'P25963', 'T291-p'), 
(8, 'P25963', 'T299-p'), 
(9, 'P25963', 'S283-p'), 
(10, 'P25963', 'S293-p'), 
(11, 'P25963', 'Y42-p'), 
(12, 'P25963', 'Y305-p'), 
(13, 'P25963', 'S32-p'), 
(14, 'P25963', 'K21-ub'), 
(15, 'P25963', 'S36-p'), 
(16, 'A0A0E1M553', 'Any'), 
(17, 'P68135', 'Any'), 
(18, 'P10987', 'Any'), 
(19, 'P28348', 'Any'), 
(20, 'Q08209', 'Any'), 
(21, 'Q6PHZ2', 'T287-p'), 
(22, 'P11035', 'Any'), 
(23, 'P52901', 'Any'), 
(24, 'P00004', 'Any'), 

(25, 'A0AVT1', 'Any'), 
(26, 'A2RRP1', 'Any');

--
-- Estructura de tabla para la tabla `Disease` ---------------------------
--
CREATE TABLE `Disease`(
  `di_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `di_res` varchar(20) DEFAULT NULL -- position residue being targeted
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `Disease`
-- 
INSERT INTO `Disease` (`di_id`, `prot_id`, `di_res`) 
VALUES 
(1, 'P23528', 'Any'), 
(2, 'P01009', 'Any'), 
(3, 'P25963', 'Any'), 
(4, 'A0A0E1M553', 'Any'), 
(5, 'P68135', 'Any'), 
(6, 'P10987', 'Any'), 
(7, 'P28348', 'Any'), 
(8, 'Q08209', 'Any'), 
(9, 'Q6PHZ2', 'Any'), 
(10, 'P11035', 'Any'), 
(11, 'P52901', 'Any'), 
(12, 'P00004', 'Any'), 

(13, 'A0AVT1', 'Any'), 
(14, 'A2RRP1', 'Any');


--
-- Estructura de tabla para la tabla `Regulatory`--------------------------------
--
CREATE TABLE `Regulatory`( 
  `reg_id` int(11) NOT NULL, 
  `unknown` tinyint(1) NOT NULL,
  `act_gain` tinyint(1) NOT NULL, 
  `act_loss` tinyint(1) NOT NULL, 
  `ppi_gain` tinyint(1) NOT NULL, 
  `ppi_loss` tinyint(1) NOT NULL,
  `stability`tinyint(1) NOT NULL,
  `relocation`tinyint(1) NOT NULL,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Regulatory`
--
INSERT INTO `Regulatory` (`reg_id`, `unknown`, 
  `act_gain`, `act_loss`, `ppi_gain`, `ppi_loss`, `stability`, `relocation`) 
VALUES 
(1,1,0,0,0,0,0,0),
(2,0,0,0,0,0,0,0),

(3,0,  1,0,0,0,0,0),
(4,0,  0,1,0,0,0,0),
(5,0,  0,0,1,0,0,0),
(6,0,  0,0,0,1,0,0),
(7,0,  0,0,0,0,1,0),
(8,0,  0,0,0,0,0,1),

(9,0,  0,0,0,1,0,1),
(10,0, 0,0,0,1,1,0),
(11,0, 0,0,1,0,0,1),
(12,0, 0,0,1,0,1,0),
(13,0, 0,1,0,0,0,1),
(14,0, 0,1,0,0,1,0),
(15,0, 1,0,0,0,0,1),
(16,0, 1,0,0,0,1,0),
(17,0, 0,0,1,1,0,0),
(18,0, 0,1,0,1,0,0),
(19,0, 0,1,1,0,0,0),
(20,0, 1,0,0,1,0,0),
(21,0, 1,0,1,0,0,0),
(22,0, 1,1,0,0,0,0),
(23,0, 0,0,0,0,1,1),

(24,0, 0,0,0,1,1,1),
(25,0, 0,0,1,0,1,1),
(26,0, 0,1,0,0,1,1),
(27,0, 1,0,0,0,1,1),
(28,0, 0,0,1,1,0,1),
(29,0, 0,0,1,1,1,0),
(30,0, 1,1,1,0,0,0),
(31,0, 0,1,0,1,0,1),
(32,0, 0,1,0,1,1,0),
(33,0, 0,1,1,0,0,1),
(34,0, 0,1,1,0,1,0),
(35,0, 1,0,0,1,0,1),
(36,0, 1,0,0,1,1,0),
(37,0, 1,0,1,0,0,1),
(38,0, 1,0,1,0,1,0),
(39,0, 1,1,0,0,0,1),
(40,0, 1,1,0,0,1,0),
(41,0, 0,1,1,1,0,0),
(42,0, 1,0,1,1,0,0),
(43,0, 1,1,0,1,0,0),


(44,0, 0,0,1,1,1,1),
(45,0, 0,1,0,1,1,1),
(46,0, 0,1,1,0,1,1),
(47,0, 1,0,0,1,1,1),
(48,0, 1,0,1,0,1,1),
(49,0, 1,1,0,0,1,1),
(50,0, 0,1,1,1,0,1),
(51,0, 0,1,1,1,1,0),
(52,0, 1,0,1,1,0,1),
(53,0, 1,0,1,1,1,0),
(54,0, 1,1,0,1,0,1),
(55,0, 1,1,0,1,1,0),
(56,0, 1,1,1,1,0,0),
(57,0, 1,1,1,0,0,1),
(58,0, 1,1,1,0,1,0),

(59,0, 0,1,1,1,1,1),
(60,0, 1,0,1,1,1,1),
(61,0, 1,1,0,1,1,1),
(62,0, 1,1,1,0,1,1),
(63,0, 1,1,1,1,0,1),
(64,0, 1,1,1,1,1,0),

(65,0, 1,1,1,1,1,1);

--
-- Estructura de tabla para la tabla `Organism` ---------------------------
--
CREATE TABLE `Organism`(
  `org_id`int(11) NOT NULL,
  `org_sp`varchar(50) NOT NULL,
  `org_tissue` varchar(100) DEFAULT NULL,
  `org_oxidant`varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Organism`
--
INSERT INTO `Organism` (`org_id`, `org_sp`, `org_tissue`, `org_oxidant`)
VALUES 
(1, 'Homo sapiens', NULL, 'taurine chloramine (TnCl)'), 
(2, 'Homo sapiens', 'extracellular', 'hydrogen peroxidie (H2O2)'), 
(3, 'Homo sapiens', 'Jurkat cells', 'taurine chloramine (TnCl)'), 
(4, 'Escherichia coli', NULL, 'hypochlorite (HOCl)'), 
(5, 'Oryctolagus cuniculus', 'muscle', 'mical-catalyzed'), 
(6, 'Drosophila melanogaster', NULL, 'mical-catalyzed'), 
(7, 'Aspergillus nidulans', NULL, 'unknown'), 
(8, 'Homo sapiens', NULL, 'hydrogen peroxide (H2O2)'), 
(9, 'Homo sapiens', 'Jurkat cells', 'hydrogen peroxide (H2O2)'), 
(10, 'Mus musculus', 'heart', 'hydrogen peroxide (H2O2), ROS, angiotensin II, ischaemia'), 
(11, 'Arabidopsis thaliana', 'leaf', 'hydrogen peroxide (H2O2)'), 
(12, 'Equus caballus', NULL, 'hypochlorite (HOCl)'), 
(13, 'Drosophila melanogaster', NULL, 'chloramine-T, peroxinitrite (ONOO)'), 
(14, 'Saccharomyces cerevisiae', NULL, 'hydrogen peroxide (H2O2), ROS'), 
(15, 'Homo sapiens', NULL, 'NOR3'), 
(16, 'Homo sapiens', 'blood', 'hypoclorite (HClO), hydrogen peroxide (H2O2)'); 

--
-- Estructura de tabla para la tabla `Taxon` ---------------------------
--
CREATE TABLE `Taxon`(
  `org_sp`varchar(50) NOT NULL,
  `domain`varchar(50) NOT NULL,
  `kingdom` varchar(50) DEFAULT NULL,
  `phylum` varchar(50) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` varchar(50) DEFAULT NULL,
  `family` varchar(50) DEFAULT NULL,
  `genus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`org_sp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Taxon`
--
INSERT INTO `Taxon` (`org_sp`, `domain`, `kingdom`, `phylum`, `class`, `order`, `family`, `genus`)
VALUES 
('Arabidopsis thaliana', 'Eukaryota', 'Plantae', NULL, 'Magnoliopsida', 'Capparales', 'Brassicaceae', 'Arabidopsis'), 
('Aspergillus nidulans', 'Eukaryota', 'Fungi', 'Ascomycota', 'Eurotiomycetes', 'Eurotiales', 'Trichomaceae', 'Aspergillus'), 
('Drosophila melanogaster', 'Eukaryota', 'Animalia', 'Arthropoda', 'Insecta', 'Diptera', 'Drosophilidae', 'Drosophila'), 
('Equus caballus', 'Eukaryota', 'Animalia', 'Chordata', 'Mammalia', 'Perissodactyla', 'Equidae', 'Equus'), 
('Escherichia coli', 'Bacteria', NULL, 'Proteobacteria', 'Gammaproteobacteria', 'Enterobacteriales', 'Enterobacteriaceae', 'Escherichia'), 
('Homo sapiens', 'Eukaryota', 'Animalia', 'Chordata', 'Mammalia', 'Primates', 'Hominidae', 'Homo'), 
('Mus musculus', 'Eukaryota', 'Animalia', 'Chordata', 'Mammalia', 'Rodentia', 'Muridae', 'Mus'), 
('Oryctolagus cuniculus', 'Eukaryota', 'Animalia', 'Chordata', 'Mammalia', 'Lagomorpha', 'Leporidae', 'Oryctolagus'), 
('Saccharomyces cerevisiae', 'Eukaryota', 'Fungi', 'Ascomycota', 'Hemiascomycetes', 'Saccharomycetales', 'Saccharomycetaceae', 'Saccharomyces'); 


--
-- Estructura de tabla para la tabla `DOI` ---------------------------
--
CREATE TABLE `DOI`(
  `doi_id`int(11) PRIMARY KEY,
  `ref` varchar(300) NOT NULL,
  `character` int(2) DEFAULT NULL -- 1 for key references and 0 for the remaining
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `DOI`
--
INSERT INTO `DOI` (`doi_id`, `ref`, `character`)
VALUES
(1, '10.1016/j.freeradbiomed.2014.07.018', 1), 
(2, '10.1074/jbc.M004850200', 1), 
(3, '10.1074/jbc.M110832200', 1), 
(4, '10.1073/pnas.1300578110/-/DCSupplemen', 1), 
(5, '10.1016/j.molcel.2013.06.019', 1), 
(6, '10.1038/ncb2871', 1), 
(7, '10.1126/science.1211956', 0), 
(8, '10.1371/journal.pgen.1005297', 1), 
(9, '10.1021/bi702044x', 1), 
(10, '10.1016/j.cell.2008.02.048', 1), 
(11, '10.1042/BJ20090764', 1), 
(12, 'Proteomics Insights 2009:2', 1), 
(13, '10.3389/fpls.2012.00153', 0), 
(14, '10.1074/jbc.M200709200', 1), 
(15, '10.1073/pnas.0809279106', 0), 

(16,  '10.1074/mcp.M110.006866' , 1);


/* '10.1074/mcp.M110.006866' Ghesquiere 2011*/



--
-- Estructura de tabla para la tabla `Evidence` ---------------------------
--
CREATE TABLE `Evidence`(
  `evi_id`int(11) PRIMARY KEY,
  `met_id` int(11) NOT NULL,
  `doi_id` int(11) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Evidence`
--
INSERT INTO `Evidence` (`evi_id`, `met_id`, `doi_id`)
VALUES 
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 3),
(5, 5, 4),
(6, 6, 4), 
(7, 7, 4),
(8, 8, 5),
(9, 8, 7),
(10, 9, 5),
(11, 9, 7),
(12, 10, 6),
(13, 10, 7),
(14, 11, 6),
(15, 11, 7),
(16, 12, 8),
(17, 13, 9),
(18, 14, 10),
(19, 15, 10), 
(20, 16, 11),
(21, 17, 12),
(22, 17, 13),
(23, 18, 14),
(24, 18, 15),

(25, 19, 16),
(26, 20, 16),
(27, 21, 16),
(28, 22, 16);
--
-- Foreign key
--

ALTER TABLE Methionine
  ADD CONSTRAINT fk_Methionine_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Methionine
  ADD CONSTRAINT fk_Methionine_Regulatory
  FOREIGN KEY (reg_id)
  REFERENCES Regulatory (reg_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Methionine
  ADD CONSTRAINT fk_Methionine_Organism
  FOREIGN KEY (org_id)
  REFERENCES Organism (org_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Phosphorylation
  ADD CONSTRAINT fk_Phosphorylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Acetylation
  ADD CONSTRAINT fk_Acetylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Methylation
  ADD CONSTRAINT fk_Methylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Ubiquitination
  ADD CONSTRAINT fk_Ubiquitination_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Sumoylation
  ADD CONSTRAINT fk_Sumoylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE OGlcNAc
  ADD CONSTRAINT fk_OGlcNAc_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE RegPTM
  ADD CONSTRAINT fk_RegPTM_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Disease
  ADD CONSTRAINT fk_Disease_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Evidence
  ADD CONSTRAINT fk_Evidence_DOI
  FOREIGN KEY (doi_id)
  REFERENCES DOI (doi_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Evidence
  ADD CONSTRAINT fk_Evidence_Methionine
  FOREIGN KEY (met_id)
  REFERENCES Methionine (met_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Organism
  ADD CONSTRAINT fk_Organism_Taxon
  FOREIGN KEY (org_sp)
  REFERENCES Taxon (org_sp)
  ON DELETE CASCADE ON UPDATE CASCADE;
 