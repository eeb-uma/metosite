
--
-- Estructura de tabla para la tabla `Methionine` ------------------------
--

CREATE TABLE `Methionine` (
  `met_id` int(11) NOT NULL, 
  `met_pos` int(4) NOT NULL,
  `met_ext` int(3) DEFAULT NULL,
  `met_vivo_vitro` varchar(5) DEFAULT NULL,
  `prot_id` varchar(20) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `org_id`int(11) NOT NULL,
  PRIMARY KEY (`met_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `Methionine`
--
INSERT INTO `Methionine` (`met_id`, `met_pos`, `met_ext`, `met_vivo_vitro`,
`prot_id`, `reg_id`, `org_id`) 
VALUES 

--
-- ------- Methionine Jurkat -----------------
--

(1, 492, 10.76, 'vivo', 'A0AVT1',1, 9), 
(2, 56, 11.87, 'vivo', 'A0AVT1',1, 9), 
(3, 2042, 11.7, 'vivo', 'A2RRP1',1, 9), 
(4, 2204, 0.18, 'vivo', 'A2RRP1',1, 9), 
(5, 1050, 0.21, 'vivo', 'A3KN83',1, 9), 
(6, 167, 17.3, 'vivo', 'A3KN83',1, 9), 
(7, 1927, 15.44, 'vivo', 'A5YKK6',1, 9), 
(8, 2361, 6.57, 'vivo', 'A5YKK6',1, 9), 
(9, 2154, 22.25, 'vivo', 'A5YKK6',1, 9), 
(10, 216, 1.06, 'vivo', 'A6NEC2',1, 9), 

--
-- ------- Methionine Arabidopsis -----------------
--

(11, 292, NULL, 'vivo', 'P0DI78',1, 38),  
(12, 9, NULL, 'vivo', 'Q9SMW7',1, 38),  
(13, 62, NULL, 'vivo', 'Q9M8S0',1, 38),  
(14, 445, NULL, 'vivo', 'O04318',1, 38),  
(15, 1, NULL, 'vivo', 'Q9FG81',1, 38), 

--
-- ----------------- Methionine Regulatory ----------------------- 
--

(16, 115, NULL, 'vitro', 'P23528',18, 1),  
(17, 351, NULL, 'both', 'P01009',4, 2),  
(18, 358, NULL, 'both', 'P01009',4, 2),  
(19, 123, NULL, 'both', 'A0A0E1M553',3, 4),  
(20, 206, NULL, 'both', 'A0A0E1M553',3, 4),  
(21, 230, NULL, 'both', 'A0A0E1M553',3, 4),
(22, 80, NULL, 'vitro', 'P00004',39, 12),
(23, 538, NULL, 'both', 'P11035',3, 11);



--
-- Estructura de tabla para la tabla `Protein` ---------------------------
--
CREATE TABLE `Protein`( 
  `prot_id` varchar(20) NOT NULL,
  `prot_name`varchar(250) DEFAULT NULL,
  `prot_nickname`varchar(250) DEFAULT NULL,
  `gene_name`varchar(250) DEFAULT NULL,
  `prot_seq` text DEFAULT NULL,
  `prot_pdb` varchar(5) DEFAULT NULL, 
  `prot_sub` varchar(1000) DEFAULT NULL,
  `prot_note` text DEFAULT NULL,
  `prot_sp` varchar(35) NOT NULL,
  PRIMARY KEY (`prot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Protein`
--

INSERT INTO `Protein` (`prot_id`, `prot_name`,  `prot_nickname`, `gene_name`, `prot_seq`, 
`prot_pdb`, `prot_sub`, `prot_note`, `prot_sp`) 
VALUES 

-- 
-- -------- Protein Jurkat -----------------
--
('A0AVT1', 'Ubiquitin-like modifier-activating enzyme 6', NULL, 'UBA6', 'MEGSEPVAAHQGEEASCSSWGTGSTNKNLPIMSTASVEIDDALYSRQRYVLGDTAMQKMAKSHVFLSGMGGLGLEIAKNLVLAGIKAVTIHDTEKCQAWDLGTNFFLSEDDVVNKRNRAEAVLKHIAELNPYVHVTSSSVPFNETTDLSFLDKYQCVVLTEMKLPLQKKINDFCRSQCPPIKFISADVHGIWSRLFCDFGDEFEVLDTTGEEPKEIFISNITQANPGIVTCLENHPHKLETGQFLTFREINGMTGLNGSIQQITVISPFSFSIGDTTELEPYLHGGIAVQVKTPKTVFFESLERQLKHPKCLIVDFSNPEAPLEIHTAMLALDQFQEKYSRKPNVGCQQDSEELLKLATSISETLEEKPDVNADIVHWLSWTAQGFLSPLAAAVGGVASQEVLKAVTGKFSPLCQWLYLEAADIVESLGKPECEEFLPRGDRYDALRACIGDTLCQKLQNLNIFLVGCGAIGCEMLKNFALLGVGTSKEKGMITVTDPDLIEKSNLNRQFLFRPHHIQKPKSYTAADATLKINSQIKIDAHLNKVCPTTETIYNDEFYTKQDVIITALDNVEARRYVDSRCLANLRPLLDSGTMGTKGHTEVIVPHLTESYNSHRDPPEEEIPFCTLKSFPAAIEHTIQWARDKFESSFSHKPSLFNKFWQTYSSAEEVLQKIQSGHSLEGCFQVIKLLSRRPRNWSQCVELARLKFEKYFNHKALQLLHCFPLDIRLKDGSLFWQSPKRPPSPIKFDLNEPLHLSFLQNAAKLYATVYCIPFAEEDLSADALLNILSEVKIQEFKPSNKVVQTDETARKPDHVPISSEDERNAIFQLEKAILSNEATKSDLQMAVLSFEKDDDHNGHIDFITAASNLRAKMYSIEPADRFKTKRIAGKIIPAIATTTATVSGLVALEMIKVTGGYPFEAYKNCFLNLAIPIVVFTETTEVRKTKIRNGISFTIWDRWTVHGKEDFTLLDFINAVKEKYGIEPTMVVQGVKMLYVPVMPGHAKRLKLTMHKLVKPTTEKKYVDLTVSFAPDIDGDEDLPGPPVRYYFSHDTD', NULL, 'cytosol, cytoplasm', NULL, 'Homo sapiens'), 
('A2RRP1', 'Neuroblastoma-amplified sequence', NULL, 'NBAS', 'MAAPESGPALSPGTAEGEEETILYDLLVNTEWPPETEVQPRGNQKHGASFIITKAIRDRLLFLRQYIWYSPAPFLLPDGLVRLVNKQINWHLVLASNGKLLAAVQDQCVEIRSAKDDFTSIIGKCQVPKDPKPQWRRVAWSYDCTLLAYAESTGTVRVFDLMGSELFVISPASSFIGDLSYAIAGLIFLEYKASAQWSAELLVINYRGELRSYLVSVGTNQSYQESHCFSFSSHYPHGINTAIYHPGHRLLLVGGCETAEVGMSKASSCGLSAWRVLSGSPYYKQVTNGGDGVTAVPKTLGLLRMLSVKFYSRQGQEQDGIFKMSLSPDGMLLAAIHFSGKLSIWAIPSLKQQGEWGQNEQPGYDDLNPDWRLSTEKRKKIKDKESFYPLIDVNWWADSAVTLARCSGALTVSSVKTLKNLLGKSCEWFEPSPQVTATHDGGFLSLECEIKLAPKRSRLETRAGEEDEGEEDSDSDYEISAKARYFGYIKQGLYLVTEMERFAPPRKRPRTITKNYRLVSLRSTTPEELYQRKIESEEYEEALSLAHTYGLDTDLVYQRQWRKSAVNVASIQNYLSKIKKRSWVLHECLERVPENVDAAKELLQYGLKGTDLEALLAIGKGADDGRFTLPGEIDIDSISYEELSPPDEEPAKNKKEKELKKRQELLKLVNFSKLTLEQKELCRCRRKLLTYLDRLATYEEILGVPHASEQRYDAEFFKKFRNQNIVLSARTYAQESNVQALEILFTYHGSDLLPHRLAILSNFPETTSPHEYSVLLPEACFNGDSLMIIPWHEHKHRAKDWCEELACRMVVEPNLQDESEFLYAAQPELLRFRMTQLTVEKVMDWYQTRAEEIEHYARQVDCALSLIRLGMERNIPGLLVLCDNLVTLETLVYEARCDVTLTLKELQQMKDIEKLRLLMNSCSEDKYVTSAYQWMVPFLHRCEKQSPGVANELLKEYLVTLAKGDLKFPLKIFQHSKPDLQQKIIPDQDQLMAIALECIYTCERNDQLCLCYDLLECLPERGYGDKTEATTKLHDMVDQLEQILSVSELLEKHGLEKPISFVKNTQSSSEEARKLMVRLTRHTGRKQPPVSESHWRTLLQDMLTMQQNVYTCLDSDACYEIFTESLLCSSRLENIHLAGQMMHCSACSENPPAGIAHKGKPHYRVSYEKSIDLVLAASREYFNSSTNLTDSCMDLARCCLQLITDRPPAIQEELDLIQAVGCLEEFGVKILPLQVRLCPDRISLIKECISQSPTCYKQSTKLLGLAELLRVAGENPEERRGQVLILLVEQALRFHDYKAASMHCQELMATGYPKSWDVCSQLGQSEGYQDLATRQELMAFALTHCPPSSIELLLAASSSLQTEILYQRVNFQIHHEGGENISASPLTSKAVQEDEVGVPGSNSADLLRWTTATTMKVLSNTTTTTKAVLQAVSDGQWWKKSLTYLRPLQGQKCGGAYQIGTTANEDLEKQGCHPFYESVISNPFVAESEGTYDTYQHVPVESFAEVLLRTGKLAEAKNKGEVFPTTEVLLQLASEALPNDMTLALAYLLALPQVLDANRCFEKQSPSALSLQLAAYYYSLQIYARLAPCFRDKCHPLYRADPKELIKMVTRHVTRHEHEAWPEDLISLTKQLHCYNERLLDFTQAQILQGLRKGVDVQRFTADDQYKRETILGLAETLEESVYSIAISLAQRYSVSRWEVFMTHLEFLFTDSGLSTLEIENRAQDLHLFETLKTDPEAFHQHMVKYIYPTIGGFDHERLQYYFTLLENCGCADLGNCAIKPETHIRLLKKFKVVASGLNYKKLTDENMSPLEALEPVLSSQNILSISKLVPKIPEKDGQMLSPSSLYTIWLQKLFWTGDPHLIKQVPGSSPEWLHAYDVCMKYFDRLHPGDLITVVDAVTFSPKAVTKLSVEARKEMTRKAIKTVKHFIEKPRKRNSEDEAQEAKDSKVTYADTLNHLEKSLAHLETLSHSFILSLKNSEQETLQKYSHLYDLSRSEKEKLHDEAVAICLDGQPLAMIQQLLEVAVGPLDISPKDIVQSAIMKIISALSGGSADLGGPRDPLKVLEGVVAAVHASVDKGEELVSPEDLLEWLRPFCADDAWPVRPRIHVLQILGQSFHLTEEDSKLLVFFRTEAILKASWPQRQVDIADIENEENRYCLFMELLESSHHEAEFQHLVLLLQAWPPMKSEYVITNNPWVRLATVMLTRCTMENKEGLGNEVLKMCRSLYNTKQMLPAEGVKELCLLLLNQSLLLPSLKLLLESRDEHLHEMALEQITAVTTVNDSNCDQELLSLLLDAKLLVKCVSTPFYPRIVDHLLASLQQGRWDAEELGRHLREAGHEAEAGSLLLAVRGTHQAFRTFSTALRAAQHWV', NULL, 'endoplasmic reticulum membrane, peripheral membrane, cytoplasm', NULL, 'Homo sapiens'), 
('A3KN83', 'Protein strawberry notch homolog 1', NULL, 'SBNO1', 'MVEPGQDLLLAALSESGISPNDLFDIDGGDAGLATPMPTPSVQQSVPLSALELGLETEAAVPVKQEPETVPTPALLNVRQQPPSTTTFVLNQINHLPPLGSTIVMTKTPPVTTNRQTITLTKFIQTTASTRPSVSAPTVRNAMTSAPSKDQVQLKDLLKNNSLNELMKLKPPANIAQPVATAATDVSNGTVKKESSNKEGARMWINDMKMRSFSPTMKVPVVKEDDEPEEEDEEEMGHAETYAEYMPIKLKIGLRHPDAVVETSSLSSVTPPDVWYKTSISEETIDNGWLSALQLEAITYAAQQHETFLPNGDRAGFLIGDGAGVGKGRTIAGIIYENYLLSRKRALWFSVSNDLKYDAERDLRDIGAKNILVHSLNKFKYGKISSKHNGSVKKGVIFATYSSLIGESQSGGKYKTRLKQLLHWCGDDFDGVIVFDECHKAKNLCPVGSSKPTKTGLAVLELQNKLPKARVVYASATGASEPRNMAYMNRLGIWGEGTPFREFSDFIQAVERRGVGAMEIVAMDMKLRGMYIARQLSFTGVTFKIEEVLLSQSYVKMYNKAVKLWVIARERFQQAADLIDAEQRMKKSMWGQFWSAHQRFFKYLCIASKVKRVVQLAREEIKNGKCVVIGLQSTGEARTLEALEEGGGELNDFVSTAKGVLQSLIEKHFPAPDRKKLYSLLGIDLTAPSNNSSPRDSPCKENKIKKRKGEEITREAKKARKVGGLTGSSSDDSGSESDASDNEESDYESSKNMSSGDDDDFNPFLDESNEDDENDPWLIRKDHKKNKEKKKKKSIDPDSIQSALLASGLGSKRPSFSSTPVISPAPNSTPANSNTNSNSSLITSQDAVERAQQMKKDLLDKLEKLAEDLPPNTLDELIDELGGPENVAEMTGRKGRVVSNDDGSISYESRSELDVPVEILNITEKQRFMDGDKNIAIISEAASSGISLQADRRAKNQRRRVHMTLELPWSADRAIQQFGRTHRSNQVTAPEYVFLISELAGEQRFASIVAKRLESLGALTHGDRRATESRDLSRFNFDNKYGRNALEIVMKSIVNLDSPMVSPPPDYPGEFFKDVRQGLIGVGLINVEDRSGILTLDKDYNNIGKFLNRILGMEVHQQNALFQYFADTLTAVVQNAKKNGRYDMGILDLGSGDEKVRKSDVKKFLTPGYSTSGHVELYTISVERGMSWEEATKIWAELTGPDDGFYLSLQIRNNKKTAILVKEVNPKKKLFLVYRPNTGKQLKLEIYADLKKKYKKVVSDDALMHWLDQYNSSADTCTHAYWRGNCKKASLGLVCEIGLRCRTYYVLCGSVLSVWTKVEGVLASVSGTNVKMQIVRLRTEDGQRIVGLIIPANCVSPLVNLLSTSDQSQQLAVQQKQLWQQHHPQSITNLSNA', NULL, NULL, NULL, 'Homo sapiens'), 
('A5YKK6', 'CCR4-NOT transcription complex subunit 1', NULL, 'CNOT1', 'MNLDSLSLALSQISYLVDNLTKKNYRASQQEIQHIVNRHGPEADRHLLRCLFSHVDFSGDGKSSGKDFHQTQFLIQECALLITKPNFISTLSYAIDNPLHYQKSLKPAPHLFAQLSKVLKLSKVQEVIFGLALLNSSSSDLRGFAAQFIKQKLPDLLRSYIDADVSGNQEGGFQDIAIEVLHLLLSHLLFGQKGAFGVGQEQIDAFLKTLRRDFPQERCPVVLAPLLYPEKRDILMDRILPDSGGVAKTMMESSLADFMQEVGYGFCASIEECRNIIVQFGVREVTAAQVARVLGMMARTHSGLTDGIPLQSISAPGSGIWSDGKDKSDGAQAHTWNVEVLIDVLKELNPSLNFKEVTYELDHPGFQIRDSKGLHNVVYGIQRGLGMEVFPVDLIYRPWKHAEGQLSFIQHSLINPEIFCFADYPCHTVATDILKAPPEDDNREIATWKSLDLIESLLRLAEVGQYEQVKQLFSFPIKHCPDMLVLALLQINTSWHTLRHELISTLMPIFLGNHPNSAIILHYAWHGQGQSPSIRQLIMHAMAEWYMRGEQYDQAKLSRILDVAQDLKALSMLLNGTPFAFVIDLAALASRREYLKLDKWLTDKIREHGEPFIQACMTFLKRRCPSILGGLAPEKDQPKSAQLPPETLATMLACLQACAGSVSQELSETILTMVANCSNVMNKARQPPPGVMPKGRPPSASSLDAISPVQIDPLAGMTSLSIGGSAAPHTQSMQGFPPNLGSAFSTPQSPAKAFPPLSTPNQTTAFSGIGGLSSQLPVGGLGTGSLTGIGTGALGLPAVNNDPFVQRKLGTSGLNQPTFQQSKMKPSDLSQVWPEANQHFSKEIDDEANSYFQRIYNHPPHPTMSVDEVLEMLQRFKDSTIKREREVFNCMLRNLFEEYRFFPQYPDKELHITACLFGGIIEKGLVTYMALGLALRYVLEALRKPFGSKMYYFGIAALDRFKNRLKDYPQYCQHLASISHFMQFPHHLQEYIEYGQQSRDPPVKMQGSITTPGSIALAQAQAQAQVPAKAPLAGQVSTMVTTSTTTTVAKTVTVTRPTGVSFKKDVPPSINTTNIDTLLVATDQTERIVEPPENIQEKIAFIFNNLSQSNMTQKVEELKETVKEEFMPWVSQYLVMKRVSIEPNFHSLYSNFLDTLKNPEFNKMVLNETYRNIKVLLTSDKAAANFSDRSLLKNLGHWLGMITLAKNKPILHTDLDVKSLLLEAYVKGQQELLYVVPFVAKVLESSIRSVVFRPPNPWTMAIMNVLAELHQEHDLKLNLKFEIEVLCKNLALDINELKPGNLLKDKDRLKNLDEQLSAPKKDVKQPEELPPITTTTTSTTPATNTTCTATVPPQPQYSYHDINVYSLAGLAPHITLNPTIPLFQAHPQLKQCVRQAIERAVQELVHPVVDRSIKIAMTTCEQIVRKDFALDSEESRMRIAAHHMMRNLTAGMAMITCREPLLMSISTNLKNSFASALRTASPQQREMMDQAAAQLAQDNCELACCFIQKTAVEKAGPEMDKRLATEFELRKHARQEGRRYCDPVVLTYQAERMPEQIRLKVGGVDPKQLAVYEEFARNVPGFLPTNDLSQPTGFLAQPMKQAWATDDVAQIYDKCITELEQHLHAIPPTLAMNPQAQALRSLLEVVVLSRNSRDAIAALGLLQKAVEGLLDATSGADADLLLRYRECHLLVLKALQDGRAYGSPWCNKQITRCLIECRDEYKYNVEAVELLIRNHLVNMQQYDLHLAQSMENGLNYMAVAFAMQLVKILLVDERSVAHVTEADLFHTIETLMRINAHSRGNAPEGLPQLMEVVRSNYEAMIDRAHGGPNFMMHSGISQASEYDDPPGLREKAEYLLREWVNLYHSAAAGRDSTKAFSAFVGQMHQQGILKTDDLITRFFRLCTEMCVEISYRAQAEQQHNPAANPTMIRAKCYHNLDAFVRLIALLVKHSGEATNTVTKINLLNKVLGIVVGVLLQDHDVRQSEFQQLPYHRIFIMLLLELNAPEHVLETINFQTLTAFCNTFHILRPTKAPGFVYAWLELISHRIFIARMLAHTPQQKGWPMYAQLLIDLFKYLAPFLRNVELTKPMQILYKGTLRVLLVLLHDFPEFLCDYHYGFCDVIPPNCIQLRNLILSAFPRNMRLPDPFTPNLKVDMLSEINIAPRILTNFTGVMPPQFKKDLDSYLKTRSPVTFLSDLRSNLQVSNEPGNRYNLQLINALVLYVGTQAIAHIHNKGSTPSMSTITHSAHMDIFQNLAVDLDTEGRYLFLNAIANQLRYPNSHTHYFSCTMLYLFAEANTEAIQEQITRVLLERLIVNRPHPWGLLITFIELIKNPAFKFWNHEFVHCAPEIEKLFQSVAQCCMGQKQAQQVMEGTGAS', '4C0D', 'nucleus, P-body', NULL, 'Homo sapiens'), 
('A6NEC2', 'Puromycin-sensitive aminopeptidase-like protein', NULL, 'NPEPPSL1', 'MWLAAAAPSLARRLLFLGPPPPPLLLLVFSRSSRRRLHSLGLAAMPEKRPFERLPADVSPINCSLCLKPDLLDFTFEGKLEAAAQVRQATNQIVMNCADIDIITASYAPEGDEEIHATGFNYQNEDEKVTLSFPSTLQTGTGTLKIDFVGELNDKMKGFYRSKYTTPSGEVRYAAVTQFEATDARRAFPCWDERAIKATFDISLVVPKDRVALSNMNVIDRKPYPDDENLVEVKFARTPVTSTYLVAFVVGEYDFVETRSKDGVCVCVYTPVGKAEQGKFALEVAAKTLPFYKDYFNVPYPLPKIDLIAIADFAAGAMENWDLVTYRETALLIDPKNSCSSSRQWVALVVGHELAHQWFGNLVTMEWWTHLRLNEGFASWIEYLCVDHCFPEYDIWTQFVSADYTRAQELDALDNSHPIEVSVGHPSEVDEIFDAISYSKGASVIRMLHDYIGDKDFKKGMNMYLTKFQQKNAAAGNL', NULL, 'extracellular, cytoplasm', NULL, 'Homo sapiens'), 
--
-- ------------------- Protein Arabidopsis ----------------------------
--
('P0DI78', 'UPF0496 protein At3g28290', '', 'At3g28290 MZF16.7', 'MVLSKENMLKYSAHLRAYNSACGDHPELKSFDSELQQKTSNLINSFTSDAKTGLVPLPQHAAYKEFTKHLAEVNQQVSDYIIGYGEVVWENSTLRSLVETYFESAKKTLDIAENVTEYVDEAKRGERYIVAAVAQFEKDKENDVGKKTKRYENTLRELKKFEAMGNPFDGDKFTTLFKLMHKEQESLLERVRETKEKLDEELKNIEMEISSRKKWSIISNVLFIGAFVAVAVGSMVLVCTGVGAGVGVAGLLSLPLIAIGWVGVHTILENKIQAREKQEEALKKAHRIANEMDKGMETDKVDMNSISGKVHALKSKITSMLNAVKDATEDGANEVDTKQVMETLTGDVVELTEDIKAVGDDVAKYSKMIEETSYHVLQKITGSGK', 'None', 'membrane', NULL, 'Arabidopsis thaliana'), 
('Q9SMW7', 'Basic transcription factor 3', 'AtBTF3', 'BTF3', 'MNREKLMKMANTVRTGGKGTVRRKKKAVHKTNTTDDKRLQSTLKRIGVNSIPAIEEVNIFKDDVVIQFINPKVQASIAANTWVVSGSPQTKKLQDILPQIISQLGPDNMDNLKKLAEQFQKQASGEGNAASATIQEEDDDDVPELVGETFETAAEEKAPAAAASS', 'None', NULL, NULL, 'Arabidopsis thaliana'), 
('Q9M8S0', 'Protein METHYLENE BLUE SENSITIVITY 1', NULL, 'MBS1', 'MTGKAKPKKHTAKEIQAKIDAALTNRGGGKAGIADRTGKEKGGHAKYECPHCKITAPGLKTMQIHHESKHPNIIYEESKLVNLHAVLAPVAESKPKPGIRGSLKK', 'None', 'nucleus, cytoplasm', NULL, 'Arabidopsis thaliana'), 
('O04318', 'Nitrile-specifier protein 3', 'AtNSP3', 'NSP3', 'MAQKLVAQGGETGDVWDDGVYDNVTKVYVGQGQYGIAFVKFEYANGSEVVVGDEHGEKTELGVEEFEIDSDDYIVYVEGYREKVSDMTSEMITFLSFKTSKGKTSQPIVKKPGVKFVLHGGKIVGFHGRSTDVLHSLGAYVSLPSTPKLLGNWIKVEQNGEGPGLRCSHGIAQVGNKIYSFGGELIPNQPIDKHLYVFDLETRTWSIAPATGDVPHLSCLGVRMVSVGSTLYTFGGRDFSRQYNGFYSFDTTTNEWKLLTPVEEGPTPRSFHSMAADEENVYVFGGVGAMDRIKTLDSYNIVDKTWFHCSNPGDSFSIRGGAGLEVVQGKVWIVYGFNGCEVDDVHFYDPAEDKWTQVETFGVKPNERSVFASAAIGKHIVIFGGEIAMDPRAHVGPGQLIDGTFALDTETLQWERLDKFEGTPSSRGWTASTTGTIDGKKGLVMHGGKAPTNDRFDDLFFYGIDSV', 'None', 'chloroplast', NULL, 'Arabidopsis thaliana'), 
('Q9FG81', 'Aluminum induced protein with YGL and LRDR motifs', NULL, 'MQD19.19', 'MLAVFEKTVANSPEALQSPHSSESAFALKDGSLATHFASVNPNSVTLNFGSSGFVAYSLDNPDPRVPRLFAVVDDIFCLFQGHIENLPFLKQQYGLNKITNEAIIVIEAYRTLRDRGPYPVDKVVRDFHGKFAFILFDSVKKTVFAAADADGSVPFFWGTDAEGHLVFSDNTEMVKKGCAKSYGPFPKGCFFTSSGGLRSFEHPKNELKPVPRVDSSGDVCGATFKVDAETKREGTKMPRVDSSQNWAGHI', 'None', 'cytosol, nucleus', NULL, 'Arabidopsis thaliana'), 
--
-- --------------------------- Protein Regulatory ---------------------------------------------------
--
('P23528', 'Cofilin-1', 'Cofilin', 'CFL1', 'MASGVAVSDGVIKVFNDMKVRKSSTPEEVKKRKKAVLFCLSEDKKNIILEEGKEILVGDVGQTVDDPYATFVKMLPDKDCRYALYDATYETKESKKEDLVFIFWAPESAPLKSKMIYASSKDAIKKKLTGIKHELQANCYEEVKDRCTLAEKLGGSAVISLEGKPL', '4BEX', 'nuclear matrix, nucleus, cytoskeleton, cytoplasm, cell cortex, peripheral membrane protein ruffle membrane, lamellipodium membrane. It should be noted that cellular localization varies throughout development and may be related to phosphorylation levels. Shows diffuse cortical cytoplasm localization', 'Oxidation inhibits its F-actin-binding and depolymerization activity', 'Homo sapiens'), 
('P01009', 'Alpha-1-antitrypsin', 'A1AT', 'SERPINA1', 'EDPQGDAAQKTDTSHHDQDHPTFNKITPNLAEFAFSLYRQLAHQSNSTNIFFSPVSIATAFAMLSLGTKADTHDEILEGLNFNLTEIPEAQIHEGFQELLRTLNQPDSQLQLTTGNGLFLSEGLKLVDKFLEDVKKLYHSEAFTVNFGDTEEAKKQINDYVEKGTQGKIVDLVKELDRDTVFALVNYIFFKGKWERPFEVKDTEEEDFHVDQVTTVKVPMMKRLGMFNIQHCKKLSSWVLLMKYLGNATAIFFLPDEGKLQHLENELTHDIITKFLENEDRRSASLHLPKLSITGTYDLKSVLGQLGITKVFSNGADLSGVTEEAPLKLSKAVHKAVLTIDEKGTEAAGAMFLEAIPMSIPPEVKFNKPFVFLMIEQNTKSPLFMGKVVNPTQK', '3CWM', 'extracellular', 'The processed protein starts at the position 25 from the precursor. Oxidation causes loss of anti-elastase activity', 'Homo sapiens'), 
('A0A0E1M553', 'Hypochlorite-responsive transcription factor', 'HypT', 'hypT', 'MDDCGAVLHNIETKWLYDFLTLEKCRNFSQAAVSRNVSQPAFSRRIRALEQAIGVELFNRQVTPLQLSEQGKIFHSQIRHLLQQLESNLAELRGGSDYAQRKIKIAAAHSLSLGLLPSIISQMPPLFTWAIEAIDVDEAVDKLREGQSDCIFSFHDEDLLEAPFDHIRLFESQLFPVCASDEHGEALFNLAQPHFPLLNYSRNSYMGRLINRTLTRHSELSFSTFFVSSMSELLKQVALDGCGIAWLPEYAIQQEIRSGQLVVLNRDELVIPIQAYAYRMNTRMNPVAERFWRELRELEIVLS', NULL, 'cytoplasm', 'The activation of this transcription factor requires the simultaneous oxidation of M123, M206 and M230', 'Escherichia coli'),
('P00004', 'Cytochrome c', 'Cyt c', 'CYCS', 'GDVEKGKKIFVQKCAQCHTVEKGGKHKTGPNLHGLFGRKTGQAPGFTYTDANKNKGITWKEETLMEYLENPKKYIPGTKMIFAGIKKKTEREDLIAYLKKATNE', '1HRC', 'mitochondrion intermembrane space', 'The oxidation of M80 inhibits electron transport but increases the peroxidase activity. It may also influence the subcellular location of the protein (10.1073/pnas.0809279106)', 'Equus caballus'),
('P11035', 'Nitrate reductase [NADH] 2', 'NR', 'NIA2', 'MAASVDNRQYARLEPGLNGVVRSYKPPVPGRSDSPKAHQNQTTNQTVFLKPAKVHDDDEDVSSEDENETHNSNAVYYKEMIRKSNAELEPSVLDPRDEYTADSWIERNPSMVRLTGKHPFNSEAPLNRLMHHGFITPVPLHYVRNHGHVPKAQWAEWTVEVTGFVKRPMKFTMDQLVSEFAYREFAATLVCAGNRRKEQNMVKKSKGFNWGSAGVSTSVWRGVPLCDVLRRCGIFSRKGGALNVCFEGSEDLPGGAGTAGSKYGTSIKKEYAMDPSRDIILAYMQNGEYLTPDHGFPVRIIIPGFIGGRMVKWLKRIIVTTKESDNFYHFKDNRVLPSLVDAELADEEGWWYKPEYIINELNINSVITTPCHEEILPINAFTTQRPYTLKGYAYSGGGKKVTRVEVTVDGGETWNVCALDHQEKPNKYGKFWCWCFWSLEVEVLDLLSAKEIAVRAWDETLNTQPEKMIWNLMGMMNNCWFRVKTNVCKPHKGEIGIVFEHPTLPGNESGGWMAKERHLEKSADAPPSLKKSVSTPFMNTTAKMYSMSEVKKHNSADSCWIIVHGHIYDCTRFLMDHPGGSDSILINAGTDCTEEFEAIHSDKAKKMLEDYRIGELITTGYSSDSSSPNNSVHGSSAVFSLLAPIGEATPVRNLALVNPRAKVPVQLVEKTSISHDVRKFRFALPVEDMVLGLPVGKHIFLCATINDKLCLRAYTPSSTVDVVGYFELVVKIYFGGVHPRFPNGGLMSQYLDSLPIGSTLEIKGPLGHVEYLGKGSFTVHGKPKFADKLAMLAGGTGITPVYQIIQAILKDPEDETEMYVIYANRTEEDILLREELDGWAEQYPDRLKVWYVVESAKEGWAYSTGFISEAIMREHIPDGLDGSALAMACGPPPMIQFAVQPNLEKMQYNIKEDFLIF', NULL, 'cytosol, plasma membrane, vacuole', 'The oxidation inhibits the phosphorylation of the regulatory S534, which would avoid the inactivation of NR', 'Arabidopsis thaliana');




--
-- Estructura de tabla para la tabla `Phosphorylation` ---------------------------
--
CREATE TABLE `Phosphorylation`(
  `p_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `p_res` varchar(20) DEFAULT NULL -- position residue being phosphorylated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Phosphorylation`
--
INSERT INTO `Phosphorylation` (`p_id`, `prot_id`, `p_res`) 
VALUES 

--
-- -------------- Phosphorylation Jurkat -----------
--
(1, 'A0AVT1', 'S16-p'), 
(2, 'A0AVT1', 'S36-p'), 
(3, 'A0AVT1', 'Y44-p'), 
(4, 'A0AVT1', 'T54-p'), 
(5, 'A0AVT1', 'T146-p'), 
(6, 'A0AVT1', 'S149-p'), 
(7, 'A0AVT1', 'T241-p'), 
(8, 'A0AVT1', 'T246-p'), 
(9, 'A0AVT1', 'S301-p'), 
(10, 'A0AVT1', 'T637-p'), 
(11, 'A0AVT1', 'S737-p'), 
(12, 'A0AVT1', 'S743-p'), 
(13, 'A0AVT1', 'S951-p'), 
(14, 'A0AVT1', 'Y979-p'), 
(15, 'A0AVT1', 'Y994-p'), 
(16, 'A0AVT1', 'T1051-p'), 
(17, 'A2RRP1', 'S11-p'), 
(18, 'A2RRP1', 'T294-p'), 
(19, 'A2RRP1', 'S307-p'), 
(20, 'A2RRP1', 'S473-p'), 
(21, 'A2RRP1', 'S475-p'), 
(22, 'A2RRP1', 'Y477-p'), 
(23, 'A2RRP1', 'S480-p'), 
(24, 'A2RRP1', 'Y485-p'), 
(25, 'A2RRP1', 'Y488-p'), 
(26, 'A2RRP1', 'Y494-p'), 
(27, 'A2RRP1', 'T497-p'), 
(28, 'A2RRP1', 'T511-p'), 
(29, 'A2RRP1', 'T525-p'), 
(30, 'A2RRP1', 'Y539-p'), 
(31, 'A2RRP1', 'Y549-p'), 
(32, 'A2RRP1', 'Y557-p'), 
(33, 'A2RRP1', 'S564-p'), 
(34, 'A2RRP1', 'Y640-p'), 
(35, 'A2RRP1', 'T1027-p'), 
(36, 'A2RRP1', 'T1030-p'), 
(37, 'A2RRP1', 'S1145-p'), 
(38, 'A2RRP1', 'S1243-p'), 
(39, 'A2RRP1', 'S1259-p'), 
(40, 'A2RRP1', 'T1260-p'), 
(41, 'A2RRP1', 'S1382-p'), 
(42, 'A2RRP1', 'S1388-p'), 
(43, 'A2RRP1', 'S1441-p'), 
(44, 'A2RRP1', 'S1842-p'), 
(45, 'A2RRP1', 'S1869-p'), 
(46, 'A2RRP1', 'S1870-p'), 
(47, 'A2RRP1', 'Y1877-p'), 
(48, 'A2RRP1', 'Y1883-p'), 
(49, 'A2RRP1', 'T1894-p'), 
(50, 'A2RRP1', 'S1902-p'), 
(51, 'A2RRP1', 'S2052-p'), 
(52, 'A3KN83', 'S148-p'), 
(53, 'A3KN83', 'S162-p'), 
(54, 'A3KN83', 'S212-p'), 
(55, 'A3KN83', 'S214-p'), 
(56, 'A3KN83', 'S588-p'), 
(57, 'A3KN83', 'S595-p'), 
(58, 'A3KN83', 'T686-p'), 
(59, 'A3KN83', 'S689-p'), 
(60, 'A3KN83', 'S692-p'), 
(61, 'A3KN83', 'S693-p'), 
(62, 'A3KN83', 'S697-p'), 
(63, 'A3KN83', 'T713-p'), 
(64, 'A3KN83', 'S754-p'), 
(65, 'A3KN83', 'S755-p'), 
(66, 'A3KN83', 'S768-p'), 
(67, 'A3KN83', 'S794-p'), 
(68, 'A3KN83', 'S799-p'), 
(69, 'A3KN83', 'S802-p'), 
(70, 'A3KN83', 'S807-p'), 
(71, 'A3KN83', 'S811-p'), 
(72, 'A3KN83', 'S815-p'), 
(73, 'A3KN83', 'S817-p'), 
(74, 'A3KN83', 'S818-p'), 
(75, 'A3KN83', 'T819-p'), 
(76, 'A3KN83', 'S823-p'), 
(77, 'A3KN83', 'S828-p'), 
(78, 'A3KN83', 'T829-p'), 
(79, 'A3KN83', 'S904-p'), 
(80, 'A3KN83', 'S939-p'), 
(81, 'A3KN83', 'S1015-p'), 
(82, 'A3KN83', 'S1062-p'), 
(83, 'A3KN83', 'T1166-p'), 
(84, 'A3KN83', 'S1313-p'), 
(85, 'A3KN83', 'S1324-p'), 
(86, 'A3KN83', 'S1326-p'), 
(87, 'A3KN83', 'T1328-p'), 
(88, 'A3KN83', 'S1386-p'), 
(89, 'A5YKK6', 'S7-p'), 
(90, 'A5YKK6', 'T21-p'), 
(91, 'A5YKK6', 'S318-p'), 
(92, 'A5YKK6', 'S322-p'), 
(93, 'A5YKK6', 'S626-p'), 
(94, 'A5YKK6', 'S725-p'), 
(95, 'A5YKK6', 'T730-p'), 
(96, 'A5YKK6', 'Y851-p'), 
(97, 'A5YKK6', 'T927-p'), 
(98, 'A5YKK6', 'Y928-p'), 
(99, 'A5YKK6', 'Y937-p'), 
(100, 'A5YKK6', 'S948-p'), 
(101, 'A5YKK6', 'T1051-p'), 
(102, 'A5YKK6', 'T1053-p'), 
(103, 'A5YKK6', 'T1058-p'), 
(104, 'A5YKK6', 'S1061-p'), 
(105, 'A5YKK6', 'T1073-p'), 
(106, 'A5YKK6', 'Y1234-p'), 
(107, 'A5YKK6', 'T1418-p'), 
(108, 'A5YKK6', 'T1419-p'), 
(109, 'A5YKK6', 'S1432-p'), 
(110, 'A5YKK6', 'T1525-p'), 
(111, 'A5YKK6', 'S1649-p'), 
(112, 'A5YKK6', 'S1703-p'), 
(113, 'A5YKK6', 'Y1817-p'), 
(114, 'A5YKK6', 'Y1842-p'), 
(115, 'A5YKK6', 'Y2074-p'), 
(116, 'A5YKK6', 'S2188-p'), 
(117, 'A5YKK6', 'S2194-p'), 
(118, 'A5YKK6', 'Y2264-p'), 
(119, 'A6NEC2', 'S30-p'), 
(120, 'A6NEC2', 'S39-p'), 
(121, 'A6NEC2', 'Y164-p'), 
(122, 'A6NEC2', 'T165-p'), 
(123, 'A6NEC2', 'T166-p'), 
(124, 'A6NEC2', 'S168-p'), 
(125, 'A6NEC2', 'Y173-p'), 
(126, 'A6NEC2', 'Y295-p'), 
(127, 'A6NEC2', 'T329-p'), 
(128, 'A6NEC2', 'Y464-p'), 
(129, 'A6NEC2', 'T466-p'), 
--
-- -------------- Phosphorylation Arabidopsis -----------
--
(130, 'P0DI78', 'Any'),  
(131, 'Q9SMW7', 'Any'),  
(132, 'Q9M8S0', 'Any'),  
(133, 'O04318', 'Any'),  
(134, 'Q9FG81', 'Any'),  
--
-- -------------- Phosphorylation Regulatory -----------
--
(135, 'P23528', 'S3-p'),  
(136, 'P23528', 'S8-p'),  
(137, 'P23528', 'S23-p'),  
(138, 'P23528', 'S24-p'),  
(139, 'P23528', 'T25-p'),  
(140, 'P23528', 'S41-p'),  
(141, 'P23528', 'T63-p'),  
(142, 'P23528', 'Y68-p'),  
(143, 'P23528', 'T70-p'),  
(144, 'P23528', 'Y82-p'),  
(145, 'P23528', 'Y85-p'),  
(146, 'P23528', 'T88-p'),  
(147, 'P23528', 'Y89-p'),  
(148, 'P23528', 'T91-p'),  
(149, 'P23528', 'S94-p'),  
(150, 'P23528', 'S108-p'),  
(151, 'P23528', 'Y117-p'),  
(152, 'P23528', 'S119-p'),  
(153, 'P23528', 'T129-p'),  
(154, 'P23528', 'Y140-p'),  
(155, 'P23528', 'S156-p'),  
(156, 'P23528', 'S160-p'),  
(157, 'P01009', 'T13-p'),  
(158, 'P01009', 'S14-p'),  
(159, 'P01009', 'Y160-p'),  
(160, 'P01009', 'S237-p'),  
(161, 'P01009', 'S283-p'),  
(162, 'P01009', 'S285-p'),  
(163, 'P01009', 'S292-p'),  
(164, 'P01009', 'T294-p'),  
(165, 'P01009', 'Y297-p'),  
(166, 'P01009', 'S301-p'),  
(167, 'P01009', 'T309-p'),  
(168, 'P01009', 'S319-p'),  
(169, 'P01009', 'S359-p'),  
(170, 'P01009', 'T392-p'),   
(171, 'A0A0E1M553', 'Any'), 

(172, 'P00004', 'Any'),
(173, 'P11035', 'Any');

--
-- Estructura de tabla para la tabla `Acetylation` ---------------------------
--

CREATE TABLE `Acetylation`(
  `ac_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `ac_res` varchar(20) DEFAULT NULL -- position residue being acetylated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Acetylation`
--
INSERT INTO `Acetylation` (`ac_id`, `prot_id`, `ac_res`) 
VALUES 

--
-- ------- Acetylation Jurkat ------------
-- 
(1, 'A0AVT1', 'K544-ac'), 
(2, 'A0AVT1', 'K1003-ac'), 
(3, 'A2RRP1', 'K1057-ac'), 
(4, 'A2RRP1', 'K1261-ac'), 
(5, 'A2RRP1', 'K1931-ac'), 
(6, 'A3KN83', 'K149-ac'), 
(7, 'A3KN83', 'K356-ac'), 
(8, 'A3KN83', 'K369-ac'), 
(9, 'A3KN83', 'K378-ac'), 
(10, 'A3KN83', 'K393-ac'), 
(11, 'A3KN83', 'K413-ac'), 
(12, 'A3KN83', 'K1222-ac'), 
(13, 'A5YKK6', 'K877-ac'), 
(14, 'A5YKK6', 'K882-ac'), 
(15, 'A5YKK6', 'K1063-ac'), 
(16, 'A5YKK6', 'K1514-ac'), 
(17, 'A6NEC2', 'K48-ac'), 
--
-- -------- Acetylation Arabidopsis
--
(18, 'P0DI78', 'Any'),  
(19, 'Q9SMW7', 'Any'),  
(20, 'Q9M8S0', 'Any'),  
(21, 'O04318', 'Any'),  
(22, 'Q9FG81', 'Any'), 
--
-- ---------------- Acetylation Regulatory --------------------------
--
(23, 'P23528', 'K13-ac'),  
(24, 'P23528', 'K19-ac'),  
(25, 'P23528', 'K30-ac'),  
(26, 'P23528', 'K31-ac'),  
(27, 'P23528', 'K33-ac'),  
(28, 'P23528', 'K44-ac'),  
(29, 'P23528', 'K45-ac'),  
(30, 'P23528', 'K73-ac'),  
(31, 'P23528', 'K78-ac'),  
(32, 'P23528', 'K92-ac'),  
(33, 'P23528', 'K96-ac'),  
(34, 'P23528', 'K114-ac'),  
(35, 'P23528', 'K121-ac'),  
(36, 'P23528', 'K125-ac'),  
(37, 'P23528', 'K126-ac'),  
(38, 'P23528', 'K127-ac'),  
(39, 'P23528', 'K132-ac'),  
(40, 'P23528', 'K144-ac'),  
(41, 'P01009', 'K135-ac'),  
(42, 'P01009', 'K136-ac'),  
(43, 'P01009', 'K174-ac'),  
(44, 'P01009', 'K233-ac'),  
(45, 'P01009', 'K290-ac'),   
(46, 'A0A0E1M553', 'Any'),
 
(47, 'P00004', 'K8-ac'),  
(48, 'P00004', 'K27-ac'),  
(49, 'P00004', 'K72-ac'),  
(50, 'P00004', 'K86-ac'),  
(51, 'P00004', 'K87-ac'),
(52, 'P11035', 'Any');


--
-- Estructura de tabla para la tabla `Methylation` ---------------------------
--
CREATE TABLE `Methylation`(
  `m_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `m_res` varchar(20) DEFAULT NULL -- position residue being methylated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Methylation`
--
INSERT INTO `Methylation` (`m_id`, `prot_id`, `m_res`) 
VALUES 

--
-- -------- Methylation Jurkat ---------------
--
(1, 'A0AVT1', 'Any'), 
(2, 'A2RRP1', 'Any'), 
(3, 'A3KN83', 'Any'), 
(4, 'A5YKK6', 'R45-m1'), 
(5, 'A5YKK6', 'R238-m1'), 
(6, 'A5YKK6', 'R1189-m1'), 
(7, 'A6NEC2', 'Any'), 
--
-- -------- Methylation Arabidopsi9s ---------------
--
(8, 'P0DI78', 'Any'),  
(9, 'Q9SMW7', 'Any'),  
(10, 'Q9M8S0', 'Any'),  
(11, 'O04318', 'Any'),  
(12, 'Q9FG81', 'Any'), 
 --
-- ----- Methylation Regulatory ---------
--
(13, 'P23528', 'K19-m'),  
(14, 'P23528', 'K96-m'),  
(15, 'P01009', 'Any'),   
(16, 'A0A0E1M553', 'Any'),

(17, 'P00004', 'Any'),
(18, 'P11035', 'Any'); 

--
-- Estructura de tabla para la tabla `Ubiquitination` ---------------------------
--
CREATE TABLE `Ubiquitination`(
  `u_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `u_res` varchar(20) DEFAULT NULL -- position residue being ubiquitinated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Ubiquitination`
--
INSERT INTO `Ubiquitination` (`u_id`, `prot_id`, `u_res`) 
VALUES 

--
-- ------ Ubiquitination Jurkat ------------
--

(1, 'A0AVT1', 'K58-ub'), 
(2, 'A0AVT1', 'K169-ub'), 
(3, 'A0AVT1', 'K182-ub'), 
(4, 'A0AVT1', 'K238-ub'), 
(5, 'A0AVT1', 'K295-ub'), 
(6, 'A0AVT1', 'K490-ub'), 
(7, 'A0AVT1', 'K503-ub'), 
(8, 'A0AVT1', 'K521-ub'), 
(9, 'A0AVT1', 'K544-ub'), 
(10, 'A0AVT1', 'K628-ub'), 
(11, 'A0AVT1', 'K644-ub'), 
(12, 'A0AVT1', 'K652-ub'), 
(13, 'A0AVT1', 'K687-ub'), 
(14, 'A0AVT1', 'K709-ub'), 
(15, 'A0AVT1', 'K729-ub'), 
(16, 'A0AVT1', 'K739-ub'), 
(17, 'A0AVT1', 'K746-ub'), 
(18, 'A0AVT1', 'K800-ub'), 
(19, 'A0AVT1', 'K810-ub'), 
(20, 'A0AVT1', 'K830-ub'), 
(21, 'A0AVT1', 'K871-ub'), 
(22, 'A0AVT1', 'K882-ub'), 
(23, 'A0AVT1', 'K889-ub'), 
(24, 'A0AVT1', 'K1003-ub'), 
(25, 'A2RRP1', 'K115-ub'), 
(26, 'A2RRP1', 'K265-ub'), 
(27, 'A2RRP1', 'K284-ub'), 
(28, 'A2RRP1', 'K416-ub'), 
(29, 'A2RRP1', 'K490-ub'), 
(30, 'A2RRP1', 'K563-ub'), 
(31, 'A2RRP1', 'K600-ub'), 
(32, 'A2RRP1', 'K620-ub'), 
(33, 'A2RRP1', 'K673-ub'), 
(34, 'A2RRP1', 'K679-ub'), 
(35, 'A2RRP1', 'K687-ub'), 
(36, 'A2RRP1', 'K799-ub'), 
(37, 'A2RRP1', 'K841-ub'), 
(38, 'A2RRP1', 'K944-ub'), 
(39, 'A2RRP1', 'K955-ub'), 
(40, 'A2RRP1', 'K977-ub'), 
(41, 'A2RRP1', 'K1057-ub'), 
(42, 'A2RRP1', 'K1063-ub'), 
(43, 'A2RRP1', 'K1086-ub'), 
(44, 'A2RRP1', 'K1169-ub'), 
(45, 'A2RRP1', 'K1246-ub'), 
(46, 'A2RRP1', 'K1261-ub'), 
(47, 'A2RRP1', 'K1314-ub'), 
(48, 'A2RRP1', 'K1440-ub'), 
(49, 'A2RRP1', 'K1452-ub'), 
(50, 'A2RRP1', 'K1667-ub'), 
(51, 'A2RRP1', 'K1908-ub'), 
(52, 'A2RRP1', 'K1986-ub'), 
(53, 'A2RRP1', 'K2213-ub'), 
(54, 'A3KN83', 'K149-ub'), 
(55, 'A3KN83', 'K155-ub'), 
(56, 'A3KN83', 'K327-ub'), 
(57, 'A3KN83', 'K369-ub'), 
(58, 'A3KN83', 'K378-ub'), 
(59, 'A3KN83', 'K925-ub'), 
(60, 'A3KN83', 'K1011-ub'), 
(61, 'A3KN83', 'K1222-ub'), 
(62, 'A5YKK6', 'K231-ub'), 
(63, 'A5YKK6', 'K355-ub'), 
(64, 'A5YKK6', 'K372-ub'), 
(65, 'A5YKK6', 'K449-ub'), 
(66, 'A5YKK6', 'K556-ub'), 
(67, 'A5YKK6', 'K808-ub'), 
(68, 'A5YKK6', 'K1163-ub'), 
(69, 'A5YKK6', 'K1321-ub'), 
(70, 'A5YKK6', 'K1426-ub'), 
(71, 'A5YKK6', 'K1514-ub'), 
(72, 'A5YKK6', 'K1560-ub'), 
(73, 'A5YKK6', 'K1567-ub'), 
(74, 'A5YKK6', 'K1600-ub'), 
(75, 'A5YKK6', 'K1708-ub'), 
(76, 'A5YKK6', 'K1851-ub'), 
(77, 'A5YKK6', 'K2086-ub'), 
(78, 'A5YKK6', 'K2151-ub'), 
(79, 'A5YKK6', 'K2178-ub'), 
(80, 'A5YKK6', 'K2185-ub'), 
(81, 'A5YKK6', 'K2364-ub'), 
(82, 'A6NEC2', 'K155-ub'), 
(83, 'A6NEC2', 'K157-ub'), 
(84, 'A6NEC2', 'K208-ub'), 
(85, 'A6NEC2', 'K222-ub'), 
(86, 'A6NEC2', 'K234-ub'), 
(87, 'A6NEC2', 'K261-ub'), 
(88, 'A6NEC2', 'K274-ub'), 
(89, 'A6NEC2', 'K279-ub'), 
(90, 'A6NEC2', 'K287-ub'), 
(91, 'A6NEC2', 'K293-ub'), 
(92, 'A6NEC2', 'K440-ub'), 
(93, 'A6NEC2', 'K455-ub'), 
(94, 'A6NEC2', 'K459-ub'), 
(95, 'A6NEC2', 'K467-ub'),   
-- 
--  ------- Ubiquitination Arabidopsis
--
(96, 'P0DI78', 'Any'),  
(97, 'Q9SMW7', 'Any'),  
(98, 'Q9M8S0', 'Any'),  
(99, 'O04318', 'Any'),  
(100, 'Q9FG81', 'Any'), 
--
-- ----- Ubiquitination Regulatory -------
--
(101, 'P23528', 'K13-ub'),  
(102, 'P23528', 'K19-ub'),  
(103, 'P23528', 'K22-ub'),  
(104, 'P23528', 'K30-ub'),  
(105, 'P23528', 'K34-ub'),  
(106, 'P23528', 'K44-ub'),  
(107, 'P23528', 'K45-ub'),  
(108, 'P23528', 'K53-ub'),  
(109, 'P23528', 'K73-ub'),  
(110, 'P23528', 'K78-ub'),  
(111, 'P23528', 'K92-ub'),  
(112, 'P23528', 'K95-ub'),  
(113, 'P23528', 'K96-ub'),  
(114, 'P23528', 'K112-ub'),  
(115, 'P23528', 'K114-ub'),  
(116, 'P23528', 'K121-ub'),  
(117, 'P23528', 'K132-ub'),  
(118, 'P23528', 'K144-ub'),  
(119, 'P23528', 'K152-ub'),  
(120, 'P23528', 'K164-ub'),  
(121, 'P01009', 'Any'),    
(122, 'A0A0E1M553', 'Any'),

(123, 'P00004', 'Any'),
(124, 'P11035', 'Any');


--
-- Estructura de tabla para la tabla `Sumoylation` ---------------------------
--
CREATE TABLE `Sumoylation`(
  `s_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `s_res` varchar(20) DEFAULT NULL -- position residue being sumoylated 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `Sumoylation`
-- 
INSERT INTO `Sumoylation` (`s_id`, `prot_id`, `s_res`) 
VALUES 

-- 
-- ------ Sumoylation Jurkat ----------
--
(1, 'A0AVT1', 'Any'), 
(2, 'A2RRP1', 'Any'), 
(3, 'A3KN83', 'Any'), 
(4, 'A5YKK6', 'Any'), 
(5, 'A6NEC2', 'Any'), 
--
-- ------- Sumoylation Arabidopsis ----------
--
(6, 'P0DI78', 'Any'),  
(7, 'Q9SMW7', 'Any'),  
(8, 'Q9M8S0', 'Any'),  
(9, 'O04318', 'Any'),  
(10, 'Q9FG81', 'Any'), 
--
-- ----- Sumoylation Regulatory -----
--

(11, 'P23528', 'K13-sm'),  
(12, 'P23528', 'K19-sm'),  
(13, 'P23528', 'K30-sm'),  
(14, 'P23528', 'K34-sm'),  
(15, 'P23528', 'K44-sm'),  
(16, 'P23528', 'K45-sm'),  
(17, 'P23528', 'K53-sm'),  
(18, 'P23528', 'K112-sm'),  
(19, 'P23528', 'K114-sm'),  
(20, 'P23528', 'K121-sm'),  
(21, 'P23528', 'K132-sm'),  
(22, 'P23528', 'K164-sm'),  
(23, 'P01009', 'Any'),    
(24, 'A0A0E1M553', 'Any'),

(25, 'P00004', 'Any'),
(26, 'P11035', 'Any');

--
-- Estructura de tabla para la tabla `OGlcNAc` ---------------------------
--
CREATE TABLE `OGlcNAc`(
  `g_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `g_res` varchar(20) DEFAULT NULL -- position residue being glycosilated
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `OGlcNAc`
-- 
INSERT INTO `OGlcNAc` (`g_id`, `prot_id`, `g_res`) 
VALUES 

--
-- ------- OGlcNAc Jurkat ----------
--
(1, 'A0AVT1', 'Any'), 
(2, 'A2RRP1', 'Any'), 
(3, 'A3KN83', 'T112-gl'), 
(4, 'A5YKK6', 'Any'), 
(5, 'A6NEC2', 'Any'),
--
-- ------- OGlcNAc Arabidopsis -----
--
(6, 'P0DI78', 'Any'),  
(7, 'Q9SMW7', 'Any'),  
(8, 'Q9M8S0', 'Any'),  
(9, 'O04318', 'Any'),  
(10, 'Q9FG81', 'Any'), 
-- 
-- ------- OGlcNAc Regulatory ------
--
(11, 'P23528', 'Any'),  
(12, 'P01009', 'Any'),   
(13, 'A0A0E1M553', 'Any'), 

(16, 'P00004', 'Any'),
(17, 'P11035', 'Any');


--
-- Estructura de tabla para la tabla `RegPTM` ---------------------------
--
CREATE TABLE `RegPTM`(
  `r_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `r_res` varchar(20) DEFAULT NULL -- position residue being targeted
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `RegPTM`
-- 
INSERT INTO `RegPTM` (`r_id`, `prot_id`, `r_res`) 
VALUES 

--
-- ---- RegPTM Jurkat -----------
--
(1, 'A0AVT1', 'Any'), 
(2, 'A2RRP1', 'Any'), 
(3, 'A3KN83', 'Any'), 
(4, 'A5YKK6', 'Any'), 
(5, 'A6NEC2', 'Any'), 
--
-- ---- RegPTM Arabidopsis -----------
--
(6, 'P0DI78', 'Any'),  
(7, 'Q9SMW7', 'Any'),  
(8, 'Q9M8S0', 'Any'),  
(9, 'O04318', 'Any'),  
(10, 'Q9FG81', 'Any'),
--
-- ----- RegPTM Regulatory --------
--
(11, 'P23528', 'S3-p'),  
(12, 'P23528', 'S23-p'),  
(13, 'P23528', 'Y68-p'),  
(14, 'P23528', 'S24-p'),  
(15, 'P01009', 'T392-p'),   
(16, 'A0A0E1M553', 'Any'),

(17, 'P00004', 'Any'),
(18, 'P11035', 'Any');


--
-- Estructura de tabla para la tabla `Disease` ---------------------------
--
CREATE TABLE `Disease`(
  `di_id`int(11) PRIMARY KEY,
  `prot_id` varchar(20) NOT NULL,
  `di_res` varchar(20) DEFAULT NULL -- position residue being targeted
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de datos para la tabla `Disease`
-- 
INSERT INTO `Disease` (`di_id`, `prot_id`, `di_res`) 
VALUES 

--
-- ------ Disease Jurkat --------
--
(1, 'A0AVT1', 'Any'), 
(2, 'A2RRP1', 'Any'), 
(3, 'A3KN83', 'Any'), 
(4, 'A5YKK6', 'Any'), 
(5, 'A6NEC2', 'Any'), 
--
-- ----- Disease Arabidopsis ---------
--
(6, 'P0DI78', 'Any'),  
(7, 'Q9SMW7', 'Any'),  
(8, 'Q9M8S0', 'Any'),  
(9, 'O04318', 'Any'),  
(10, 'Q9FG81', 'Any'), 
--
-- ------ Disease Regulatory --------
--
(11, 'P23528', 'Any'),  
(12, 'P01009', 'Any'),   
(13, 'A0A0E1M553', 'Any'),

(14, 'P00004', 'Any'),
(15, 'P11035', 'Any');

--
-- Estructura de tabla para la tabla `Regulatory`--------------------------------
--
CREATE TABLE `Regulatory`( 
  `reg_id` int(11) NOT NULL, 
  `unknown` tinyint(1) NOT NULL,
  `act_gain` tinyint(1) NOT NULL, 
  `act_loss` tinyint(1) NOT NULL, 
  `ppi_gain` tinyint(1) NOT NULL, 
  `ppi_loss` tinyint(1) NOT NULL,
  `stability`tinyint(1) NOT NULL,
  `relocation`tinyint(1) NOT NULL,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Regulatory`
--
INSERT INTO `Regulatory` (`reg_id`, `unknown`, 
  `act_gain`, `act_loss`, `ppi_gain`, `ppi_loss`, `stability`, `relocation`) 
VALUES 
(1,1,0,0,0,0,0,0),
(2,0,0,0,0,0,0,0),

(3,0,  1,0,0,0,0,0),
(4,0,  0,1,0,0,0,0),
(5,0,  0,0,1,0,0,0),
(6,0,  0,0,0,1,0,0),
(7,0,  0,0,0,0,1,0),
(8,0,  0,0,0,0,0,1),

(9,0,  0,0,0,1,0,1),
(10,0, 0,0,0,1,1,0),
(11,0, 0,0,1,0,0,1),
(12,0, 0,0,1,0,1,0),
(13,0, 0,1,0,0,0,1),
(14,0, 0,1,0,0,1,0),
(15,0, 1,0,0,0,0,1),
(16,0, 1,0,0,0,1,0),
(17,0, 0,0,1,1,0,0),
(18,0, 0,1,0,1,0,0),
(19,0, 0,1,1,0,0,0),
(20,0, 1,0,0,1,0,0),
(21,0, 1,0,1,0,0,0),
(22,0, 1,1,0,0,0,0),
(23,0, 0,0,0,0,1,1),

(24,0, 0,0,0,1,1,1),
(25,0, 0,0,1,0,1,1),
(26,0, 0,1,0,0,1,1),
(27,0, 1,0,0,0,1,1),
(28,0, 0,0,1,1,0,1),
(29,0, 0,0,1,1,1,0),
(30,0, 1,1,1,0,0,0),
(31,0, 0,1,0,1,0,1),
(32,0, 0,1,0,1,1,0),
(33,0, 0,1,1,0,0,1),
(34,0, 0,1,1,0,1,0),
(35,0, 1,0,0,1,0,1),
(36,0, 1,0,0,1,1,0),
(37,0, 1,0,1,0,0,1),
(38,0, 1,0,1,0,1,0),
(39,0, 1,1,0,0,0,1),
(40,0, 1,1,0,0,1,0),
(41,0, 0,1,1,1,0,0),
(42,0, 1,0,1,1,0,0),
(43,0, 1,1,0,1,0,0),


(44,0, 0,0,1,1,1,1),
(45,0, 0,1,0,1,1,1),
(46,0, 0,1,1,0,1,1),
(47,0, 1,0,0,1,1,1),
(48,0, 1,0,1,0,1,1),
(49,0, 1,1,0,0,1,1),
(50,0, 0,1,1,1,0,1),
(51,0, 0,1,1,1,1,0),
(52,0, 1,0,1,1,0,1),
(53,0, 1,0,1,1,1,0),
(54,0, 1,1,0,1,0,1),
(55,0, 1,1,0,1,1,0),
(56,0, 1,1,1,1,0,0),
(57,0, 1,1,1,0,0,1),
(58,0, 1,1,1,0,1,0),

(59,0, 0,1,1,1,1,1),
(60,0, 1,0,1,1,1,1),
(61,0, 1,1,0,1,1,1),
(62,0, 1,1,1,0,1,1),
(63,0, 1,1,1,1,0,1),
(64,0, 1,1,1,1,1,0),

(65,0, 1,1,1,1,1,1);


--
-- Estructura de tabla para la tabla `Organism` ---------------------------
--
CREATE TABLE `Organism`(
  `org_id`int(11) NOT NULL,
  `org_sp`varchar(50) NOT NULL,
  `org_tissue` varchar(100) DEFAULT NULL,
  `org_oxidant`varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Organism`
--
INSERT INTO `Organism` (`org_id`, `org_sp`, `org_tissue`, `org_oxidant`)
VALUES 

(1, 'Homo sapiens', NULL, 'taurine chloramine (TnCl)'), 
(2, 'Homo sapiens', 'extracellular', 'hydrogen peroxide (H2O2)'), 
(4, 'Escherichia coli', NULL, 'hypochlorite (HClO)'), 
(9, 'Homo sapiens', 'Jurkat cells', 'hydrogen peroxide (H2O2)'), 
(11, 'Arabidopsis thaliana', 'leaf', 'hydrogen peroxide (H2O2)'), 
(12, 'Equus caballus', NULL, 'hypochlorite (HClO)'), 
(38, 'Arabidopsis thaliana', 'leaf', 'high light irradiation'); 

--
-- Estructura de tabla para la tabla `Taxon` ---------------------------
--
CREATE TABLE `Taxon`(
  `org_sp`varchar(50) NOT NULL,
  `domain`varchar(50) NOT NULL,
  `kingdom` varchar(50) DEFAULT NULL,
  `phylum` varchar(50) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` varchar(50) DEFAULT NULL,
  `family` varchar(50) DEFAULT NULL,
  `genus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`org_sp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Taxon`
--
INSERT INTO `Taxon` (`org_sp`, `domain`, `kingdom`, `phylum`, `class`, `order`, `family`, `genus`)
VALUES 

('Arabidopsis thaliana', 'Eukaryota', 'Plantae', NULL, 'Magnoliopsida', 'Capparales', 'Brassicaceae', 'Arabidopsis'), 
('Escherichia coli', 'Bacteria', 'Monera', 'Proteobacteria', 'Gammaproteobacteria', 'Enterobacteriales', 'Enterobacteriaceae', 'Escherichia'),
('Equus caballus', 'Eukaryota', 'Animalia', 'Chordata', 'Mammalia', 'Perissodactyla', 'Equidae', 'Equus'), 
('Homo sapiens', 'Eukaryota', 'Animalia', 'Chordata', 'Mammalia', 'Primates', 'Hominidae', 'Homo');

--
-- Estructura de tabla para la tabla `DOI` ---------------------------
--
CREATE TABLE `DOI`(
  `doi_id`int(11) PRIMARY KEY,
  `ref` varchar(300) NOT NULL,
  `pmid`varchar(300) NOT NULL,
  `character` int(2) DEFAULT NULL, -- 1 for key references and 0 for the remaining
  `title`  text DEFAULT NULL,
  `href` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `DOI`
--
INSERT INTO `DOI` (`doi_id`, `ref`, `pmid`, `character`, `title`, `href`)
VALUES



/* ----- For Jurkat cells, Ghesquiere 2011 ------- */
(1, '10.1074/mcp.M110.006866', '21406390', 1, 'Redox proteomics of protein-bound methionine oxidation.', 'https://www.ncbi.nlm.nih.gov/pubmed/21406390'), 


/* ------- For Arabidopsis ------------------------*/
(2, '10.1074/mcp.M114.043729', '25693801', 1, 'Protein methionine sulfoxide dynamics in Arabidopsis thaliana under oxidative stress.', 'https://www.ncbi.nlm.nih.gov/pubmed/25693801'), 
/* ------------- For Bacillus cereus --------------*/
(7, '10.1016/j.freeradbiomed.2014.07.018', '25058340', 1, 'Taurine chloramine-induced inactivation of cofilin protein through methionine oxidation.', 'https://www.ncbi.nlm.nih.gov/pubmed/25058340'), 
(8, '10.1074/jbc.M004850200', '10867014', 1, 'Oxidation of either methionine 351 or methionine 358 in alpha 1-antitrypsin causes loss of anti-neutrophil elastase activity.', 'https://www.ncbi.nlm.nih.gov/pubmed/10867014'), 
(10, '10.1073/pnas.1300578110', '23690622', 1, 'Methionine oxidation activates a transcription factor in response to oxidative stress.', 'https://www.ncbi.nlm.nih.gov/pubmed/23690622'), 
(17, '10.1042/BJ20090764', '19527223', 1, 'Coupling oxidative signals to protein phosphorylation via methionine oxidation in Arabidopsis.', 'https://www.ncbi.nlm.nih.gov/pubmed/19527223'), 
(20, '10.1074/jbc.M200709200', '12050149', 1, 'Protein oxidation of cytochrome C by reactive halogen species enhances its peroxidase activity.', 'https://www.ncbi.nlm.nih.gov/pubmed/12050149'), 
(21, '10.1073/pnas.0809279106', '19196960', 0, 'Disruption of the M80-Fe ligation stimulates the translocation of cytochrome c to the cytoplasm and nucleus in nonapoptotic cells.', 'https://www.ncbi.nlm.nih.gov/pubmed/19196960'); 


--
-- Estructura de tabla para la tabla `Evidence` ---------------------------
--
CREATE TABLE `Evidence`(
  `evi_id`int(11) PRIMARY KEY,
  `met_id` int(11) NOT NULL,
  `doi_id` int(11) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Volcado de datos para la tabla `Evidence`
--
INSERT INTO `Evidence` (`evi_id`, `met_id`, `doi_id`)
VALUES 
/* --------- Evidence Jurkat ------ */
(1, 1, 1), 
(2, 2, 1), 
(3, 3, 1), 
(4, 4, 1), 
(5, 5, 1), 
(6, 6, 1), 
(7, 7, 1), 
(8, 8, 1), 
(9, 9, 1), 
(10, 10, 1), 

/* --------- Evidence Arabidopsis ------ */

(11,11,2), 
(12,12,2), 
(13,13,2), 
(14,14,2), 
(15,15,2),


/* ------ Evidence Regulatory ------- */


(16, 16, 7), 
(17, 17, 8), 
(18, 18, 8), 
(19, 19, 10), 
(20, 20, 10), 
(21, 21, 10),
(22, 22, 20),
(23, 22, 21),
(24, 23, 17);


--
-- Foreign key
--

ALTER TABLE Methionine
  ADD CONSTRAINT fk_Methionine_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Methionine
  ADD CONSTRAINT fk_Methionine_Regulatory
  FOREIGN KEY (reg_id)
  REFERENCES Regulatory (reg_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Methionine
  ADD CONSTRAINT fk_Methionine_Organism
  FOREIGN KEY (org_id)
  REFERENCES Organism (org_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Phosphorylation
  ADD CONSTRAINT fk_Phosphorylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Acetylation
  ADD CONSTRAINT fk_Acetylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Methylation
  ADD CONSTRAINT fk_Methylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Ubiquitination
  ADD CONSTRAINT fk_Ubiquitination_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Sumoylation
  ADD CONSTRAINT fk_Sumoylation_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE OGlcNAc
  ADD CONSTRAINT fk_OGlcNAc_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE RegPTM
  ADD CONSTRAINT fk_RegPTM_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Disease
  ADD CONSTRAINT fk_Disease_Protein
  FOREIGN KEY (prot_id)
  REFERENCES Protein (prot_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Evidence
  ADD CONSTRAINT fk_Evidence_DOI
  FOREIGN KEY (doi_id)
  REFERENCES DOI (doi_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Evidence
  ADD CONSTRAINT fk_Evidence_Methionine
  FOREIGN KEY (met_id)
  REFERENCES Methionine (met_id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE Organism
  ADD CONSTRAINT fk_Organism_Taxon
  FOREIGN KEY (org_sp)
  REFERENCES Taxon (org_sp)
  ON DELETE CASCADE ON UPDATE CASCADE;
 
