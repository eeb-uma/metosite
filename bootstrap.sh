#!/bin/bash

# Build docker images
docker-compose build

# Run services
docker-compose up -d

# Rebuild swagger docs
docker-compose exec api /var/www/vendor/bin/swagger /var/www/server -o /var/www/server/swagger.json